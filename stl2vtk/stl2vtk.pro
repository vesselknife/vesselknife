#-------------------------------------------------
#
# Project created by QtCreator 2014-12-28T20:40:55
#
#-------------------------------------------------

TEMPLATE = app
TARGET = stl2vtk

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
DESTDIR = ../../Executables

SOURCES += \
    ../shared/tree.cpp \
    ../shared/vtkimport.cpp \
    stl2vtk.cpp

HEADERS  += \
    ../shared/getoption.h \
    ../shared/tree.h \
    ../shared/vtkimport.h

include(../Pri/config.pri)
include(../Pri/vtk.pri)
