/*
 * VesselKnife tree format to VTK data format converter
 *
 * Copyright 2015 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "../shared/tree.h"
#include <string>

#include <vtkGenericDataObjectWriter.h>
#include <vtkStructuredGrid.h>
#include <vtkPointData.h>
#include <vtkSmartPointer.h>
#include <vtkDoubleArray.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkFieldData.h>
#include <vtkDataSetAttributes.h>



#include <vtkSmartPointer.h>
#include <vtkVersion.h>

#include <vtkParametricFunctionSource.h>
#include <vtkTupleInterpolator.h>
#include <vtkTubeFilter.h>
#include <vtkParametricSpline.h>

#include <vtkDoubleArray.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>

#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
//#include <vtkRenderWindow.h>
//#include <vtkRenderer.h>
//#include <vtkRenderWindowInteractor.h>
#include <vtkBooleanOperationPolyDataFilter.h>
#include <vtkSTLWriter.h>
#include <vtkSTLReader.h>
#include <vtkTriangleFilter.h>
#include <vtkAppendPolyData.h>
#include "../shared/vtkimport.h"


vtkSmartPointer<vtkPolyData> importStl(const char* filename)
{
    vtkSmartPointer<vtkSTLReader> stlReader = vtkSmartPointer<vtkSTLReader>::New();
    stlReader->SetFileName(filename);
    stlReader->Update();
    vtkSmartPointer<vtkPolyData> poly = stlReader->GetOutput();
    return poly;
}

//=============================================================================
// converter
bool export2vtk(vtkSmartPointer<vtkPolyData> currentUnion, const char* filename)
{
    vtkSmartPointer<vtkGenericDataObjectWriter> writer = vtkSmartPointer<vtkGenericDataObjectWriter>::New();

#if VTK_MAJOR_VERSION <= 5
    writer->SetInput(currentUnion);
#else
    writer->SetInputData(currentUnion);
#endif

    writer->SetFileName(filename);
    writer->Update();
    //always true as VTK has no notification on IO completion
    return true;
}

//=============================================================================
// Main function
int main(int argc, char* argv[])
{
    if(argc != 3)
    {
        fprintf(stderr, "Usage: stl2vtk <input.stl> <output.vtk>\n");
        return 1;
    }
    export2vtk(importStl(argv[1]), argv[2]);

    return 0;
}
