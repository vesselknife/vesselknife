#ifndef SMOOTHING_H
#define SMOOTHING_H

#include "tree.h"

class Smoothing
{
public:
    static Tree SmoothCenterline(const Tree *itree, const double tau1, const double tau2, const unsigned int iterations);
    static Tree SmoothRadii(const Tree *itree, const double tau1, const double tau2, const unsigned int iterations);
};

#endif // SMOOTHING_H
