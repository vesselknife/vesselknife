#ifndef TREEEXTEND_H
#define TREEEXTEND_H

#include "tree.h"
#include "../shared/byteimage.h"
#include "mz2itk.h"
#include <itkHessianRecursiveGaussianImageFilter.h>
#include <itkHessian3DToVesselnessMeasureImageFilter.h>

const int dxa[27][3] =
{
    {0, 0, 0},
    {0, 0, -1},
    {0, 0, 1},

    {0, -1, 0},
    {0, -1, -1},
    {0, -1, 1},

    {0, 1, 0},
    {0, 1, -1},
    {0, 1, 1},

    {-1, 0, 0},
    {-1, 0, -1},
    {-1, 0, 1},

    {-1, -1, 0},
    {-1, -1, -1},
    {-1, -1, 1},

    {-1, 1, 0},
    {-1, 1, -1},
    {-1, 1, 1},

    {1, 0, 0},
    {1, 0, -1},
    {1, 0, 1},

    {1, -1, 0},
    {1, -1, -1},
    {1, -1, 1},

    {1, 1, 0},
    {1, 1, -1},
    {1, 1, 1}
};


class TreeExtend
{
    typedef itk::Image< double, 3 > ImageType;
    typedef itk::HessianRecursiveGaussianImageFilter< ImageType > HFiltr;
    typedef HFiltr::Pointer PHFiltr;
    typedef itk::Image< itk::SymmetricSecondRankTensor< double, 3 >, 3 > TensorImageType;

public:
    TreeExtend(Tree *tree, ImageType::Pointer image, double mingoal, int maxnodes, double sigma, bool normalize = false, double alfa1 = 0.5, double alfa2 = 2.0);
    Tree getResult();

private:
    void computeTensorImage();
    Tree traverseTree();
    std::vector<NodeIn3D> extendTip(NodeIn3D& tip, NodeIn3D& neighbor, Tree *tree = NULL);
    double computeGoalFunction(TensorImageType::IndexType& probee, double* thrvp, double *thrvn);
    double vesselness(TensorImageType::PixelType::EigenValuesArrayType& eval);

    ImageType::Pointer m_image;
    TensorImageType::Pointer tensorimage;
    ImageType::SpacingType spacing;
    PHFiltr filter;
    Tree *m_tree;

    double m_sigma;
    double m_mingoal;
    double m_alfa1;
    double m_alfa2;
    bool m_normalize;
    int m_maxnodes;

};

#endif // TREEEXTEND_H
