#ifndef TREEDIAMETERS_H
#define TREEDIAMETERS_H

#include "tree.h"
#include "sphear.h"
#include "byteimage.h"
#include <vector>
#include <linalg.h>
#include <dataanalysis.h>


class TreeDiameters
{
public:
    TreeDiameters(Tree* tree, Byte3DImage* image, double gamma, int divider, double rho);
    Tree getResult();
    Tree getResult(int ooo);
    Tree getResult(unsigned int node[3]);

private:
    alglib::real_2d_array radially_spaced_directions;
    alglib::real_2d_array pts;
    alglib::real_2d_array cvm;
    alglib::real_1d_array w;
    alglib::real_2d_array u;
    alglib::real_2d_array vt;

    unsigned int radially_spaced_directions_number;
    double gamma;
    double rho;
    int divider;
    Tree *tree;
    Byte3DImage *image;

    double diameterFromPca(void);
    void findVectors(double center[3]);
    void findBorderIntersection(double center[3], unsigned int direction_index);

};



//using namespace alglib;


/*
const int dx[26][3] =
{
    {0, 0, -1},
    {0, 0, 1},

    {0, -1, 0},
    {0, -1, -1},
    {0, -1, 1},

    {0, 1, 0},
    {0, 1, -1},
    {0, 1, 1},

    {-1, 0, 0},
    {-1, 0, -1},
    {-1, 0, 1},

    {-1, -1, 0},
    {-1, -1, -1},
    {-1, -1, 1},

    {-1, 1, 0},
    {-1, 1, -1},
    {-1, 1, 1},

    {1, 0, 0},
    {1, 0, -1},
    {1, 0, 1},

    {1, -1, 0},
    {1, -1, -1},
    {1, -1, 1},

    {1, 1, 0},
    {1, 1, -1},
    {1, 1, 1}
};

class TreeDiameters
{
public:
    TreeDiameters(Tree* tree, Byte3DImage* image, float firstlev, float secondlev);
    Tree getResult();
    Tree getResult(int ooo);
    Tree getResult(unsigned int node[3]);

private:
    alglib::real_2d_array pts;
    alglib::real_2d_array cvm;
    alglib::real_1d_array w;
    alglib::real_2d_array u;
    alglib::real_2d_array vt;

    double firstlevel;
    double secondlevel;
    Tree *intree;
    Byte3DImage *inimage;
    double diameter_from_pca(unsigned int node[3]);
    void findvectors(unsigned int node[3]);
    void findvectors2(alglib::real_2d_array& r);
};
*/
#endif // TREEDIAMETERS_H
