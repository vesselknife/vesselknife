#ifndef MZ2ITK_H
#define MZ2ITK_H

#include <itkImage.h>
#include "../shared/byteimage.h"
#include <itkCastImageFilter.h>
#include <itkRescaleIntensityImageFilter.h>
#include <itkIntensityWindowingImageFilter.h>
#include <itkIdentityTransform.h>
#include <itkResampleImageFilter.h>

template <class T> class Mz2Itk
{
public:
    typedef itk::Image< T, 3 > Image3DType;

    static typename Image3DType::Pointer getItk(Byte3DImage::Pointer input, double multiplicant)
    {
        typedef itk::IntensityWindowingImageFilter< Byte3DImage, Image3DType > RescaleFilterType;
        typename RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
        rescaleFilter->SetInput(input);
        rescaleFilter->SetWindowMinimum(0);
        rescaleFilter->SetWindowMaximum(255);
        rescaleFilter->SetOutputMinimum(0);
        rescaleFilter->SetOutputMaximum(255*multiplicant);
        rescaleFilter->Update();
        typename Image3DType::Pointer output = rescaleFilter->GetOutput();
        return output;
    }

    static typename Image3DType::Pointer getItk(Byte3DImage::Pointer input)
    {
        typedef itk::CastImageFilter< Byte3DImage, Image3DType > RescaleFilterType;
        typename RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
        rescaleFilter->SetInput(input);
        rescaleFilter->Update();
        typename Image3DType::Pointer output = rescaleFilter->GetOutput();
        return output;
    }

    static void getMZ(Byte3DImage::Pointer* output, typename Image3DType::Pointer input)
    {
        typedef itk::CastImageFilter< Image3DType, Byte3DImage > RescaleFilterType;
        typename RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
        rescaleFilter->SetInput(input);
        rescaleFilter->Update();
        *output = rescaleFilter->GetOutput();
    }

    static Byte3DImage::Pointer getMZ(typename Image3DType::Pointer input)
    {
        typename Byte3DImage::Pointer output = Byte3DImage::New();
        getMZ(&output, input);
        return output;
    }

    static void getMZNormalized(Byte3DImage::Pointer* output, typename Image3DType::Pointer input, T* min = NULL, T* max = NULL)
    {
        typedef itk::RescaleIntensityImageFilter< Image3DType, Byte3DImage > RescaleFilterType;
        typename RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
        rescaleFilter->SetInput(input);
        rescaleFilter->SetOutputMinimum(0);
        rescaleFilter->SetOutputMaximum(255);
        rescaleFilter->Update();
        *output = rescaleFilter->GetOutput();

#ifdef VESSELKNIFE_CONSOLE
        extern bool verbose;
        if(verbose)
        {
            printf("\Image mapped from <%f, %f> to <0, 255>\n", (float)rescaleFilter->GetInputMinimum(), (float)rescaleFilter->GetInputMaximum());
        }
#endif

        if(min != NULL) *min = rescaleFilter->GetInputMinimum();
        if(max != NULL) *max = rescaleFilter->GetInputMaximum();
    }

    static Byte3DImage::Pointer getMZNormalized(typename Image3DType::Pointer input)
    {
        typename Byte3DImage::Pointer output = Byte3DImage::New();
        getMZNormalized(&output, input);
        return output;
    }


    static typename Image3DType::Pointer getUpsizedBinaryI(Byte3DImage::Pointer input)
    {
        //typename Image3DType::Pointer output = Image3DType::New();
        typename Image3DType::RegionType::SizeType Size;
        typename Image3DType::SpacingType Spacing;
        for(int i = 0; i < 3; i++)
        {
            Size[i]  = input->GetLargestPossibleRegion().GetSize()[i]*2;
            Spacing[i] = input->GetSpacing()[i]/2.0;
        }

        typedef itk::IdentityTransform<double, 3> TransformType;
        typedef itk::ResampleImageFilter<Byte3DImage, Image3DType> ResampleImageFilterType;

        typename ResampleImageFilterType::Pointer resample = ResampleImageFilterType::New();
        resample->SetInput(input);
        resample->SetSize(Size);
        resample->SetOutputSpacing(Spacing);
        resample->SetTransform(TransformType::New());
        resample->UpdateLargestPossibleRegion();

        typename Image3DType::Pointer output = resample->GetOutput();
        return output;
    }

    static typename Image3DType::Pointer getUpsizedBinaryM(Byte3DImage::Pointer input)
    {
        typename Image3DType::Pointer output = Image3DType::New();
        typename Image3DType::RegionType Region;
        typename Image3DType::RegionType::IndexType index;
        typename Image3DType::RegionType::IndexType xyz;
        typename Image3DType::RegionType::SizeType Size;
        typename Image3DType::RegionType::SizeType Sizei;
        double spac[3], orgi[3];
        for(int i = 0; i < 3; i++)
        {
            index[i] = 0;
            Sizei[i] = input->GetLargestPossibleRegion().GetSize()[i];
            Size[i]  = input->GetLargestPossibleRegion().GetSize()[i]*2;
            spac[i] = input->GetSpacing()[i]/2.0;
            orgi[i] = 0;
        }
        Region.SetIndex( index );
        Region.SetSize( Size );

        output->SetRegions( Region );
        output->SetOrigin(orgi);
        output->SetSpacing(spac);

        size_t numberOfPixels = output->GetLargestPossibleRegion().GetNumberOfPixels();
        T* data = new T[numberOfPixels];
        memset(data, 0, numberOfPixels*sizeof(T));
        output->GetPixelContainer()->SetImportPointer(data, numberOfPixels, true); //true - dealocate data on delete, false - no date dealocation


        unsigned int dims = input->GetLargestPossibleRegion().GetImageDimension();
        for(unsigned int d = 0; d < dims; d++) xyz[d] = 0;

        for(xyz[2] = 0; xyz[2] < Sizei[2]; ++xyz[2])
        {
            index[2] = xyz[2] * 2;
            for(xyz[1] = 0; xyz[1] < Sizei[1]; ++xyz[1])
            {
                index[1] = xyz[1] * 2;
                for(xyz[0] = 0; xyz[0] < Sizei[0]; ++xyz[0])
                {
                    index[0] = xyz[0] * 2;
                    unsigned char inputvalue = input->GetPixel(xyz);
                    output->SetPixel(index, inputvalue);
                }
            }
        }

        const int directions[7][4] =
        {
            {0, 0, 1, 1},
            {0, 1, 0, 1},
            {1, 0, 0, 1},
            {0, 1, 1, 4},
            {1, 0, 1, 4},
            {1, 1, 0, 4},
            {1, 1, 1, 13}
        };
        const int neighbours[7][13][3] =
        {
            {{ 0, 0, 1}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}},
            {{ 0, 1, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}},
            {{ 1, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}},
            {{ 0, 0, 1}, { 0, 1, 0}, { 0, 1, 1}, { 0,-1, 1}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}},
            {{ 0, 0, 1}, { 1, 0, 0}, { 1, 0, 1}, {-1, 0, 1}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}},
            {{ 1, 0, 0}, { 0, 1, 0}, { 1, 1, 0}, {-1, 1, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}, { 0, 0, 0}},
            {{ 0, 0, 1}, { 0, 1, 0}, { 1, 0, 0}, { 0, 1, 1}, { 1, 1, 0}, { 1, 0, 1}, { 0,-1, 1}, {-1, 1, 0}, {-1, 0, 1}, { 1, 1, 1}, {-1, 1, 1}, {-1,-1, 1}, { 1,-1, 1}}

        };

        for(int dirs = 0; dirs < 7; dirs++)
        {
            for(int dir = 0; dir < directions[dirs][3]; dir++)
            {
                for(index[2] = directions[dirs][2]; index[2] < Size[2]-1; index[2]+=2)
                {
                    for(index[1] = directions[dirs][1]; index[1] < Size[1]-1; index[1]+=2)
                    {
                        for(index[0] = directions[dirs][0]; index[0] < Size[0]-1; index[0]+=2)
                        {

                            int sx = neighbours[dirs][dir][0];
                            int sy = neighbours[dirs][dir][1];
                            int sz = neighbours[dirs][dir][2];

                            index[0] += sx;
                            index[1] += sy;
                            index[2] += sz;
                            T v1 = output->GetPixel(index);
                            index[0] -= 2*sx;
                            index[1] -= 2*sy;
                            index[2] -= 2*sz;
                            T v2 = output->GetPixel(index);
                            index[0] += sx;
                            index[1] += sy;
                            index[2] += sz;
                            v1 = v1<v2 ?v1 :v2;

                            v2 = output->GetPixel(index);
                            output->SetPixel(index, v2<v1 ?v1 :v2);
                        }
                    }
                }
            }
        }
        return output;
    }

};


#endif // MZ2ITK_H
