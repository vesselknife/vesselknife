/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2024  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILENAMEREMAP_H
    #define FILENAMEREMAP_H

    #if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
        #include <string>

        #define _MZ_UTF8_TO_U16
//        #define _MZ_UTF8_TO_SHORT_WORKAROUND

        #if defined _MZ_UTF8_TO_U16
            std::wstring _mz_utf8_to_u16(const char* fn);
        #endif

        #if defined _MZ_UTF8_TO_SHORT_WORKAROUND
            std::string _mz_utf8_to_short_for_writing(const char* fn);
            std::string _mz_utf8_to_short_for_reading(const char* fn);
        #endif
    #endif
#endif // FILENAMEREMAP_H
