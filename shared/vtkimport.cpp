
#include "vtkimport.h"

#include <vtkGenericDataObjectReader.h>
#include <vtkStructuredGrid.h>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkFieldData.h>
#include <vtkDataSetAttributes.h>
#include <string>

Tree VtkImport::getTree(const char* filename)
{
    Tree tree;

    vtkSmartPointer<vtkGenericDataObjectReader> reader = vtkSmartPointer<vtkGenericDataObjectReader>::New();
    reader->SetFileName(filename);
    reader->Update();

    if(reader->IsFilePolyData())
    {
//        std::cout << "output is a polydata" << std::endl;
        vtkPolyData* output = reader->GetPolyDataOutput();
//        std::cout << "output has " << output->GetNumberOfPoints() << " points." << std::endl;
//        int nlines = output->GetNumberOfLines();
//        vtkCellArray* lines= output->GetLines();
        vtkPoints* points = output->GetPoints();

//        vtkFieldData* field = output->GetFieldData();
        //std::cout << "Field: " << field->GetNumberOfTuples() << std::endl;
        //std::cout << "Field: " << field->GetArrayName(0) << std::endl;
        //vtkIdType narrays = field->getNumberOfArrays();
        //vtkIdType narrays = output->GetFieldData()->getNumberOfArrays();
        //std::cout << "There are " << narrays << " arrays." << std::endl;
//        std::cout << std::endl << "Edges" << endl << "----------" << std::endl;
//        std::cout << "There are " << lines->GetNumberOfCells() << " cells." << std::endl;
//        std::cout << "There are " << points->GetNumberOfPoints() << " points." << std::endl;

        vtkDataSetAttributes* attribs = output->GetAttributes(vtkDataSetAttributes::SCALARS);
//        std::cout << "ArrayName " << attribs->GetArrayName(0) << std::endl;
//        std::cout << "Components " << attribs->GetArray(0)->GetNumberOfComponents() << std::endl;
//        std::cout << "Tuples " << attribs->GetArray(0)->GetNumberOfTuples() << std::endl;

        bool diameters = attribs->GetArray(0)->GetNumberOfTuples() >= points->GetNumberOfPoints() &&
                         attribs->GetArray(0)->GetNumberOfComponents() >= 1;

        for(vtkIdType i = 0; i < output->GetNumberOfCells(); i++)
        {
            //std::cout << "Type: " << extractEdges->GetOutput()->GetCell(i)->GetClassName() << std::endl;
            //std::cout << "Line " << i << " : " << *(output->GetCell(i)) << std::endl;
            //vtkIdType numperofpoints = output->GetCell(i)->GetNumberOfPoints();
            vtkIdType numperofpoints = output->GetCell(i)->GetNumberOfPoints();
            std::vector<NodeIn3D> branch;
            //std::cout << "Line " << std::endl;

            for(vtkIdType n = 0; n < numperofpoints; n++)
            {
                double vertex[3];
                points->GetPoint(output->GetCell(i)->GetPointIds()->GetId(n), vertex);
                NodeIn3D node;
                node.x = vertex[0];
                node.y = vertex[1];
                node.z = vertex[2];
                if(diameters) node.radius = attribs->GetArray(0)->GetComponent(n, 0);
                else node.radius = 0;
                if(n == 0 || n == numperofpoints-1) node.connections = 1;
                else node.connections = 2;
                branch.push_back(node);
            }

            tree.addBranch(branch);
        }
    }
    return tree;
}

VtkImport::VtkImport()
{
}
