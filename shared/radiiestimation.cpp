#include "radiiestimation.h"

#include <dlib/matrix.h>
#include <dlib/optimization.h>

//#include <itkHessianRecursiveGaussianImageFilter.h>
#include <itkDiscreteGaussianImageFilter.h>
#include "vkScaffoldHessianToVesselnessFilter.h"

#include "../shared/sphear.h"
#include <itkContinuousIndex.h>
#include <itkLinearInterpolateImageFunction.h>

typedef dlib::matrix<double, 1, 1> InputData;
typedef dlib::matrix<double, 2, 1> Parameters2;
typedef dlib::matrix<double, 3, 1> Parameters3;
typedef dlib::matrix<double, 2, 2> Covarm2;
typedef dlib::matrix<double, 3, 3> Covarm3;
const double pi = 3.141592653589793;
const double almostzero = 0.0000000000001;
double modelVesselnessPower = 3;

//=============================================================================
//=============================================================================
//=============================================================================
//=============================================================================

double modelVesselness (
    const InputData& data,
    const Parameters2& params
)
{
    const double x = data(0);
    const double l = params(1)*0.0341071;
    const double a = params(0)*params(1)*9.674351*pow(x, modelVesselnessPower-3);//14.7606796/1.525754;
    const double k = 17.288712;
    const int n = 432;
    return a*(k/sqrt(1+k*k*(x/l-l/x)*(x/l-l/x)))*pow((x/l)*(x/l)/(1+(x/l)*(x/l)), n);
}

double residualVesselness (
    const std::pair<InputData, double>& data,
    const Parameters2& params
)
{
    return modelVesselness(data.first, params) - data.second;
}

double RadiiEstimation::vesselnessInSingleNode(ImageFType::IndexType index, HessianImageType* hessianimage)
{
    const double m_Alpha1 = 0.5;
    const double m_Alpha2 = 2.0;

    HessianImageType::PixelType hessian = hessianimage->GetPixel(index);
    typedef itk::FixedArray< double, HessianImageType::ImageDimension > EigenValuesArrayType;
    typedef itk::Matrix< double, HessianImageType::ImageDimension, HessianImageType::ImageDimension > EigenVectorMatrixType;
    typedef itk::SymmetricEigenAnalysis< HessianImageType::PixelType, EigenValuesArrayType, EigenVectorMatrixType > SymmetricEigenAnalysisType;
    EigenValuesArrayType eigenValue;
    EigenVectorMatrixType eigenVector;
    SymmetricEigenAnalysisType symmetricEigenSystem(HessianImageType::ImageDimension);
    symmetricEigenSystem.SetOrderEigenValues(true);
    symmetricEigenSystem.ComputeEigenValuesAndVectors(hessian, eigenValue, eigenVector);
    double normalizeValue = std::min(-1.0 * eigenValue[1], -1.0 * eigenValue[0]);
    if ( normalizeValue > 0 )
    {
        double lineMeasure;
        if ( eigenValue[2] <= 0 )
          {
              lineMeasure =  eigenValue[2] / ( m_Alpha1 * normalizeValue );
              lineMeasure = std::exp( -0.5 * lineMeasure *lineMeasure);
          }
          else
          {
              lineMeasure = eigenValue[2] / ( m_Alpha2 * normalizeValue );
              lineMeasure = std::exp( -0.5 * lineMeasure * lineMeasure);
          }
        lineMeasure *= normalizeValue;
        return lineMeasure;
    }
    return 0;
}

void RadiiEstimation::vesselnessProfileMatching(
                               double* result, const ImageFType::Pointer input_image,
                               const ImageFType::IndexType* pixelIndex, const unsigned int poinscount,
                               const double min_stdev, const double max_stdev, const unsigned int scales,
                               double sigma_power)
{
    typedef itk::HessianRecursiveGaussianImageFilter< ImageFType > F1;
    typedef F1::Pointer P;
    typedef ScaffoldHessianToVesselnessFilter< float > F2;
    typedef F2::Pointer P2;

    typedef itk::Image<itk::SymmetricSecondRankTensor<double, 3>, 3> HessianImageType;

    ImageFType::SizeType size;
    size.Fill(1);
    P filter = F1::New();
    filter->SetInput(input_image);
/*
    P2 filter2 = F2::New();
    filter2->SetMethodSato();
*/
    double q = pow((double)max_stdev/min_stdev, (1.0/double(scales-1)));

    float*  buffer = new float[scales*poinscount];
    for(int s = 0; s < scales; s++)
    {
        float stdev = min_stdev*pow(q, (double)s);
        filter->SetSigma(stdev);
        filter->Update();
        HessianImageType* hessianimage = filter->GetOutput();

        for(int i = 0; i < poinscount; i++)
        {
            double val = vesselnessInSingleNode(pixelIndex[i], hessianimage);
/*
            ImageFType::IndexType index = pixelIndex[i];
            ImageFType::RegionType region(index, size);
            filter2->GetOutput()->SetRequestedRegion(region);
            filter2->SetInput(hessianimage);
            filter2->Update();
            ImageFType::Pointer out = filter2->GetOutput();
            float val = out->GetPixel(index);
*/
            buffer[i*scales + s] = (double)val * pow(stdev, sigma_power);
        }
    }

    for(int i = 0; i < poinscount; i++)
    {
        std::vector<std::pair<InputData, double> > samples;
        InputData m;
        double maxx = 0;
        double maxy = 0;
        unsigned int exts = 0;
        unsigned int s;

        for(s = 0; s < scales; s++)
        {
            float val = buffer[i*scales + s];
            if(maxy < val)
            {
                exts = s;
                maxy = val;
            }
        }
        maxx = min_stdev*pow(q, exts);

        unsigned int mins = exts;
        unsigned int maxs = exts;
        double lastval = maxy;
        for(s = exts+1; s < scales; s++)
        {
            float val = buffer[i*scales + s];
            if(val > lastval)
                break;
            maxs = s;
            lastval = val;
        }
        lastval = maxy;
        for(s = exts-1; s < scales; s--)
        {
            float val = buffer[i*scales + s];
            if(val > lastval)
                break;
            mins = s;
            lastval = val;
        }
        if(maxs-mins < 2)
        {
            result[i] = maxx;
            continue;
        }
        for(s = mins; s <= maxs; s++)
        {
            float stdev = min_stdev*pow(q, s);
            m(0) = stdev;
            float val = buffer[i*scales + s];
            samples.push_back(std::make_pair(m, val));
        }

        Parameters2 params;
        params(0) = maxy;//1.525754;//3.0515086640625;
        params(1) = maxx;

        //printf("y = %f x = %f : ", (float)params(0), (float)params(1));
        try
        {
            modelVesselnessPower = sigma_power;
            dlib::solve_least_squares(dlib::objective_delta_stop_strategy(1e-8),
                                         residualVesselness,
                                         dlib::derivative(residualVesselness),
                                         samples,
                                         params);
            result[i] = fabs(params(1));
//            if(result[i] > 3)
//            {
//                for(int s = 0; s < scales; s++)
//                {
//                    float val = buffer[i*scales + s];
//                    printf("%f ", val);
//                }
//                printf("\ny = %f (%f) x = %f (%f) i = %i\n\n", (float)params(0), (float)maxy, (float)params(1), (float)maxx, (int)samples.size());
//                fflush(stdout);
//            }
        }
        catch (...)
        {
            result[i] = maxx;
        }

//        for(s = 0; s < scales; s++)
//            printf("%f ", buffer[i*scales + s]);
//        printf("  =>  %f\n", result[i]);

    }
    delete[] buffer;
}

Tree RadiiEstimation::RadiiFromVesselnessProfile(const Tree *itree, const ImageFType::Pointer image, const double minstd, const double maxstd, const unsigned int scales, double sigma_power)
{
    unsigned int i;
    Tree tree = *itree;
    int nodecount = tree.nodeCount();
    double* result = new double[nodecount];
    ImageFType::IndexType* pixelIndex = new ImageFType::IndexType[nodecount];

    auto spacing = image->GetSpacing();
    for(i = 0; i < nodecount; i++)
    {
        pixelIndex[i][0] = tree.node(i).x / spacing[0] + 0.5;
        pixelIndex[i][1] = tree.node(i).y / spacing[1] + 0.5;
        pixelIndex[i][2] = tree.node(i).z / spacing[2] + 0.5;
    }
    vesselnessProfileMatching(result, image, pixelIndex, nodecount, minstd, maxstd, scales, sigma_power);
    for(i = 0; i < nodecount; i++)
    {
        tree.setRadius(i, result[i]);
    }
    delete[] pixelIndex;
    delete[] result;
    return tree;
}


//=============================================================================
//=============================================================================
//=============================================================================
//=============================================================================
void supplementJoints(TreeStructure* ts)
{
    unsigned int bmax = ts->branches.size();
    for(unsigned int b = 0; b < bmax; b++)
    {
        unsigned int nmax = ts->branches[b].nodeIndex.size();
        if(nmax < 2)
            continue;
        NodeIn3D* nk = &(ts->nodes[ts->branches[b].nodeIndex[nmax-1]]);
        NodeIn3D* np = &(ts->nodes[ts->branches[b].nodeIndex[0]]);
        NodeIn3D* nks = &(ts->nodes[ts->branches[b].nodeIndex[nmax-2]]);
        NodeIn3D* nps = &(ts->nodes[ts->branches[b].nodeIndex[1]]);
        if(np->connections > 2 && nps->connections <= 2)
        {
            np->radius += nps->radius;
        }
        if(nk->connections > 2 && nks->connections <= 2)
        {
            nk->radius += nks->radius;
        }
    }
    unsigned int nmax = ts->nodes.size();
    for(unsigned int n = 0; n < nmax; n++)
    {
        if(ts->nodes[n].connections > 2)
            ts->nodes[n].radius /= ts->nodes[n].connections;
    }
}

double modelErfc (
    const InputData& data,
    const Parameters3& params
)
{
    const double v0 = params(0);
    const double dv = params(1);
    const double r = params(2);
    const double d = data(0);
    return v0+dv*erfc(d-r);
}

double residualErfc (
    const std::pair<InputData, double>& data,
    const Parameters3& params
)
{
    return modelErfc(data.first, params) - data.second;
}

double erfcMatchingSingle(std::vector<double>& input)
{
    try
    {
        std::vector<std::pair<InputData, double> > samples;
        InputData m;
        for(double f : input)
        {
            m(0) = samples.size();
            samples.push_back(std::make_pair(m, f));
        }
        Parameters3 params;
        params(0) = 1.0; //v0
        params(1) = 0.5; //dv
        params(2) = 1.0; //r
        dlib::solve_least_squares_lm(dlib::objective_delta_stop_strategy(1e-7)/*.be_verbose()*/,
                                     residualErfc,
                                     dlib::derivative(residualErfc),
                                     samples,
                                     params);
        return params(2);
    }
    catch (...)
    {
        return -1.0;
    }
}

std::vector<double> erfcMatching(std::vector<std::vector<double> >& input)
{
    unsigned int directions = input.size();
    std::vector<double> rad;
    rad.resize(directions);
    for(unsigned int i = 0; i < directions; i++)
    {
        rad[i] = erfcMatchingSingle(input[i]);
    }
    return rad;
}

struct Vector3
{
    double x;
    double y;
    double z;
};

std::vector <Vector3> findVectorsFromPartialEstimates(std::vector<double>& estimates, Sphear& sphear)
{
    std::vector <Vector3> vectors;
    unsigned int num_of_dirs = sphear.verticesNumber();
    vectors.resize(num_of_dirs);
    for(unsigned int d = 0; d < num_of_dirs; d++)
    {
        double vector[3];
        sphear.vertex(d, vector);

        vectors[d].x = vector[0]*estimates[d];
        vectors[d].y = vector[1]*estimates[d];
        vectors[d].z = vector[2]*estimates[d];
    }
    return vectors;
}

void estimateRadiusByPCA(std::vector <Vector3>& vectors, double eigenvals[2])
{
    double varx = 0.0;
    double vary = 0.0;
    double varz = 0.0;
    double varxy = 0.0;
    double varxz = 0.0;
    double varyz = 0.0;

    unsigned int imax = vectors.size();
    for(unsigned int i = 0; i < imax; i++)
    {
        varx += vectors[i].x*vectors[i].x;
        vary += vectors[i].y*vectors[i].y;
        varz += vectors[i].z*vectors[i].z;
        varxy += vectors[i].x*vectors[i].y;
        varxz += vectors[i].x*vectors[i].z;
        varyz += vectors[i].y*vectors[i].z;
    }

    if(varx > almostzero || vary > almostzero || varz > almostzero)
    {
        Covarm3 covarm;
        covarm(0, 0) = varx/imax;
        covarm(1, 1) = vary/imax;
        covarm(2, 2) = varz/imax;
        covarm(0, 1) = covarm(1, 0) = varxy/imax;
        covarm(0, 2) = covarm(2, 0) = varxz/imax;
        covarm(2, 1) = covarm(1, 2) = varyz/imax;
        dlib::eigenvalue_decomposition<Covarm3> eigdec(covarm);
        auto eigvals = eigdec.get_real_eigenvalues();

        double e0 = fabs(eigvals(0));
        double e1 = fabs(eigvals(1));
        if(e0 <= e1)
        {
            eigenvals[0] = e0;
            eigenvals[1] = e1;
        }
        else
        {
            eigenvals[0] = e1;
            eigenvals[1] = e0;
        }
    }
    else
    {
        eigenvals[0] = 0;
        eigenvals[1] = 0;
    }
}


struct Vector2
{
    double x;
    double y;
};

void findOrthogonalDirections(Vector3& v, std::vector <Vector3>* orthogonals)
{
   Vector3 s;
   Vector3 c;
   if(fabs(v.x) <= fabs(v.y) && fabs(v.x) <= fabs(v.z))
   {
       s.x = 0.0;
       s.y = v.z;
       s.z = -v.y;
   }
   else if(fabs(v.y) <= fabs(v.z) && fabs(v.y) <= fabs(v.x))
   {
       s.y = 0.0;
       s.z = v.x;
       s.x = -v.z;
   }
   else
   {
       s.z = 0.0;
       s.x = v.y;
       s.y = -v.x;
   }
   c.x = v.z*s.y - v.y*s.z;
   c.y = v.x*s.z - v.z*s.x;
   c.z = v.y*s.x - v.x*s.y;

   double dc = sqrt(c.x*c.x + c.y*c.y + c.z*c.z);
   double ds = sqrt(s.x*s.x + s.y*s.y + s.z*s.z);
   c.x /= dc;
   c.y /= dc;
   c.z /= dc;
   s.x /= ds;
   s.y /= ds;
   s.z /= ds;

    unsigned int imax = orthogonals->size();
    for(unsigned int i = 0; i < imax; i++)
    {
        double a = 2.0*pi*i/imax;
        double ca = cos(a);
        double sa = sin(a);
        (*orthogonals)[i].x = c.x*ca + s.x*sa;
        (*orthogonals)[i].y = c.y*ca + s.y*sa;
        (*orthogonals)[i].z = c.z*ca + s.z*sa;
    }
}

Vector3 findParallelDirection(const TreeStructure* tree, const unsigned int branch, const unsigned int node, const std::vector<double>& filter)
{
    const NodeIn3D* nn = &(tree->nodes[tree->branches[branch].nodeIndex[node]]);
    unsigned int nodescount = tree->branches[branch].nodeIndex.size();
    unsigned int samplcount = filter.size();
    Vector3 vnn;
    vnn.x = nn->x;
    vnn.y = nn->y;
    vnn.z = nn->z;
    Vector3 v;
    v.x = 0;
    v.y = 0;
    v.z = 0;
    unsigned int ii;
    ii = node + 1;
    for(unsigned int i = 0; i < samplcount && ii < nodescount; i++, ii++)
    {
        const NodeIn3D* mm = &(tree->nodes[tree->branches[branch].nodeIndex[ii]]);
        v.x += (mm->x - vnn.x)*filter[i];
        v.y += (mm->y - vnn.y)*filter[i];
        v.z += (mm->z - vnn.z)*filter[i];
    }
    ii = node - 1;
    for(unsigned int i = 0; i < samplcount && ii < nodescount; i++, ii--)
    {
        const NodeIn3D* mm = &(tree->nodes[tree->branches[branch].nodeIndex[ii]]);
        v.x -= (mm->x - vnn.x)*filter[i];
        v.y -= (mm->y - vnn.y)*filter[i];
        v.z -= (mm->z - vnn.z)*filter[i];
    }
    double n = sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
    if(n > 0)
    {
        v.x /= n;
        v.y /= n;
        v.z /= n;
    }
    else
    {
        v.x = 0;
        v.y = 0;
        v.z = 1;
    }
    return v;
}

std::vector <Vector2> findCSVectorsFromPartialEstimates(std::vector<double>& estimates)
{
    std::vector <Vector2> vectors;
    unsigned int imax = estimates.size();
    vectors.resize(imax);
    for(unsigned int i = 0; i < imax; i++)
    {
        double a = 2.0*pi*i/imax;
        vectors[i].x = cos(a)*estimates[i];
        vectors[i].y = sin(a)*estimates[i];
    }
    return vectors;
}

void estimateRadiusByCSPCA(std::vector <Vector2>& vectors, double eigenvals[2])
{
    double varx = 0.0;
    double vary = 0.0;
    double varxy = 0.0;
    unsigned int imax = vectors.size();
    for(unsigned int i = 0; i < imax; i++)
    {
        varx += vectors[i].x*vectors[i].x;
        vary += vectors[i].y*vectors[i].y;
        varxy += vectors[i].x*vectors[i].y;
    }
    if(varx > almostzero || vary > almostzero)
    {
        Covarm2 covarm;
        covarm(0, 0) = varx/imax;
        covarm(1, 1) = vary/imax;
        covarm(0, 1) = covarm(1, 0) = varxy/imax;
        dlib::eigenvalue_decomposition<Covarm2> eigdec(covarm);
        auto eigvals = eigdec.get_real_eigenvalues();

        double e0 = fabs(eigvals(0));
        double e1 = fabs(eigvals(1));
        if(e0 <= e1)
        {
            eigenvals[0] = e0;
            eigenvals[1] = e1;
        }
        else
        {
            eigenvals[0] = e1;
            eigenvals[1] = e0;
        }
    }
    else
    {
        eigenvals[0] = 0;
        eigenvals[1] = 0;
    }
}

double gaussianFunction(double x, double s)
{
    return 0.3989422804014327*exp(-0.5*x*x/(s*s))/s;
}

std::vector<double> gaussDerivative(double sigma)
{
    std::vector<double> v;
    unsigned int size = sigma*2.5+1;
    double g = gaussianFunction(0, sigma);
    for(unsigned int i = 0; i < size; i++)
    {
        double gg = gaussianFunction(i+1, sigma);
        v.push_back(g-gg);
        g = gg;
    }
    return v;
}

Tree RadiiEstimation::RadiiFromOmnidirectionalErfcMatching(const Tree* itree, const ImageFType::Pointer image,
                                            const unsigned int tetrahedron_divisions,
                                            const unsigned int profile_samples,
                                            const double profile_range,
                                            const double eigenvalue_weight)
{
    Tree tree = *itree;
    TreeStructure* ts = (TreeStructure*) &tree;
    itk::ContinuousIndex<double, 3> pixel;
    itk::LinearInterpolateImageFunction<ImageFType, double>::Pointer interpolator =
      itk::LinearInterpolateImageFunction<ImageFType, double>::New();
    interpolator->SetInputImage(image);
    ImageFType::SizeType imagesize = image->GetLargestPossibleRegion().GetSize();
    Sphear sphear(tetrahedron_divisions);
    unsigned int num_of_dirs = sphear.verticesNumber();

    unsigned int nmax = ts->nodes.size();
    auto spacing = image->GetSpacing();
    for(unsigned int n = 0; n < nmax; n++)
    {
        NodeIn3D* nn = &(ts->nodes[n]);
        nn->radius = 0;
        std::vector<std::vector<double> > data;
        for(unsigned int d = 0; d < num_of_dirs; d++)
        {
            std::vector<double> line;
            line.resize(profile_samples);
            double vector[3];
            sphear.vertex(d, vector);
            for(unsigned int i = 0; i < profile_samples; i++)
            {
                double l = (double)i*profile_range/profile_samples;
                pixel[0] = (vector[0]*l + nn->x)/spacing[0];
                pixel[1] = (vector[1]*l + nn->y)/spacing[1];
                pixel[2] = (vector[2]*l + nn->z)/spacing[2];
                if(pixel[0] < 0) pixel[0] = 0;
                if(pixel[0] > imagesize[0]-1) pixel[0] = imagesize[0]-1;
                if(pixel[1] < 0) pixel[1] = 0;
                if(pixel[1] > imagesize[1]-1) pixel[1] = imagesize[1]-1;
                if(pixel[2] < 0) pixel[2] = 0;
                if(pixel[2] > imagesize[2]-1) pixel[2] = imagesize[2]-1;
                line[i] = interpolator->EvaluateAtContinuousIndex(pixel);
            }
            data.push_back(line);
        }
        std::vector<double> perfc = erfcMatching(data);
        std::vector<Vector3> verfc = findVectorsFromPartialEstimates(perfc, sphear);
        double rerfc[2];
        estimateRadiusByPCA(verfc, rerfc);
        double r =  profile_range* sqrt(2.0*(rerfc[0]*(1.0-eigenvalue_weight) + rerfc[1]*eigenvalue_weight))  /profile_samples;
        if(r > nn->radius)
            nn->radius = r;
    }
    return tree;
}

Tree RadiiEstimation::RadiiFromCrossSectionErfcMatching(const Tree* itree, const ImageFType::Pointer image,
                                            const double direction_sigma,
                                            const unsigned int num_of_dirs,
                                            const unsigned int profile_samples,
                                            const double profile_range,
                                            const double eigenvalue_weight)
{
    Tree tree = *itree;
    TreeStructure* ts = (TreeStructure*) &tree;
    itk::ContinuousIndex<double, 3> pixel;
    itk::LinearInterpolateImageFunction<ImageFType, double>::Pointer interpolator =
      itk::LinearInterpolateImageFunction<ImageFType, double>::New();
    interpolator->SetInputImage(image);
    ImageFType::SizeType imagesize = image->GetLargestPossibleRegion().GetSize();
    unsigned int nmax = ts->nodes.size();
    for(unsigned int n = 0; n < nmax; n++)
        ts->nodes[n].radius = 0;

    auto spacing = image->GetSpacing();
    unsigned int bmax = ts->branches.size();

    std::vector<double> filter = gaussDerivative(direction_sigma);

    for(unsigned int b = 0; b < bmax; b++)
    {
        unsigned int nmax = ts->branches[b].nodeIndex.size();
        for(unsigned int n = 0; n < nmax; n++)
        {
            NodeIn3D* nn = &(ts->nodes[ts->branches[b].nodeIndex[n]]);
            if(nn->connections > 2)
                continue;
            std::vector<std::vector<double> > data;
            Vector3 v;
            v = findParallelDirection(ts, b, n, filter);

            std::vector <Vector3> dirs;
            dirs.resize(num_of_dirs);
            findOrthogonalDirections(v, &dirs);

            for(unsigned int d = 0; d < num_of_dirs; d++)
            {
                std::vector<double> line;
                line.resize(profile_samples);
                for(unsigned int i = 0; i < profile_samples; i++)
                {
                    double l = (double)i*profile_range/profile_samples;
                    pixel[0] = (dirs[d].x * l + nn->x)/spacing[0];
                    pixel[1] = (dirs[d].y * l + nn->y)/spacing[1];
                    pixel[2] = (dirs[d].z * l + nn->z)/spacing[2];
                    if(pixel[0] < 0) pixel[0] = 0;
                    if(pixel[0] > imagesize[0]-1) pixel[0] = imagesize[0]-1;
                    if(pixel[1] < 0) pixel[1] = 0;
                    if(pixel[1] > imagesize[1]-1) pixel[1] = imagesize[1]-1;
                    if(pixel[2] < 0) pixel[2] = 0;
                    if(pixel[2] > imagesize[2]-1) pixel[2] = imagesize[2]-1;
                    line[i] = interpolator->EvaluateAtContinuousIndex(pixel);
                }
                data.push_back(line);
            }
            std::vector<double> perfc = erfcMatching(data);
            std::vector<Vector2> verfc = findCSVectorsFromPartialEstimates(perfc);
            double rerfc[2];
            estimateRadiusByCSPCA(verfc, rerfc);
            double r =  profile_range* sqrt(2.0*(rerfc[0]*(1.0-eigenvalue_weight) + rerfc[1]*eigenvalue_weight))  /profile_samples;
            if(r > nn->radius)
                nn->radius = r;
        }
    }
    supplementJoints(ts);
    return tree;
}
/*
double findBorderIntersection(const ImageBType::Pointer image, const double center[3], const double vector[3])
{
    const double almostzero = 0.000001;
    double tmp[3]; // temporary distance
    double shift[3]; // shift for x, y, and z step
    double dist[3]; // distance for x, y, and z step
    int d;
    for(d = 0; d < 3; d++)
    {
        dist[d] = -1.0;
        shift[d] = center[d];

        if(vector[d] > 0) shift[d] = ceil(shift[d]);
        else shift[d] = floor(shift[d]);

        if(fabs(vector[d]) > almostzero)
        {
            tmp[d] = shift[d]-center[d];
            dist[d] = 0.0;
            for(int dd = 0; dd < 3; dd++)
            {
                if(dd != d) tmp[dd] = tmp[d]*vector[dd]/vector[d];
                dist[d] += tmp[dd]*tmp[dd];
            }
        }
    }
    // Computation of plane and line intersection point with the subsequent (from center point) planes
    do
    {
        auto spacing = image->GetSpacing();
        ImageBType::IndexType node; //center location in voxel index space
        for(d = 0; d < 3; d++)
        {
            if( dist[d] >= 0.0 &&
                (dist[d] <= dist[0] || dist[0] < 0) &&
                (dist[d] <= dist[1] || dist[1] < 0) &&
                (dist[d] <= dist[2] || dist[2] < 0) )
            {
                tmp[d] = shift[d]-center[d];
                if(vector[d] > 0) node[d] = shift[d] + 1.0;
                else node[d] = shift[d];
                for(int dd = 0; dd < 3; dd++)
                {
                    if(dd != d)
                    {
                        tmp[dd] = tmp[d]*vector[dd]/vector[d];
                        node[dd] = tmp[dd] + center[dd] + 0.5;
                    }
                }
                if(node[0] >= image->GetLargestPossibleRegion().GetSize()[0] || node[1] >= image->GetLargestPossibleRegion().GetSize()[1] || node[2] >= image->GetLargestPossibleRegion().GetSize()[2])
                {
                    double x = tmp[0]*spacing[0];
                    double y = tmp[1]*spacing[1];
                    double z = tmp[2]*spacing[2];
                    return sqrt(x*x+y*y+z*z);
                }
                unsigned char pix = image->GetPixel(node);
                if(pix)
                {
                    if(vector[d] > 0) shift[d] += 1.0;
                    else shift[d] -= 1.0;

                    tmp[d] = shift[d]-center[d];
                    dist[d] = 0.0;
                    for(int dd = 0; dd < 3; dd++)
                    {
                        if(dd != d) tmp[dd] = tmp[d]*vector[dd]/vector[d];
                        dist[d] += tmp[dd]*tmp[dd];
                    }
                }
                else
                {
                    double x = tmp[0]*spacing[0];
                    double y = tmp[1]*spacing[1];
                    double z = tmp[2]*spacing[2];
                    return sqrt(x*x+y*y+z*z);
                }
                break;
            }
        }
        if(d >= 3)
        {
            return -1.0;
        }
    }while(true);
    return -1.0;
}
*/


double findBorderIntersection(const ImageBType::Pointer image, const double center[3], const double vector[3])
{
    const double almostzero = 0.00000001;
    int increment[3];
    unsigned int planeindex[3];
    double sdistance[3];
    double cs[3];
    auto spacing = image->GetSpacing();
    auto size = image->GetLargestPossibleRegion().GetSize();
    ImageBType::IndexType node;
    int d;
    for(d = 0; d < 3; d++)
    {
        cs[d] = center[d] / spacing[d] + 0.5;
        node[d] = floor(cs[d]);

        if(vector[d] >= 0)
        {
            increment[d] = 1;
            planeindex[d] = (int) ceil(cs[d]);
        }
        else if(vector[d] < 0)
        {
            increment[d] = -1;
            planeindex[d] = (int) floor(cs[d]);
        }
        if(fabs(vector[d]) < almostzero)
            sdistance[d] = size[0]*spacing[0]+size[1]*spacing[1]+size[2]*spacing[2]*1000;
        else
            sdistance[d] = ((int)planeindex[d]-cs[d])*spacing[d]/vector[d];
    }
    ImageBType::PixelType cpixel = image->GetPixel(node);
    while(true)
    {
        if(sdistance[0] < sdistance[1] && sdistance[0] < sdistance[2]) //X
            d = 0;
        else if(sdistance[1] < sdistance[2]) //Y
            d = 1;
        else //Z
            d = 2;

        if(d != 0)
            node[0] = cs[0] + sdistance[d]*vector[0]/spacing[0];
        if(d != 1)
            node[1] = cs[1] + sdistance[d]*vector[1]/spacing[1];
        if(d != 2)
            node[2] = cs[2] + sdistance[d]*vector[2]/spacing[2];

        double sp = ((int)planeindex[d]+(double)increment[d]*0.5-cs[d])*spacing[d]/vector[d];
        node[d] = cs[d] + sp*vector[d]/spacing[d];


        if(node[0] >= size[0] || node[1] >= size[1] || node[2] >= size[2])
            return sdistance[d];
        ImageBType::PixelType pixel = image->GetPixel(node);
        if(pixel != cpixel)
            return sdistance[d];
        planeindex[d] += increment[d];
        sdistance[d] = ((int)planeindex[d]-cs[d])*spacing[d]/vector[d];
    }
    return -1.0;
}

Tree RadiiEstimation::RadiiFromBinaryOmnidirectional(
        const Tree* itree,
        const ImageBType::Pointer image,
        const unsigned int tetrahedron_divisions,
        const double eigenvalue_weight)
{
    Tree tree = *itree;
    TreeStructure* ts = (TreeStructure*) &tree;
    Sphear sphear(tetrahedron_divisions);
    unsigned int num_of_dirs = sphear.verticesNumber();

    unsigned int nmax = ts->nodes.size();
    //auto spacing = image->GetSpacing();
    for(unsigned int n = 0; n < nmax; n++)
    {
        NodeIn3D* nn = &(ts->nodes[n]);
        nn->radius = 0;
        double center[3];
        center[0] = nn->x;///spacing[0];
        center[1] = nn->y;///spacing[1];
        center[2] = nn->z;///spacing[2];
        std::vector <Vector3> vectors;
        unsigned int d;
        for(d = 0; d < num_of_dirs; d++)
        {
            double vs[3];
//            double vector[3];
            sphear.vertex(d, vs);
//            vector[0] = vs[0];///spacing[0];
//            vector[1] = vs[1];///spacing[1];
//            vector[2] = vs[2];///spacing[2];
            double er = findBorderIntersection(image, center, vs);
            if(er < 0)
                break;
            Vector3 vdist;
            vdist.x = er*vs[0];
            vdist.y = er*vs[1];
            vdist.z = er*vs[2];
            vectors.push_back(vdist);
//            if(n == 111)
//            {
//                float r = sqrt(vdist.x*vdist.x + vdist.y*vdist.y + vdist.z*vdist.z);
//                printf("%f\t%f\t%f\t%f\n", (float)vdist.x, (float)vdist.y, (float)vdist.z, r);
//                fflush(stdout);
//            }
        }
        if(d<num_of_dirs)
        {
            nn->radius = 0.0;
        }
        else
        {
            double rbin[2];
            estimateRadiusByPCA(vectors, rbin);
            double r =  sqrt(2.0*(rbin[0]*(1.0-eigenvalue_weight) + rbin[1]*eigenvalue_weight));
            if(r > nn->radius)
                nn->radius = r;
        }
    }
    return tree;
}

Tree RadiiEstimation::RadiiFromBinaryCrossSection(
        const Tree* itree,
        const ImageBType::Pointer image,
        const double direction_sigma,
        const unsigned int num_of_dirs,
        const double eigenvalue_weight)
{
    Tree tree = *itree;
    TreeStructure* ts = (TreeStructure*) &tree;
    unsigned int nmax = ts->nodes.size();
    for(unsigned int n = 0; n < nmax; n++)
        ts->nodes[n].radius = 0;
    //auto spacing = image->GetSpacing();
    unsigned int bmax = ts->branches.size();
    std::vector<double> filter = gaussDerivative(direction_sigma);

    for(unsigned int b = 0; b < bmax; b++)
    {
        unsigned int nmax = ts->branches[b].nodeIndex.size();
        for(unsigned int n = 0; n < nmax; n++)
        {
            NodeIn3D* nn = &(ts->nodes[ts->branches[b].nodeIndex[n]]);
            if(nn->connections > 2)
                continue;
            Vector3 v;
            v = findParallelDirection(ts, b, n, filter);
            std::vector <Vector3> dirs;
            dirs.resize(num_of_dirs);
            double center[3];
            center[0] = nn->x;///spacing[0];
            center[1] = nn->y;///spacing[1];
            center[2] = nn->z;///spacing[2];
            findOrthogonalDirections(v, &dirs);
            std::vector <Vector2> vectors;
            unsigned int d;
            for(d = 0; d < num_of_dirs; d++)
            {
                double a = 2.0*pi*(double)d/num_of_dirs;
                double vector[3];
                vector[0] = dirs[d].x;///spacing[0];
                vector[1] = dirs[d].y;///spacing[1];
                vector[2] = dirs[d].z;///spacing[2];
                double er = findBorderIntersection(image, center, vector);
                if(er < 0)
                    break;
                Vector2 v2;
                v2.x = cos(a)*er;
                v2.y = sin(a)*er;
                vectors.push_back(v2);
            }
            if(d<num_of_dirs)
            {
                nn->radius = 0.0;
            }
            else
            {
                double rbin[2];
                estimateRadiusByCSPCA(vectors, rbin);
                double r =  sqrt(2.0*(rbin[0]*(1.0-eigenvalue_weight) + rbin[1]*eigenvalue_weight));
                //double r = sqrt(rbin[0]*rbin[0]*(1.0-eigenvalue_weight) + rbin[1]*rbin[1]*eigenvalue_weight);
                //double r = (rbin[0]*(1.0-eigenvalue_weight) + rbin[1]*eigenvalue_weight);
                if(r > nn->radius)
                    nn->radius = r;
            }
        }
    }
    supplementJoints(ts);
    return tree;
}

