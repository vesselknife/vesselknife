#ifndef TREESMOOTH_H
#define TREESMOOTH_H
#include "tree.h"
#include "../shared/byteimage.h"
#include <vector>

struct NodeForSmooth
{
    float x, y, z;
    float dx, dy, dz;
    std::vector<unsigned int> n;
};

class TreeSmooth
{
public:
    /**
     * @brief treeSmooth class to smooth tree branches
     * @param tree input tree
     * @param d1 first derivate minimization parameter
     * @param d2 second derivate minimization parameter
     * @param fa anchore to original location parameter - linear relationship
     * @param faaa anchore to original location parameter - third power relationship
     */
    TreeSmooth(Tree* tree, float d1, float d2, float fa, float faaa);
    Tree getResult(int iterations, int chillout);

private:
    float d1, d2, fa, faaa;
    Tree* tree;
    std::vector<NodeForSmooth> mtree;
    int iteration;

    void initialize(void);
    void compute(int iterations, int chillout);
    Tree getResult(void);

    void step(void);
    inline void computeF(int ni);
    inline void computeD(int ni);
};

#endif // TREESMOOTH_H
