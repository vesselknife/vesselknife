#ifndef BYTEIMAGE_H
#define BYTEIMAGE_H
#include <itkImage.h>
typedef itk::Image< unsigned char, 3 > Byte3DImage;
#endif // BYTEIMAGE_H
