/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2024  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "filenameremap.h"
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#include <windows.h>
#include <string>
#include <locale>
#include <codecvt>

#if defined _MZ_UTF8_TO_U16
std::wstring _mz_utf8_to_u16(const char* fn)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    std::wstring ws = converter.from_bytes(fn);
    return ws;
//    int size_needed = MultiByteToWideChar(CP_UTF8, 0, fn, -1, NULL, 0);
//    std::wstring ws(size_needed, L'\0');
//    MultiByteToWideChar(CP_UTF8, 0, fn, -1, &ws[0], size_needed);
}
#endif

// ITK library workaround for crappy windows file system API
#if defined _MZ_UTF8_TO_SHORT_WORKAROUND
std::string _mz_utf8_to_short_for_writing(const char* fn)
{
    SetFileApisToANSI();
    std::wstring ws = _mz_utf8_to_u16(fn);
    long length = GetShortPathNameW(ws.c_str(), NULL, 0);
    if (length == 0)
    {
        HANDLE hFile = CreateFileW(ws.c_str(), GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
        DWORD written;
        WriteFile(hFile, ws.c_str(), length*sizeof(wchar_t), &written, NULL);
        CloseHandle(hFile);
        length = GetShortPathNameW(ws.c_str(), NULL, 0);
        if (length == 0)
            return std::string(fn);
    }
    wchar_t* buffer = new wchar_t[length];
    GetShortPathNameW(ws.c_str(), buffer, length);
    ws = buffer;
    std::string rs(ws.begin(), ws.end());
    delete[] buffer;
    return rs;
}
std::string _mz_utf8_to_short_for_reading(const char* fn)
{
    SetFileApisToANSI();
    std::wstring ws = _mz_utf8_to_u16(fn);
    long length = GetShortPathNameW(ws.c_str(), NULL, 0);
    if (length == 0)
        return std::string(fn);
    wchar_t* buffer = new wchar_t[length];
    GetShortPathNameW(ws.c_str(), buffer, length);
    ws = buffer;
    std::string rs(ws.begin(), ws.end());
    delete[] buffer;
    return rs;
}
#endif

#endif

