#ifndef VTKIMPORT_H
#define VTKIMPORT_H

#include "tree.h"

class VtkImport
{
public:
    VtkImport();

    static Tree getTree(const char *filename);
};

#endif // VTKIMPORT_H
