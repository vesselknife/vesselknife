#ifndef RADIIESTIMATION_H
#define RADIIESTIMATION_H

#include "tree.h"
#include "byteimage.h"
#include <itkHessianRecursiveGaussianImageFilter.h>

typedef itk::Image<float, 3>  ImageFType;
typedef itk::Image<unsigned char, 3>  ImageBType;

class RadiiEstimation
{
public:
    typedef itk::Image<itk::SymmetricSecondRankTensor<double, 3>, 3> HessianImageType;

    static Tree RadiiFromVesselnessProfile(
            const Tree* tree, const ImageFType::Pointer image,
            const double minstd, const double maxstd, const unsigned int scales, double sigma_power);

    static Tree RadiiFromOmnidirectionalErfcMatching(
            const Tree *tree,
            const ImageFType::Pointer image,
            const unsigned int tetrahedron_divisions,
            const unsigned int profile_samples,
            const double profile_range,
            const double eigenvalue_weight);

    static Tree RadiiFromCrossSectionErfcMatching(
            const Tree* itree,
            const ImageFType::Pointer image,
            double direction_sigma,
            const unsigned int num_of_dirs,
            const unsigned int profile_samples,
            const double profile_range,
            const double eigenvalue_weight);

    static Tree RadiiFromBinaryOmnidirectional(
            const Tree* itree,
            const ImageBType::Pointer image,
            const unsigned int tetrahedron_divisions,
            const double eigenvalue_weight);

    static Tree RadiiFromBinaryCrossSection(
            const Tree* itree,
            const ImageBType::Pointer image,
            const double direction_sigma,
            const unsigned int num_of_dirs,
            const double eigenvalue_weight);


private:
    static double vesselnessInSingleNode(ImageFType::IndexType index,
                                         HessianImageType *hessianimage);


    static void vesselnessProfileMatching(double* result, const ImageFType::Pointer input_image,
            const ImageFType::IndexType* pixelIndex, const unsigned int poinscount,
            const double min_stdev, const double max_stdev, const unsigned int scales, double sigma_power);
};

#endif // RADIIESTIMATION_H
