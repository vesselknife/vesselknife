#include "smoothing.h"
#include <dlib/matrix.h>

const unsigned int matrixsize = 51;
const double lastsample = 0.0005;

std::vector <double> computeInversePentadiagonalLine(const double tau1, const double tau2)
{
    const double snake_gamma = 1.0;
    unsigned int i = 0;
    std::vector <double> kernel;
    dlib::matrix<double> pentadiagonal = dlib::identity_matrix<double>(matrixsize);
    for(i = 0; i < matrixsize; i++)
    {
        pentadiagonal(i, (i+matrixsize-2)%matrixsize) = pentadiagonal(i,(i+2)%matrixsize) = tau2;
        pentadiagonal(i,(i+matrixsize-1)%matrixsize) = pentadiagonal(i,(i+1)%matrixsize) = -4*tau2 -tau1;
        pentadiagonal(i,i) = 6*tau2 +2*tau1 + snake_gamma;
    }
    dlib::matrix<double> inversed = dlib::inv(pentadiagonal);
    double first = inversed(0, 0);
    kernel.push_back(first);
    double norm = first;
    for(i = 1; i < matrixsize/2; i++)
    {
        double cur = inversed(i, 0);
        if(cur/first < lastsample)
            break;
        kernel.push_back(cur);
        norm += 2.0*cur;
    }
    unsigned int imax = kernel.size();
    for(i = 0; i < imax; i++)
        kernel[i] /= norm;
    return kernel;
}

Tree Smoothing::SmoothCenterline(const Tree *itree, const double tau1, const double tau2, const unsigned int iterations)
{
    Tree tree = *itree;
    TreeStructure* ts = (TreeStructure*) &tree;



    std::vector <double> kernel = computeInversePentadiagonalLine(tau1, tau2);
    unsigned int kernel_size = kernel.size();

    unsigned int n;

    unsigned int bmax = ts->branches.size();
    for(unsigned int b = 0; b < bmax; b++)
    {
        unsigned int nmax = ts->branches[b].nodeIndex.size();
        std::vector <Coordinates3d> line;
        line.resize(nmax+2*kernel_size-4);

        for(unsigned int iter = 0; iter < iterations; iter++)
        {

            unsigned int l = kernel_size-2;
            for(n = 0; n < nmax; n++, l++)
            {
//                NodeIn3D* nn = &(ts->nodes[ts->branches[b].nodeIndex[n]]);
//                line[l].x = nn->x;
//                line[l].y = nn->y;
//                line[l].z = nn->z;
                line[l] = (Coordinates3d)(ts->nodes[ts->branches[b].nodeIndex[n]]);
            }
            unsigned int first = kernel_size-2;
            unsigned int last = nmax-1+kernel_size-2;
            unsigned int lastmax = nmax+2*kernel_size-5;
            unsigned int thismax = 2*last-first;
            if(thismax > lastmax)
                thismax = lastmax;
            l = last-1;

            while(true)
            {
                for(n = last+1; n <= thismax; n++, l--)
                {
                    line[n].x = 2*line[last].x-line[l].x;
                    line[n].y = 2*line[last].y-line[l].y;
                    line[n].z = 2*line[last].z-line[l].z;
                }
                if(n > lastmax)
                    break;
                last = n-1;
                thismax = 2*last-first;
                if(thismax > lastmax)
                    thismax = lastmax;
                l = last-1;
            }
            l = first+1;
            for(n = first-1; n < first; n--, l++)
            {
                line[n].x = 2*line[first].x-line[l].x;
                line[n].y = 2*line[first].y-line[l].y;
                line[n].z = 2*line[first].z-line[l].z;
            }

            l = first+1;
            for(n = 1; n < nmax-1; n++, l++)
            {
                Coordinates3d vector;
                vector.x = line[l].x*kernel[0];
                vector.y = line[l].y*kernel[0];
                vector.z = line[l].z*kernel[0];
                for(unsigned int i = 1; i < kernel_size; i++)
                {
                    vector.x += (line[l+i].x+line[l-i].x)*kernel[i];
                    vector.y += (line[l+i].y+line[l-i].y)*kernel[i];
                    vector.z += (line[l+i].z+line[l-i].z)*kernel[i];
                }
                NodeIn3D* nn = &(ts->nodes[ts->branches[b].nodeIndex[n]]);
                nn->x = vector.x;
                nn->y = vector.y;
                nn->z = vector.z;
            }
        }
    }
    return tree;
}

Tree Smoothing::SmoothRadii(const Tree *itree, const double tau1, const double tau2, const unsigned int iterations)
{
    Tree tree = *itree;
    TreeStructure* ts = (TreeStructure*) &tree;
    std::vector <double> kernel = computeInversePentadiagonalLine(tau1, tau2);
    unsigned int kernel_size = kernel.size();

    unsigned int n;

    unsigned int bmax = ts->branches.size();
    for(unsigned int b = 0; b < bmax; b++)
    {
        unsigned int nmax = ts->branches[b].nodeIndex.size();
        std::vector <double> line;
        line.resize(nmax+2*kernel_size-4);

        for(unsigned int iter = 0; iter < iterations; iter++)
        {
            unsigned int l = kernel_size-2;
            for(n = 0; n < nmax; n++, l++)
            {
                line[l] = ts->nodes[ts->branches[b].nodeIndex[n]].radius;
            }
            unsigned int first = kernel_size-2;
            unsigned int last = nmax-1+kernel_size-2;
            unsigned int lastmax = nmax+2*kernel_size-5;
            unsigned int thismax = 2*last-first;
            if(thismax > lastmax)
                thismax = lastmax;
            l = last-1;
            while(true)
            {
                for(n = last+1; n <= thismax; n++, l--)
                {
                    line[n] = line[l];
                }
                if(n > lastmax)
                    break;
                last = n-1;
                thismax = 2*last-first;
                if(thismax > lastmax)
                    thismax = lastmax;
                l = last-1;
            }
            l = first+1;
            for(n = first-1; n < first; n--, l++)
            {
                line[n] = line[l];
            }

            l = first+1;
            for(n = 1; n < nmax-1; n++, l++)
            {
                double value;
                value = line[l]*kernel[0];
                for(unsigned int i = 1; i < kernel_size; i++)
                {
                    value += (line[l+i]+line[l-i])*kernel[i];
                }
                ts->nodes[ts->branches[b].nodeIndex[n]].radius = value;
//                NodeIn3D* nn = &(ts->nodes[ts->branches[b].nodeIndex[n]]);
//                nn->radius = value;
            }
        }
    }
    return tree;
}
