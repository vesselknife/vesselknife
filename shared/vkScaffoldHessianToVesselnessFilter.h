/*=========================================================================
 * VesselKnife project: 3D Angigram Analysis Software
 *
 * Copyright 2016-2025 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * ScaffoldHessianToVesselnessFilter:
 *  1. Enables computation of Sato, Frangi and Erdt vesselness
 *  2. Implements procedure for suppression of structures elongated
 *     in XY plain, eg. fiber scaffolds
 *  3. Supports radius estimation of vessels based on Sato vesselness
 *  4. Implements multithreading computation (in DynamicThreadedGenerateData)
 *  5. Computes eigen decopmosition in-place (in comparson to
 *     itkHessian3DToVesselnessMeasureImageFilter)


References:

Implementation of Sato algorithm according to implementation in
    itkHessian3DToVesselnessMeasureImageFilter.hxx

    Sato, Yoshinobu, et al. "3D multi-scale line filter for segmentation and
    visualization of curvilinear structures in medical images."
    CVRMed-MRCAS'97. Springer Berlin Heidelberg, 1997.

Implementation of Frangi and Erdt algorithms according to:
    Frangi, Alejandro F., et al. "Multiscale vessel enhancement filtering."
    International Conference on Medical Image Computing and Computer-Assisted Intervention.
    Springer Berlin Heidelberg, 1998.

    Drechsler, Klaus, and Cristina Oyarzun Laura.
    "Comparison of vesselness functions for multiscale analysis of the liver vasculature."
    Proceedings of the 10th IEEE International Conference on Information Technology
    and Applications in Biomedicine. IEEE, 2010.

Implementation of XY enhanced vesselness algorithm (original by myself).
    Bajcsy, P., Yoon, S., Florczyk, S. J., Hotaling, N. A., Simon, M., Szczypinski, P. M.,
    ... and Sriram, R. D. (2017). Modeling, validation and verification of
    three-dimensional cell-scaffold contacts from terabyte-sized images. BMC bioinformatics,
    18(1), 526

Hannink algorithm not implemented (2D only):
    Hannink, Julius, Remco Duits, and Erik Bekkers. "Crossing-preserving multi-scale vesselness."
    International Conference on Medical Image Computing and Computer-Assisted Intervention.
    Springer International Publishing, 2014.

 *=========================================================================*/

#ifndef vkScaffoldHessianToVesselnessFilter_h
#define vkScaffoldHessianToVesselnessFilter_h

#include "itkSymmetricSecondRankTensor.h"
#include "itkSymmetricEigenAnalysisImageFilter.h"

template <typename TPixel>
class ITK_TEMPLATE_EXPORT ScaffoldHessianToVesselnessFilter : public
        itk::ImageToImageFilter<itk::Image<itk::SymmetricSecondRankTensor<double, 3>, 3>, itk::Image<TPixel, 3>>
{
public:
    typedef enum {
            Sato = 1,
            Frangi,
            Erdt,
            SatoXY,
            FrangiXY,
            ErdtXY
        } HessianToVesselnessMethod;

  ITK_DISALLOW_COPY_AND_ASSIGN(ScaffoldHessianToVesselnessFilter);
  using Self = ScaffoldHessianToVesselnessFilter;
  using Superclass = itk::ImageToImageFilter<itk::Image<itk::SymmetricSecondRankTensor<double, 3>, 3>, itk::Image<TPixel, 3>>;
  using Pointer = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;
  using InputImageType = typename Superclass::InputImageType;
  using OutputImageType = typename Superclass::OutputImageType;
  using InputPixelType = typename InputImageType::PixelType;
  using OutputPixelType = TPixel;
  using OutputImageRegionType = typename OutputImageType::RegionType;
  using InputImageRegionType = typename InputImageType::RegionType;

  static constexpr unsigned int ImageDimension = InputImageType::ImageDimension;
  static constexpr unsigned int InputPixelDimension = InputPixelType::Dimension;

  itkTypeMacro(ScaffoldHessianToVesselnessFilter, ImageToImageFilter);
  itkNewMacro(Self);

  itkSetMacro(Alpha1, double);
  itkGetConstMacro(Alpha1, double);
  itkSetMacro(Alpha2, double);
  itkGetConstMacro(Alpha2, double);
  itkSetMacro(Alpha, double);
  itkGetConstMacro(Alpha, double);
  itkSetMacro(C, double);
  itkGetConstMacro(C, double);
  itkSetMacro(Beta, double);
  itkGetConstMacro(Beta, double);

  itkSetMacro(Method, HessianToVesselnessMethod);
  itkGetConstMacro(Method, HessianToVesselnessMethod);

  void SetMethodSato(double Alpha1 = 0.5, double Alpha2 = 2.0)
  {
      SetAlpha1(Alpha1);
      SetAlpha2(Alpha2);
      SetMethod(Sato);
  }
  void SetMethodFrangi(double c, double Alpha = 0.5, double Beta = 0.5)
  {
    SetC(c);
    SetAlpha(Alpha);
    SetBeta(Beta);
    SetMethod(Frangi);
  }
  void SetMethodErdt(void)
  {
    SetMethod(Erdt);
  }
  void SetMethodSatoXY(double Alpha1 = 0.5, double Alpha2 = 2.0)
  {
      SetAlpha1(Alpha1);
      SetAlpha2(Alpha2);
      SetMethod(SatoXY);
  }
  void SetMethodFrangiXY(double c, double Alpha = 0.5, double Beta = 0.5)
  {
      SetC(c);
      SetAlpha(Alpha);
      SetBeta(Beta);
      SetMethod(FrangiXY);
  }
  void SetMethodErdtXY(void)
  {
    SetMethod(ErdtXY);
  }


protected:
  ScaffoldHessianToVesselnessFilter() = default;
  ~ScaffoldHessianToVesselnessFilter() override = default;

#if (ITK_VERSION_MAJOR < 5)
  void BeforeThreadedGenerateData() override {}
  void AfterThreadedGenerateData() override {}

  void ThreadedGenerateData(const InputImageRegionType & region, itk::ThreadIdType) override
  {
      itkDebugMacro(<< "ScaffoldHessianToVesselnessFilter::ThreadedGenerateData");
#else
  void DynamicThreadedGenerateData(const InputImageRegionType & region) override
  {
      itkDebugMacro(<< "ScaffoldHessianToVesselnessFilter::DynamicThreadedGenerateData");
#endif
      typename InputImageType::ConstPointer hessian_input = this->GetInput();
      typename OutputImageType::Pointer vesselness_output = this->GetOutput();
      itk::ImageRegionConstIterator< InputImageType > iter_hessian_input;
      iter_hessian_input = itk::ImageRegionConstIterator< InputImageType >(hessian_input, region);
      itk::ImageRegionIterator< OutputImageType > iter_vesselness_output;
      iter_vesselness_output = itk::ImageRegionIterator< OutputImageType >(vesselness_output, region);

      typedef itk::FixedArray< double, ImageDimension > EigenValuesArrayType;
      typedef itk::Matrix< double, ImageDimension, ImageDimension > EigenVectorMatrixType;
      typedef itk::SymmetricEigenAnalysis< InputPixelType, EigenValuesArrayType, EigenVectorMatrixType > SymmetricEigenAnalysisType;

      EigenValuesArrayType eigenValue;
      EigenVectorMatrixType eigenVector;
      SymmetricEigenAnalysisType symmetricEigenSystem(ImageDimension);

      iter_hessian_input.GoToBegin();
      iter_vesselness_output.GoToBegin();

      switch(m_Method)
      {

      case Sato:
          symmetricEigenSystem.SetOrderEigenValues(true);
          while ( !iter_hessian_input.IsAtEnd() )
          {
              symmetricEigenSystem.ComputeEigenValuesAndVectors(iter_hessian_input.Get(), eigenValue, eigenVector);
              double normalizeValue = std::min(-1.0 * eigenValue[1], -1.0 * eigenValue[0]);
              if ( normalizeValue > 0 )
              {
                  double lineMeasure;
                  if ( eigenValue[2] <= 0 )
                    {
                        lineMeasure =  eigenValue[2] / ( m_Alpha1 * normalizeValue );
                        lineMeasure = std::exp( -0.5 * lineMeasure *lineMeasure);
                    }
                    else
                    {
                        lineMeasure = eigenValue[2] / ( m_Alpha2 * normalizeValue );
                        lineMeasure = std::exp( -0.5 * lineMeasure * lineMeasure);
                    }
                  lineMeasure *= normalizeValue;
                  iter_vesselness_output.Set( static_cast< OutputPixelType >( lineMeasure ) );
              }
              else
              {
                  iter_vesselness_output.Set(itk::NumericTraits< OutputPixelType >::ZeroValue());
              }
              ++iter_hessian_input;
              ++iter_vesselness_output;
          }
          break;

      case Frangi:
          {
              double invNegTwoAlpha2 = -2.0*m_Alpha*m_Alpha;
              double invNegTwoBeta2 = -2.0*m_Beta*m_Beta;
              double invNegTwoC2 = -2.0*m_C*m_C;
              symmetricEigenSystem.SetOrderEigenMagnitudes(true);
              while ( !iter_hessian_input.IsAtEnd() )
              {
                  symmetricEigenSystem.ComputeEigenValuesAndVectors(iter_hessian_input.Get(), eigenValue, eigenVector);
                  if(eigenValue[1] > 0 || eigenValue[2] > 0)
                  {
                     iter_vesselness_output.Set(itk::NumericTraits< OutputPixelType >::ZeroValue());
                  }
                  else
                  {
                      double RB = fabs(eigenValue[0])/sqrt(fabs(eigenValue[1]*eigenValue[2]));
                      double RA = fabs(eigenValue[1])/fabs(eigenValue[2]);
                      double S = sqrt(eigenValue[0]*eigenValue[0] + eigenValue[1]*eigenValue[1] + eigenValue[2]*eigenValue[2]);
                      double lineMeasure = (1.0-exp(RA*RA/invNegTwoAlpha2))*exp(RB*RB*invNegTwoBeta2)*(1.0-exp(S*S*invNegTwoC2));
                      iter_vesselness_output.Set( static_cast< OutputPixelType >( lineMeasure ) );
                  }
                  ++iter_hessian_input;
                  ++iter_vesselness_output;
              }
          }
          break;

      case Erdt:
          symmetricEigenSystem.SetOrderEigenValues(true);
          while ( !iter_hessian_input.IsAtEnd() )
          {
              symmetricEigenSystem.ComputeEigenValuesAndVectors(iter_hessian_input.Get(), eigenValue, eigenVector);
              double K = (fabs(eigenValue[1]) - fabs(eigenValue[0])); K = 1.0 - fabs(K)/K;
              double lineMeasure = K*(((double)2.0/3.0)*eigenValue[2]-eigenValue[1]-eigenValue[0]);
              iter_vesselness_output.Set( static_cast< OutputPixelType >( lineMeasure ) );
              ++iter_hessian_input;
              ++iter_vesselness_output;
          }
          break;

      case SatoXY:
          symmetricEigenSystem.SetOrderEigenValues(true);
          while ( !iter_hessian_input.IsAtEnd() )
          {
              symmetricEigenSystem.ComputeEigenValuesAndVectors(iter_hessian_input.Get(), eigenValue, eigenVector);
              double dot = fabs(eigenValue[0]) > fabs(eigenValue[2])
                           ? 1.0 - fabs(eigenVector[0][2])
                           : 1.0 - fabs(eigenVector[2][2]);

              double normalizeValue = std::min(-1.0 * eigenValue[1], -1.0 * eigenValue[0]);
              if ( normalizeValue > 0 )
              {
                  double lineMeasure;
                  if ( eigenValue[2] <= 0 )
                    {
                        lineMeasure = eigenValue[2] / ( m_Alpha1 * normalizeValue );
                    }
                    else
                    {
                        lineMeasure = eigenValue[2] / ( m_Alpha2 * normalizeValue );
                    }
                  lineMeasure = std::exp( -0.5 * lineMeasure*lineMeasure);
                  lineMeasure *= normalizeValue;
                  lineMeasure *= dot;
                  iter_vesselness_output.Set( static_cast< OutputPixelType >( lineMeasure ) );
              }
              else
              {
                  iter_vesselness_output.Set(itk::NumericTraits< OutputPixelType >::ZeroValue());
              }
              ++iter_hessian_input;
              ++iter_vesselness_output;
          }
          break;

      case FrangiXY:
          {
              double invNegTwoAlpha2 = -2.0*m_Alpha*m_Alpha;
              double invNegTwoBeta2 = -2.0*m_Beta*m_Beta;
              double invNegTwoC2 = -2.0*m_C*m_C;
              symmetricEigenSystem.SetOrderEigenMagnitudes(true);
              while ( !iter_hessian_input.IsAtEnd() )
              {
                  symmetricEigenSystem.ComputeEigenValuesAndVectors(iter_hessian_input.Get(), eigenValue, eigenVector);
                  double dot = 1.0 - fabs(eigenVector[0][2]);

                  if(eigenValue[1] > 0 || eigenValue[2] > 0)
                  {
                     iter_vesselness_output.Set(itk::NumericTraits< OutputPixelType >::ZeroValue());
                  }
                  else
                  {
                      double RB = fabs(eigenValue[0])/sqrt(fabs(eigenValue[1]*eigenValue[2]));
                      double RA = fabs(eigenValue[1])/fabs(eigenValue[2]);
                      double S = sqrt(eigenValue[0]*eigenValue[0] + eigenValue[1]*eigenValue[1] + eigenValue[2]*eigenValue[2]);
                      double lineMeasure = (1.0-exp(RA*RA/invNegTwoAlpha2))*exp(RB*RB*invNegTwoBeta2)*(1.0-exp(S*S*invNegTwoC2));

                      lineMeasure *= dot;
                      iter_vesselness_output.Set( static_cast< OutputPixelType >( lineMeasure ) );
                  }
                  ++iter_hessian_input;
                  ++iter_vesselness_output;
              }

          }
          break;

      case ErdtXY:
          symmetricEigenSystem.SetOrderEigenValues(true);
          double dot = fabs(eigenValue[0]) > fabs(eigenValue[2])
                       ? 1.0 - fabs(eigenVector[0][2])
                       : 1.0 - fabs(eigenVector[2][2]);
          while ( !iter_hessian_input.IsAtEnd() )
          {
              symmetricEigenSystem.ComputeEigenValuesAndVectors(iter_hessian_input.Get(), eigenValue, eigenVector);
              double K = (fabs(eigenValue[1]) - fabs(eigenValue[0])); K = 1.0 - fabs(K)/K;
              double lineMeasure = K*(((double)2.0/3.0)*eigenValue[2]-eigenValue[1]-eigenValue[0]);
              lineMeasure *= dot;
              iter_vesselness_output.Set( static_cast< OutputPixelType >( lineMeasure ) );
              ++iter_hessian_input;
              ++iter_vesselness_output;
          }
          break;
      }
  }

  void GenerateOutputInformation() override
  {
    // Ensure output region matches input region
    Superclass::GenerateOutputInformation();
    this->GetOutput()->SetRegions(this->GetInput()->GetLargestPossibleRegion());
    this->GetOutput()->Allocate();
  }


  void PrintSelf(std::ostream & os, itk::Indent indent) const
  {
      Superclass::PrintSelf(os, indent);
      os << indent << "ScaffoldHessianToVesselnessFilter" << std::endl;
      switch(m_Method)
      {
      case SatoXY:
          os << indent << "Modification: XY enhanced" << std::endl;
      case Sato:
      {
          os << indent << "Method: Sato" << std::endl;
          os << indent << indent << "Alpha1: " << m_Alpha1 << std::endl;
          os << indent << indent << "Alpha2: " << m_Alpha2 << std::endl;
      }break;
      case FrangiXY:
          os << indent << "Modification: XY enhanced" << std::endl;
      case Frangi:
      {
          os << indent << "Method: Frangi" << std::endl;
          os << indent << indent << "c: " << m_C << std::endl;
          os << indent << indent << "Alpha: " << m_Alpha << std::endl;
          os << indent << indent << "Beta: " << m_Beta << std::endl;

      }break;
      case ErdtXY:
          os << indent << "Modification: XY enhanced" << std::endl;
      case Erdt:
      {
          os << indent << "Method: Erdt" << std::endl;
      }break;
      default: os << indent << "Method: Unspecified" << std::endl;
      }
  }

private:
  HessianToVesselnessMethod m_Method;
  double m_Alpha1;
  double m_Alpha2;
  double m_C;
  double m_Beta;
  double m_Alpha;
};
#endif
