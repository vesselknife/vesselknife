cmake_minimum_required(VERSION 3.1)
project(vesselknife_pack)

add_subdirectory(vesselknife)
add_subdirectory(vk4console)
add_subdirectory(vkconvert)

install(FILES "${PROJECT_SOURCE_DIR}/icons/vesselknife.png" DESTINATION bin)
