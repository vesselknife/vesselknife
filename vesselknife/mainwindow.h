#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include <QStyledItemDelegate>
#include <QListWidgetItem>
#include <QProcess>
#include <QTimer>
#include "imagegl.h"
#include "../shared/buildtree.h"
//#include "../shared/vtkimport.h"
#include "../shared/byteimage.h"
//#include <nifti/nifti1.h>
//#include <nifti/nifti1_io.h>
//#include "parametertreewidget.h"

class VkTool;

#include <string>
const char RELEASE_VERSION[] = "Release 25.01";

namespace Ui {
class MainWindow;
}

struct ImageWithInfo
{
    Byte3DImage::Pointer i;
    std::string t;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    ImageWithInfo* getImage(unsigned int index, std::string* name);
    ImageWithInfo *getCurrentImage(int index = -1, std::string *name = NULL);
    ImageWithInfo *getCurrentImage(QString *name);
    Tree *getCurrentTree(std::string *name = NULL, bool* checked = NULL);
    Tree *getCurrentTree(QString *name, bool* checked = NULL);

    Tree* getTree(unsigned int index, std::string* name = NULL, bool* checked = NULL);
    std::string getStringParameter(std::string parameterName);
    int getIntParameter(std::string parameterName);
    float getFloatParameter(std::string parameterName);

//    std::string getStringParameter(std::string methodName, std::string parameterName);
//    float getFloatParameter(std::string methodName, std::string parameterName);
//    int getIntParameter(std::string methodName, std::string parameterName);

    void appendImage(ImageWithInfo img, std::string name);
    void appendTree(Tree tree, std::string name, unsigned int color = 0xff0000);
    void appendTrees(std::vector<Tree> trees, std::vector<std::string> names, std::vector<unsigned int> colors);
    bool getCurrentFiducial(Byte3DImage::IndexType* fiducial);
    bool getFiducial(unsigned int index, Byte3DImage::IndexType* fiducial);
    bool crop(Byte3DImage::IndexType min, Byte3DImage::IndexType max);
    void createMethod(std::string name, std::string tooltip);
    void createParameter(std::string name, std::string value, std::string tooltip);

protected:
    void dragEnterEvent(QDragEnterEvent* event);
    void dragMoveEvent(QDragMoveEvent* event);
    void dragLeaveEvent(QDragLeaveEvent* event);
    void dropEvent(QDropEvent* event);


public slots:
    void processTimer(void);

//    void frameChanged(int);

private slots:
    void rendererSizeChanged(int);
    void pointedAt(int* coords);
    void rendererResizeEvent(QSize size);
    //void runNotImplemented();

//    void invert();
//    void gaussian();
//    void colorCombination();
//    void colorMultiplier();
//    void vesselness();
//    void multiscaleVesselness();
//    void floodFill();
//    void threshold();
//    void skeletonFromBinary();
//    void skeletonToTree();
//    void preciseBinaryToTree();
//    void whiteForegroundHoleFill();
//    void blackForegroundHoleFill();
//    void whiteTopHat();
//    void blackTopHat();
//    void median();
//    void dilate();
//    void erode();
//    void smoothTreeBranches();
//    void smoothDiameter();
//    void svdBasedDiameters();
//    void extendBranches();
//    void crop();
//    void levelSetChanVeseDense();
//    void levelSetChanVeseSparse();
//    void levelSetCurves();
//    void disjoinBranches();
//    void disjoinDisconnected();
//    void joinChecked();
//    void longBranchesFormat();
//    void shortBranchesFormat();


    void zoomSliderValueChanged(int value);



    void on_actionLoad_triggered();
    //void on_actionColor_triggered();
    void on_spinBoxMin_valueChanged(int val);
    void on_spinBoxMax_valueChanged(int val);
    void on_spinBox_X_valueChanged(int arg1);
    void on_spinBox_Y_valueChanged(int arg1);
    void on_spinBox_Z_valueChanged(int arg1);
    void on_checkBox_X_clicked(bool checked);
    void on_checkBox_Y_clicked(bool checked);
    void on_checkBox_Z_clicked(bool checked);
    void on_imageLevelsButton_clicked();
    void on_setAsRChannel_clicked();
    void on_setAsGChannel_clicked();
    void on_setAsBChannel_clicked();
    //void on_setAsAChannel_clicked();
    void on_buttonRemoveSeed_clicked();
    void on_removeBuffer_clicked();


    void on_imageOffButton_clicked(bool checked);
    void on_imageFooButton_clicked(bool checked);


    void on_actionSave_triggered();

    void on_actionSaveTree_triggered();

    void on_actionLoadTree_triggered();

    //void on_toggleView_clicked();

    void on_listWidgetTrees_itemClicked(QListWidgetItem *item);

    void on_listWidgetTrees_itemChanged(QListWidgetItem *item);


    void on_getMatrixButton_clicked();

    void on_setMatrixButton_clicked();


    void on_defaultMatrixButton_clicked();

    void on_removeTree_clicked();

    void on_toolButton_clicked();

    void on_toolButtonBalls_clicked();

    void on_actionExit_triggered();

    void on_stereoCombo_currentIndexChanged(int index);

    void on_stereoSlider_valueChanged(int value);

    void on_toolButtonWires_clicked();

    void on_toolButtonStream_clicked();

    void on_buttonImageInfo_clicked();

    void on_buttonColorPicker_clicked();

    void on_buttonIndicateNode_clicked();

    void on_buttonAddSeed_clicked();

    void on_actionTo_STL_triggered();

    void on_actionTo_VTK_triggered();

    void on_buttonSetVoxel_clicked(bool checked);

    void on_actionAbout_triggered();

    void on_actionContents_triggered();

    //void on_actionReload_raster_triggered();

    void on_actionBackground_color_triggered();

    void on_listWidgetBuffers_currentRowChanged(int currentRow);

    void on_frameSpinBox_valueChanged(int arg1);

    void on_playButton_clicked();

    void on_actionLoad_stack_triggered();

    void on_actionVoxel_size_triggered();

    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

    void on_treeWidget_itemChanged(QTreeWidgetItem *item, int column);

    void on_actionTree_to_Comsol_triggered();

    void on_actionAnimation_triggered();

    void on_actionAnimate_rotation_triggered();

private:
    QTimer timer;
    unsigned char greylevel;
    QString tempTreeFile;
    bool loadImage(QString fileName);

    void setColorToItemIcon(QListWidgetItem *item, QColor color);
    void setTemporatyFileNames(void);
    void deleteTemporatyFileNames(void);
    QString getParameter(std::string parameterName);

    unsigned int image_size[3];
    double voxel_spacing[3];
    std::vector<ImageWithInfo> images;
    std::vector<Tree> trees;
    std::vector<QRgb> treecolors;
    Ui::MainWindow *ui;
    ImageGl* renderer;

    bool loadImageItkFactory(const char* filename, ImageWithInfo* image);
    bool saveImageItkFactory(const char* filename, ImageWithInfo* image);
    void setCurrentBufferAsNthChannel(int i);
    void exportImportConversion(const char *mode, QString* inp, QString* out);
    //bool initTools(void);
    void updateImageIcons(void);
    QSlider zoomSlider;

//    VkTool* vktool;
//    int vktoolint;

    QTreeWidgetItem* methoditem;
};

#endif // MAINWINDOW_H
