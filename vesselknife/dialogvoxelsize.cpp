#include "dialogvoxelsize.h"
#include "ui_dialogvoxelsize.h"

DialogVoxelSize::DialogVoxelSize(double *voxels, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogVoxelSize)
{
    voxelsize = voxels;
    ui->setupUi(this);

    ui->voxelSizeX->setText(QString::number(voxelsize[0]));
    ui->voxelSizeY->setText(QString::number(voxelsize[1]));
    ui->voxelSizeZ->setText(QString::number(voxelsize[2]));

    //voxelsize[0] = ui->voxelSizeX->text().toDouble();
    // = getParameter(QString("multiscaleVesselness"), QString("MinStd")).toFloat();
}

DialogVoxelSize::~DialogVoxelSize()
{
    delete ui;
}

void DialogVoxelSize::on_voxelSizeX_editingFinished()
{
    voxelsize[0] = ui->voxelSizeX->text().toDouble();
    if(voxelsize[0] < 0.00000001) voxelsize[0] = 1.0;
    ui->voxelSizeX->setText(QString::number(voxelsize[0]));
}

void DialogVoxelSize::on_voxelSizeY_editingFinished()
{
    voxelsize[1] = ui->voxelSizeY->text().toDouble();
    if(voxelsize[1] < 0.00000001) voxelsize[1] = 1.0;
    ui->voxelSizeY->setText(QString::number(voxelsize[1]));
}

void DialogVoxelSize::on_voxelSizeZ_editingFinished()
{
    voxelsize[2] = ui->voxelSizeZ->text().toDouble();
    if(voxelsize[2] < 0.00000001) voxelsize[2] = 1.0;
    ui->voxelSizeZ->setText(QString::number(voxelsize[2]));
}
