#ifndef VKTOOL_H
#define VKTOOL_H

#include <string>
#include <vector>
#include "mainwindow.h"


class VkTool
{
public:
    virtual void initialize(void) = 0;
    virtual bool runMethod(void) = 0;

    //VkTool();
    //static VkTool* getObject(unsigned int i);
    std::string getObjectsName(void);
    //static std::string getObjectsName(unsigned int i);
//    static MainWindow* parent;

    //static std::vector< VkTool* > objectPointers;
protected:
    void createMethod(std::string tooltip);
    void createParameter(std::string name, std::string value, std::string tooltip = std::string());
    std::string getStringParameter(std::string parameterName);
    int getIntParameter(std::string parameterName);
    float getFloatParameter(std::string parameterName);

    bool getCurrentFiducial(Byte3DImage::IndexType* fiducial);
    bool getFiducial(unsigned int index, Byte3DImage::IndexType* fiducial);
    bool crop(Byte3DImage::IndexType min, Byte3DImage::IndexType max);

    ImageWithInfo* getImage(unsigned int index, std::string* name = NULL);
    ImageWithInfo* getCurrentImage(std::string* name = NULL);
    ImageWithInfo* getRedImage(std::string* name = NULL);
    ImageWithInfo* getGreenImage(std::string* name = NULL);
    ImageWithInfo* getBlueImage(std::string* name = NULL);
    ImageWithInfo* getAlphaImage(std::string* name = NULL);
    void appendImage(ImageWithInfo img, std::string name);

    Tree* getCurrentTree(std::string* name = NULL, bool* checked = NULL);
    Tree* getTree(unsigned int index, std::string* name = NULL, bool* checked = NULL);
    void appendTree(Tree tree, std::string name, unsigned int color = 0xff0000);
    void appendTrees(std::vector<Tree> trees, std::vector<std::string> names, std::vector<unsigned int> colors);

    std::string name;

};

#define CREATE_VKTOOL(ClassName)                \
class ClassName:VkTool                          \
{                                               \
public:                                         \
    ClassName(std::string name)                 \
    {\
        this->name = name;\
        objectPointers.push_back(this);         \
        printf("Constructor %i %s\n", (int) objectPointers.size(), name.c_str());\
        fflush(stdout);\
    }                                           \
    void initialize(void);                      \
    bool runMethod(void);                       \
};                                              \
ClassName object##ClassName(#ClassName);

#endif // VKTOOL_H
