#include "nodeproperties.h"
#include "ui_nodeproperties.h"


NodeProperties::NodeProperties(Tree* tree, Tree* extract, unsigned int index, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NodeProperties)
{
    ui->setupUi(this);

    this->tree = tree;
    this->extract = extract;
    this->index = index;

    NodeIn3D node = tree->node(index);
    if(node.connections >= 3)
    {
        ui->buttonRemove->setEnabled(false);
    }

    ui->lineEditI->setText(QString("%1").arg(index));
    ui->lineEditN->setText(QString("%1").arg(node.connections));
    ui->lineEditX->setText(QString("%1").arg(node.x));
    ui->lineEditY->setText(QString("%1").arg(node.y));
    ui->lineEditZ->setText(QString("%1").arg(node.z));
    ui->lineEditR->setText(QString("%1").arg(node.radius));
}

NodeProperties::~NodeProperties()
{
    delete ui;
}

void NodeProperties::on_buttonModify_clicked()
{
    NodeIn3D node = tree->node(index);
    node.x = ui->lineEditX->text().toDouble();
    node.y = ui->lineEditY->text().toDouble();
    node.z = ui->lineEditZ->text().toDouble();
    node.radius = ui->lineEditR->text().toDouble();
    tree->setNode(node, index);
    close();
}

void NodeProperties::on_bttonRemove_clicked()
{
    tree->removeNode(index);
    close();
}

void NodeProperties::on_buttonCut_clicked()
{
    tree->splitNode(index);
    tree->removeNode(index);
    close();
}

void NodeProperties::on_buttonRemove_clicked()
{
    unsigned int bcount = tree->count();
    for(unsigned int b = 0; b < bcount; b++)
    {
        unsigned int ncount = tree->count(b);
        for(unsigned int n = 0; n < ncount; n++)
        {
            if(index == tree->nodeIndex(b, n))
            {
                extract->addBranch(tree->branch(b));
                tree->removeBranch(b);
                close();
                return;
            }
        }
    }
}

void NodeProperties::on_buttonCancel_clicked()
{
    close();
}
