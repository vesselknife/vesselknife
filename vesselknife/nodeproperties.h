#ifndef NODEPROPERTIES_H
#define NODEPROPERTIES_H

#include <QDialog>

#include "../shared/tree.h"

namespace Ui {
class NodeProperties;
}

class NodeProperties : public QDialog
{
    Q_OBJECT

public:
    explicit NodeProperties(Tree *tree, Tree* extract, unsigned int index, QWidget *parent = 0);
    ~NodeProperties();

private slots:
    void on_buttonModify_clicked();

    void on_buttonRemove_clicked();

    void on_buttonCancel_clicked();

    void on_bttonRemove_clicked();

    void on_buttonCut_clicked();

private:
    Ui::NodeProperties *ui;
    Tree* tree;
    Tree* extract;
    unsigned int index;
};

#endif // NODEPROPERTIES_H
