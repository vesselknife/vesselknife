/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <QSpinBox>
#include <QComboBox>
#include "parametertreewidget.h"

ParameterTreeWidget::ParameterTreeWidget(QWidget *parent) : QTreeWidget(parent)
{
}
QModelIndex ParameterTreeWidget::parameterIndexFromItem(QTreeWidgetItem *item, int column)
{
    return indexFromItem(item, column);
}
QTreeWidgetItem* ParameterTreeWidget::parameterItemFromIndex(const QModelIndex &index)
{
    return itemFromIndex(index);
}
QString ParameterTreeWidget::getValue(QString name)
{
    QString ret;
    QStringList namelist = name.split(":");
    int level = 0;
    QTreeWidgetItem* item;
    int maxitems = topLevelItemCount();
    for(int i = 0; i < maxitems; i++)
    {
	item = topLevelItem(i);
	if(item == NULL) return ret;
	if(item->text(0) == namelist[level]) break;
    }
    level++;
    while(item != NULL && level < namelist.count())
    {
	int i;
	QTreeWidgetItem* itemt;
	maxitems = item->childCount();
	for(i = 0; i < maxitems; i++)
	{
	    itemt = item->child(i);
	    if(itemt == NULL) return ret;
	    if(itemt->text(0) == namelist[level])
	    {
		item = itemt;
		break;
	    }
	}
	if(i >= maxitems) return ret;
	level++;
    }
    ret = item->text(1);
    return ret;
}

QStringList ParameterTreeWidget::getNames(QString separator)
{
    QStringList ret;

    QTreeWidgetItem* item;
    int maxitems = topLevelItemCount();
    for(int i = 0; i < maxitems; i++)
    {
        item = topLevelItem(i);
        if(item == NULL) return ret;
        QString pref = item->text(0);
        getNamesRecursive(&pref, &separator, &ret, item);
    }
    return ret;
}

void ParameterTreeWidget::getNamesRecursive(QString* prefix, QString* separator, QStringList* namelist, QTreeWidgetItem* item)
{
    int maxitems = item->childCount();
    if(maxitems <= 0)
    {
        namelist->append(*prefix);
        return;
    }
    for(int i = 0; i < maxitems; i++)
    {
        QTreeWidgetItem* itemt = item->child(i);
        if(item != NULL)
        {
            QString pref = *prefix + *separator + itemt->text(0);
            getNamesRecursive(&pref, separator, namelist, itemt);
        }
    }
}

NoEditDelegate::NoEditDelegate(QObject* parent) : QStyledItemDelegate(parent)
{
}
QWidget* NoEditDelegate::createEditor(QWidget*, const QStyleOptionViewItem &, const QModelIndex &) const
{
    return 0;
}

EditDelegate::EditDelegate(QObject *parent, ParameterTreeWidget* tree)
    : QStyledItemDelegate(parent)
{
    this->tree = tree;
}

QWidget* EditDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QTreeWidgetItem* item = tree->parameterItemFromIndex(index);
    if(item != NULL)
    {
        QStringList valist = item->data(1, Qt::UserRole).toString().split(";");
        if(valist.size() >= 4)
        {
            if(valist[0] == "i")
            {
                QSpinBox *editor = new QSpinBox(parent);
                editor->setFrame(false);
                editor->setMinimum(valist[2].toInt());
                editor->setMaximum(valist[3].toInt());
                return editor;
            }
            else if(valist[0] == "t")
            {
                QComboBox *editor = new QComboBox(parent);
                editor->setFrame(true);
//                editor->setFrame(false);
//                editor->setAttribute(Qt::WA_TranslucentBackground, false);
                for(int i = 2; i < valist.size(); i++) editor->addItem(valist[i]);
                return editor;
            }
        }
    }
    return QStyledItemDelegate::createEditor(parent, option, index);
}

void EditDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QSpinBox *spinBox = qobject_cast <QSpinBox *>(editor);
    QComboBox *comboBox = qobject_cast <QComboBox *>(editor);
    if(spinBox != NULL)
    {
	int value = index.model()->data(index, Qt::EditRole).toInt();
	spinBox->setValue(value);
    }
    else if(comboBox != NULL)
    {
	QString value = index.model()->data(index, Qt::EditRole).toString();
    comboBox->setCurrentIndex(comboBox->findText(value)); // In Qt4 and Qt5
//	comboBox->setCurrentText(value); //In Qt5 only
    }
    else QStyledItemDelegate::setEditorData(editor, index);
}

void EditDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QSpinBox *spinBox = qobject_cast <QSpinBox *>(editor);
    if(spinBox != NULL)
    {
	spinBox->interpretText();
	int value = spinBox->value();
	model->setData(index, value, Qt::EditRole);
	return;
    }
    QStyledItemDelegate::setModelData(editor, model, index);
}

void EditDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}

