#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "nodeproperties.h"
#include "dialogvoxelsize.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <QFileDialog>
#include <QMessageBox>
#include <QColorDialog>
#include <QDropEvent>
#include <QMimeData>
#include <QDesktopServices>
#include <QUrl>
#include <QPushButton>
#include <QLinearGradient>
#include <QStringList>
#include <QWindow>
#include <QScreen>
#include <QGuiApplication>

#include <string>
#include <sstream>

//#include "../ITKIOFactoryRegistration/itkImageIOFactoryRegisterManager.h"
//#include "../ITKIOFactoryRegistration/itkTransformIOFactoryRegisterManager.h"
#include <itkImageIOFactory.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkCropImageFilter.h>

#include "../shared/mz2itk.h"
#include "../shared/filenameremap.h"

#include <itkVersion.h>
// #include <vtkVersion.h>


class VkTool;
MainWindow* parentWindow = NULL;
std::vector< VkTool* > objectPointers;
#include "vktools.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    renderer = new ImageGl(this);

    setCentralWidget(renderer);

//    initTools();

    connect(renderer, SIGNAL(zoomChanged(int)), this, SLOT(rendererSizeChanged(int)));
    connect(renderer, SIGNAL(pointedAt(int*)), this, SLOT(pointedAt(int*)));
    connect(renderer, SIGNAL(rendererResizeEvent(QSize)), this, SLOT(rendererResizeEvent(QSize)));

    zoomSlider.setOrientation(Qt::Horizontal);
    zoomSlider.setMinimumWidth(200);
    zoomSlider.setMinimum(-256);
    zoomSlider.setMaximum(256);
    zoomSlider.setValue(0);
    //zoomSlider.setMinimumHeight(14);
    zoomSlider.setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
    ui->statusBar->addPermanentWidget(&zoomSlider);
    connect(&zoomSlider, SIGNAL(valueChanged(int)), this, SLOT(zoomSliderValueChanged(int)));

    renderer->setZoom(zoomSlider.value());
    greylevel = 0;

    for(int f = 1; f < QApplication::arguments().size(); f++)
    {
        QString filename = QApplication::arguments()[f];

        if(!loadImage(filename))
        {
            Tree tree;
            if(tree.load(filename.toStdString().c_str()))
            {
                appendTree(tree, QFileInfo(filename).fileName().toStdString());
            }
        }
    }
    setTemporatyFileNames();

    ui->menuView->addSeparator();
    ui->menuView->addAction(ui->dockRasterData->toggleViewAction());
    ui->menuView->addAction(ui->dockWidgetRun->toggleViewAction());
    ui->menuView->addAction(ui->dockRendering->toggleViewAction());
    ui->menuView->addAction(ui->dockVectorData->toggleViewAction());
    ui->menuView->addSeparator();
    ui->menuView->addAction(ui->toolBar->toggleViewAction());

    connect(&timer, SIGNAL(timeout()), this, SLOT(processTimer()));


    parentWindow = this;
    methoditem = NULL;

//    printf("WinConstructor %i\n", objectPointers.size());
//    fflush(stdout);

    for(int i = 0; i < objectPointers.size(); i++)
    {

        objectPointers[i]->initialize();
        if(methoditem != NULL)
        {
            ui->treeWidget->addTopLevelItem(methoditem);

/////////////////////////////////////////BUTTON
//            QPushButton *button = new QPushButton("Execute");
//            button->setMaximumWidth(100);
//            button->setMinimumHeight(0);???
//            ui->treeWidget->setItemWidget(methoditem, 1, button);
/////////////////////////////////////////BUTTON
        }
        methoditem = NULL;
    }
//    ui->treeWidget->expandAll();
//Sets the first column of parameter tree not editable
    ui->treeWidget->setItemDelegateForColumn(0, new NoEditDelegate(this));
    ui->treeWidget->setItemDelegateForColumn(1, new EditDelegate(this, ui->treeWidget));

//    while((o = VkTool::getObject(i)) != NULL)
//    {
//        o->initialize();
//        if(methoditem != NULL) ui->treeWidget->addTopLevelItem(methoditem);
//        methoditem = NULL;
//        i++;
//    }

//Sets the first column of parameter tree not editable
    ui->treeWidget->setItemDelegateForColumn(0, new NoEditDelegate(this));

//    double ratio = 1.0;
//    ratio = QGuiApplication::primaryScreen()->devicePixelRatio(); //1.5
//    ratio = this->devicePixelRatio(); //1

////    QGuiApplication qga;
////    ratio = qga.devicePixelRatio();

//    ratio = qGuiApp->devicePixelRatio(); //1.5
////    QScreen* screen = ;
////    ratio = screen.devicePixelRatio();
//    QWindow* whnd = windowHandle(); //null
//    if(whnd != nullptr)
//        ratio = whnd->devicePixelRatio();

//    parentWidget();
//    parent;

//    ui->treeWidget->header()->resizeSection(0, 250);
}

MainWindow::~MainWindow()
{
    deleteTemporatyFileNames();
    delete ui;
}



void MainWindow::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    if (item == NULL) return;

    unsigned int index = ui->treeWidget->indexOfTopLevelItem(item);
    unsigned int size = objectPointers.size();
//    printf("Item clicked: %i %i\n", index, column);
//    fflush(stdout);

    if(column == 1 && index < size)
    {
        methoditem = item;
        QCursor oc = cursor();
        setCursor(Qt::WaitCursor);
        objectPointers[index]->runMethod();
        setCursor(oc);
    }
//    else if(column == 1)
//    {
//        QStringList valist = item->data(1, Qt::UserRole).toString().split(";");
//        if(valist.size() >= 3)
//        {
//            if(valist[0] == "t")
//            {
//                QMenu menu(ui->treeWidget);
//                for(int i = 2; i < valist.size(); i++)
//                {
//                    menu.addAction(new QAction(valist[i], this));
//                }
//                QRect r = ui->treeWidget->visualItemRect(item);
//                QPoint w = ui->treeWidget->mapToGlobal(r.bottomLeft());
//                w.setX(QCursor::pos().x());
//                QAction* a = menu.exec(w);//(QCursor::pos());
//                if(a != NULL) item->setText(1, a->text());
//            }
//        }
//    }
////    printf("User role: %s\n", item->data(1, Qt::UserRole).toString().toStdString().c_str());
////    fflush(stdout);
}


void MainWindow::dragEnterEvent(QDragEnterEvent* event)
{
    event->acceptProposedAction();
}

void MainWindow::dragMoveEvent(QDragMoveEvent* event)
{
    event->acceptProposedAction();
}

void MainWindow::dragLeaveEvent(QDragLeaveEvent* event)
{
    event->accept();
}

void MainWindow::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();

    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();

        if(urlList.size() >= 1)
        {
            for(QUrl ufile : urlList)
            {
                QCursor oc = cursor();
                setCursor(Qt::WaitCursor);
                QString filename = ufile.toLocalFile();
                if(loadImage(filename)) event->acceptProposedAction();
                else
                {
                    Tree tree;
                    if(tree.load(filename.toStdString().c_str()))
                    {
                        appendTree(tree, QFileInfo(filename).fileName().toStdString());
                        event->acceptProposedAction();
                    }
                }
                setCursor(oc);
            }
        }
    }
}


//bool MainWindow::loadTiffImages(QStringList fileNames)
//{
//}



bool MainWindow::loadImage(QString fileName)
{
    //Byte3DImage::Pointer img = Byte3DImage::New();
    ImageWithInfo img;
    //char* info = NULL;
    //if(loadNifti(fileName.toStdString().c_str(), img))
    if(loadImageItkFactory(fileName.toStdString().c_str(), &img))
    {

        //For tests
//                    img->voxelsize[0] = 3.5;
//                    img->voxelsize[1] = 3.5;
//                    img->voxelsize[2] = 3.5;

        if(images.size() > 0)
        {
            bool sizeok = true;
            for(unsigned int i = 0; i < 3; i++)
            {
                if(img.i->GetLargestPossibleRegion().GetSize()[i] != images[0].i->GetLargestPossibleRegion().GetSize()[i]) sizeok = false;
            }
            if(!sizeok)
            {
                //delete img;
                img.i = NULL; //the ITK smart pointer will determine whether the object should be "deleted".
                QMessageBox msgBox;
                msgBox.setText("Incorrect size");
                msgBox.setWindowTitle("Image info");
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.exec();
                return true;
            }

            if(images.size() > 0)
            {
                if( img.i->GetSpacing()[0] != images[0].i->GetSpacing()[0] ||
                    img.i->GetSpacing()[1] != images[0].i->GetSpacing()[1] ||
                    img.i->GetSpacing()[2] != images[0].i->GetSpacing()[2])
                {
                    QMessageBox msgBox;
                    msgBox.setText("Voxel size mismatch. Fix it?");
                    msgBox.setWindowTitle("Image info");
                    msgBox.setIcon(QMessageBox::Warning);

                    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Ignore | QMessageBox::Abort);
                    int ret = msgBox.exec();
                    switch(ret)
                    {
                    case QMessageBox::Yes:
                        img.i->SetSpacing(images[0].i->GetSpacing());
                        break;
                    case QMessageBox::Abort:
                        img.i = NULL; //the ITK smart pointer will determine whether the object should be "deleted".
                        return true;
                    }
                }
            }
        }

        image_size[0] = img.i->GetLargestPossibleRegion().GetSize()[0];
        image_size[1] = img.i->GetLargestPossibleRegion().GetSize()[1];
        image_size[2] = img.i->GetLargestPossibleRegion().GetSize()[2];
        voxel_spacing[0] = img.i->GetSpacing()[0];
        voxel_spacing[1] = img.i->GetSpacing()[1];
        voxel_spacing[2] = img.i->GetSpacing()[2];

        ui->horizontalSlider_X->setMaximum(image_size[0]-1);
        ui->spinBox_X->setMaximum(image_size[0]-1);
        ui->spinBox_X->setValue(image_size[0]/2);
        ui->horizontalSlider_Y->setMaximum(image_size[1]-1);
        ui->spinBox_Y->setMaximum(image_size[1]-1);
        ui->spinBox_Y->setValue(image_size[1]/2);
        ui->horizontalSlider_Z->setMaximum(image_size[2]-1);
        ui->spinBox_Z->setMaximum(image_size[2]-1);
        ui->spinBox_Z->setValue(image_size[2]/2);
//            ui->checkBox_X->setChecked(true);
//            ui->checkBox_Y->setChecked(true);
//            ui->checkBox_Z->setChecked(true);

        appendImage(img, QFileInfo(fileName).fileName().toStdString());

        unsigned char* imgdata = img.i->GetPixelContainer()->GetImportPointer();
        renderer->setImage(0, imgdata);
        renderer->setImage(1, imgdata);
        renderer->setImage(2, imgdata);
        //renderer->setImage(3, imgdata);
        renderer->setImageSizes(image_size, voxel_spacing);
        renderer->repaint();
        updateImageIcons();

        on_listWidgetTrees_itemChanged(NULL);
        return true;
    }
    else
    {
        img.i = NULL; //the ITK smart pointer will determine whether the object should be "deleted".
        return false;
    }
    return false;
}

void MainWindow::setTemporatyFileNames(void)
{
    QFile f;
    QString pathq = QDir::tempPath();
    std::string path = pathq.toStdString();
    tempTreeFile = QString::fromStdString(path);
    tempTreeFile += "/tmpTree.txt";
    f.setFileName(tempTreeFile);
    if(f.open(QFile::WriteOnly)) f.close();
}

void MainWindow::deleteTemporatyFileNames(void)
{
    QFile::remove(tempTreeFile);
}



void MainWindow::exportImportConversion(const char* mode, QString* inp, QString* out)
{
    QDir execDir(qApp->applicationDirPath());
    QString command = execDir.absolutePath() + QString("/vkconvert");
    QStringList arguments;
    arguments.append(QString("-m"));
    arguments.append(QString(mode));
    arguments.append(QString("-i"));
    arguments.append(*inp);
    arguments.append(QString("-o"));
    arguments.append(*out);
    QProcess process(this);
    if(!process.startDetached(command, arguments))
    {
        command = QString("vkconvert");
        if(!process.startDetached(command, arguments))
        {
            QMessageBox msgBox;
            msgBox.setText("Cannot find vkconvert program. Export or import failed.");
            msgBox.setWindowTitle("Export/import warning");
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
        }
    }
    ui->statusBar->showMessage(command);

    /*
    command += " -m ";
    command += mode;
    command += " -i ";
    command += inp;
    command += " -o ";
    command += out;
    QProcess process(this);
    if(!process.startDetached(command))
    {
        command = QString("vkconvert");
        command += " -m ";
        command += mode;
        command += " -i ";
        command += inp;
        command += " -o ";
        command += out;
        QProcess process(this);
        if(!process.startDetached(command))
        {
            QMessageBox msgBox;
            msgBox.setText("Cannot find vkconvert program. Export/import failed.");
            msgBox.setWindowTitle("Export/import warning");
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
        }
    }
    ui->statusBar->showMessage(command);
    */
}

void MainWindow::appendTree(Tree tree, std::string name, unsigned int color)
{
    trees.push_back(tree);
    treecolors.push_back(color);
    QListWidgetItem *item = new QListWidgetItem(ui->listWidgetTrees);
    item->setText(name.c_str());
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsUserCheckable);
    item->setCheckState(Qt::Checked);
    setColorToItemIcon(item, QColor(color));
    on_listWidgetTrees_itemChanged(NULL);
    int ile = trees.size();
    ui->listWidgetTrees->setCurrentRow(ile-1);
}

void MainWindow::appendTrees(std::vector<Tree> ntrees, std::vector<std::string> names, std::vector<unsigned int> colors)
{
    unsigned int num = ntrees.size();
    unsigned int numn = names.size();
    unsigned int numc = colors.size();
    int ile = trees.size();
    for(unsigned int n = 0; n < num; n++)
    {
        trees.push_back(ntrees[n]);
        treecolors.push_back(colors[n%numc]);
        QListWidgetItem *item = new QListWidgetItem(ui->listWidgetTrees);
        item->setText(names[n%numn].c_str());
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Checked);
        setColorToItemIcon(item, QColor(colors[n%numc]));
    }
    on_listWidgetTrees_itemChanged(NULL);
    ui->listWidgetTrees->setCurrentRow(ile);
}

void MainWindow::appendImage(ImageWithInfo img, std::string name)
{
    images.push_back(img);
    QListWidgetItem *item = new QListWidgetItem(ui->listWidgetBuffers);
    item->setText(name.c_str());
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
    updateImageIcons();
    int ile = images.size();
    ui->listWidgetBuffers->setCurrentRow(ile-1);
}

ImageWithInfo* MainWindow::getImage(unsigned int index, std::string* name)
{
    if(index >= images.size()) return NULL;
    if(name != NULL)
    {
        *name = ui->listWidgetBuffers->item(index)->text().toStdString();
    }
    return &images[index];
}

ImageWithInfo* MainWindow::getCurrentImage(int index, std::string* name)
{
    if(index >= 0 && index < 4)
    {
        unsigned char* cid = renderer->getImage(index);
        for(unsigned int d = 0; d < images.size(); d++)
            if(images[d].i->GetPixelContainer()->GetImportPointer() == cid)
            {
                if(name != NULL)
                {
                    *name = ui->listWidgetBuffers->item(d)->text().toStdString();
                }
                return &images[d];
            }
    }
    int ile = images.size();
    if(ile <= 0) return NULL;
    int row = ui->listWidgetBuffers->currentRow();
    if(row >= ile ||  row < 0)
    {
        ui->listWidgetBuffers->setCurrentRow(ile-1);
        row = ui->listWidgetBuffers->currentRow();
    }
    if(row >= ile || row < 0) return NULL;
    if(name != NULL)
    {
        *name = ui->listWidgetBuffers->item(row)->text().toStdString();
    }
    return &images[row];
}
ImageWithInfo* MainWindow::getCurrentImage(QString* name)
{
    int ile = images.size();
    if(ile <= 0) return NULL;
    int row = ui->listWidgetBuffers->currentRow();
    if(row >= ile ||  row < 0)
    {
        ui->listWidgetBuffers->setCurrentRow(ile-1);
        row = ui->listWidgetBuffers->currentRow();
    }
    if(row >= ile || row < 0) return NULL;
    if(name != NULL)
    {
        *name = ui->listWidgetBuffers->item(row)->text();
    }
    return &images[row];
}
Tree* MainWindow::getCurrentTree(std::string *name, bool* checked)
{
    int ile = trees.size();
    if(ile <= 0) return NULL;
    int row = ui->listWidgetTrees->currentRow();
    if(row >= ile ||  row < 0)
    {
        ui->listWidgetTrees->setCurrentRow(ile-1);
        row = ui->listWidgetTrees->currentRow();
    }
    if(row >= ile || row < 0) return NULL;
    if(name != NULL) *name = ui->listWidgetTrees->item(row)->text().toStdString();
    if(checked != NULL) *checked = (ui->listWidgetTrees->item(row)->checkState() == Qt::Checked);
    return &trees[row];
}
Tree* MainWindow::getCurrentTree(QString *name, bool* checked)
{
    int ile = trees.size();
    if(ile <= 0) return NULL;
    int row = ui->listWidgetTrees->currentRow();
    if(row >= ile ||  row < 0)
    {
        ui->listWidgetTrees->setCurrentRow(ile-1);
        row = ui->listWidgetTrees->currentRow();
    }
    if(row >= ile || row < 0) return NULL;
    if(name != NULL) *name = ui->listWidgetTrees->item(row)->text();
    if(checked != NULL) *checked = (ui->listWidgetTrees->item(row)->checkState() == Qt::Checked);
    return &trees[row];
}

Tree* MainWindow::getTree(unsigned int row, std::string* name, bool* checked)
{
    if(trees.size() <= row) return NULL;
    if(name != NULL) *name = ui->listWidgetTrees->item(row)->text().toStdString();
    if(checked != NULL) *checked = (ui->listWidgetTrees->item(row)->checkState() == Qt::Checked);
    return &trees[row];
}

bool MainWindow::crop(Byte3DImage::IndexType min, Byte3DImage::IndexType max)
{
    unsigned int nn = images.size();
    unsigned int n;
    typedef itk::CropImageFilter <Byte3DImage, Byte3DImage> CropImageFilterType;
    CropImageFilterType::SizeType smin, smax;

    int channels[4];
    for(n = 0; n < 4; n++)
    {
        channels[n] = -1;
        unsigned char* cid = renderer->getImage(n);
        for(unsigned int d = 0; d < images.size(); d++)
        if(images[d].i->GetPixelContainer()->GetImportPointer() == cid)
        {
            channels[n] = d;
        }
    }

    for(n = 0; n < Byte3DImage::ImageDimension; n++)
    {
        if(min[n] < 0 || min[n] >= image_size[n]) return false;
        if(max[n] < 0 || max[n] >= image_size[n]) return false;
        if(max[n] < min[n]) return false;
        smin[n] = min[n];
        smax[n] = image_size[n] - max[n] - 1;
    }
    for(n = 0; n < Byte3DImage::ImageDimension; n++)
    {
        image_size[n] = max[n] - min[n] + 1;
    }
    for(n = 0; n < nn; n++)
    {
        CropImageFilterType::Pointer cropFilter = CropImageFilterType::New();
        cropFilter->SetInput(images[n].i);
        cropFilter->SetLowerBoundaryCropSize(smin);
        cropFilter->SetUpperBoundaryCropSize(smax);
        cropFilter->Update();
        images[n].i = cropFilter->GetOutput();
//        printf("Image %i: %i %i %i\n",
//               n,
//               images[n].i->GetLargestPossibleRegion().GetSize()[0],
//               images[n].i->GetLargestPossibleRegion().GetSize()[1],
//               images[n].i->GetLargestPossibleRegion().GetSize()[2]
//              );
    }

//    printf("All   : %i %i %i\n",
//           image_size[0],
//           image_size[1],
//           image_size[2]
//          );
//    fflush(stdout);

    ui->horizontalSlider_X->setMaximum(image_size[0]-1);
    ui->spinBox_X->setMaximum(image_size[0]-1);
    ui->spinBox_X->setValue(image_size[0]/2);
    ui->horizontalSlider_Y->setMaximum(image_size[1]-1);
    ui->spinBox_Y->setMaximum(image_size[1]-1);
    ui->spinBox_Y->setValue(image_size[1]/2);
    ui->horizontalSlider_Z->setMaximum(image_size[2]-1);
    ui->spinBox_Z->setMaximum(image_size[2]-1);
    ui->spinBox_Z->setValue(image_size[2]/2);

    renderer->setImageSizes(image_size, voxel_spacing);
    for(n = 0; n < 3; n++)
    {
        if(channels[n] >= 0) renderer->setImage(n, images[channels[n]].i->GetPixelContainer()->GetImportPointer());
        else renderer->setImage(n, NULL);
    }
//    QCursor oc = cursor();
//    setCursor(Qt::WaitCursor);
    renderer->repaint();
//    setCursor(oc);

    updateImageIcons();
    on_listWidgetTrees_itemChanged(NULL);
    return true;
}

//bool MainWindow::saveNifti(const char* filename, Byte3DImage* image)
//{
//    int d;
//    nifti_image* nim = NULL;
//    int dim[8];
//    dim[0] = 3;
//    for(d = 0; d < 3; d++) dim[d+1] = image->size[d];
//    for(d = 4; d < 8; d++) dim[d] = 0;

//    nim = nifti_make_new_nim(dim, DT_UINT8, 1);
//    int linesnumber = 1;
//    for(d = 1; d < BI_DIMENSIONS; d++) linesnumber *= image->size[d];

//    nim->dx = image->voxelsize[0];
//    nim->dy = image->voxelsize[1];
//    nim->dz = image->voxelsize[2];
//    nim->nifti_type = 1; //nifti single file
////    char* name = new char[strlen(filename)+1];
////    strcpy(name, filename);
//    nifti_set_filenames(nim, filename, 0, 1);
//    if(strlen(nim->fname) > strlen(filename)) strcpy(nim->fname, filename);
////    nim->fname = name;
//    uint8 *dptr;
//    uint8 *ptr;
//    for(d = 0; d < linesnumber; d++)
//    {
//        dptr = ((uint8*)image->data) + (image->size[0] * d);
//        ptr = ((uint8*)nim->data) + (image->size[0] * d);
//        memcpy(ptr, dptr, image->size[0]);
//    }
//    nifti_image_write(nim);
//    nifti_image_free(nim);

//    return true;
//}


//bool MainWindow::loadNifti(const char* filename, Byte3DImage* image, unsigned int frame)
//{

//    unsigned int d;

//    if(image != NULL)
//    {

//        nifti_image* nim = NULL;
//        nim = nifti_image_read(filename, 1);
//        if(nim == NULL) return false;


//        //unsigned int selected = 0;
//        unsigned int sizemore = 1;
//        for(d = 3; d < nim->dim[0]; d++) sizemore *= nim->dim[d+1];

//        //if(frames != NULL) *frames = sizemore;
//        if(frame >= sizemore)
//        {
//            nifti_image_free(nim);
//            return false;   //Failed
//        }

///*
//        if(sizemore > 1)
//        {
//            ImageSelect dialog(nim->dim, &selected, this);
//            dialog.exec();
//            selected--;
//            if(selected >= sizemore)
//            {
//                nifti_image_free(nim);
//                return false;   //Failed
//            }
//        }
//*/

//        unsigned int size[BI_DIMENSIONS];
//        for(d = 0; d < nim->dim[0] && d < BI_DIMENSIONS; d++) size[d] = nim->dim[d+1];
//        for(; d < BI_DIMENSIONS; d++) size[d] = 1;
//        unsigned int sizeall = 1;
//        for(d = 0; d < BI_DIMENSIONS; d++) sizeall *= size[d];
//        unsigned int linesnumber = 1;
//        for(d = 1; d < BI_DIMENSIONS; d++) linesnumber *= size[d];

//        switch(nim->datatype)
//        {
//        case DT_UINT8:
//        case DT_INT16:
//        case DT_UINT16:
//        case DT_FLOAT32:
//        case DT_FLOAT64:
//        case DT_COMPLEX64:
//            image->allocate(size);
//            break;

////        case DT_RGB24:
////            image->allocate(size, MZPIXEL_RGB24);
////            break;

//        default:
//            nifti_image_free(nim);
//            return false;   //Failed
//        }

//        for(d = 0; d < nim->dim[0] && d < BI_DIMENSIONS; d++) image->voxelsize[d] = nim->pixdim[d+1];

//        switch(nim->datatype)
//        {
//        case DT_UINT8:
//            {
//                uint8 *dptr;
//                uint8 *ptr;
//                for(d = 0; d < linesnumber; d++)
//                {
//                    dptr = ((uint8*)image->data) + (image->size[0] * d);
//                    ptr = ((uint8*)nim->data) + frame*sizeall + (size[0] * d);
//                    memcpy(dptr, ptr, size[0]);
//                }
//            }
//            //memcpy(image->data, nim->data, sizeall);
//            break;

//        case DT_INT16:
//        {
//            uint8 *dptr;
//            int16 *ptr = (int16*)nim->data + frame*sizeall;
//            double min = *ptr;
//            double max = *ptr;
//            for(d = 0; d < linesnumber; d++)
//            {
//                ptr = ((int16*)nim->data) + frame*sizeall + (size[0] * d);
//                for(unsigned int x = 0; x < size[0]; x++)
//                {
//                    if(min > *ptr) min = *ptr;
//                    if(max < *ptr) max = *ptr;
//                    ptr++;
//                }
//            }
//            if(max > min)
//            {
//                max -= min;
//                max /= 255.9;
//                for(d = 0; d < linesnumber; d++)
//                {
//                    dptr = ((uint8*)image->data) + (image->size[0] * d);
//                    ptr = ((int16*)nim->data) + frame*sizeall + (size[0] * d);
//                    for(unsigned int x = 0; x < size[0]; x++)
//                    {
//                        *dptr = (*ptr-min)/max;
//                        ptr++; dptr++;
//                    }
//                }
//            }
//        }
//        break;
//        case DT_UINT16:
//        {
//            uint8 *dptr;
//            uint16 *ptr = (uint16*)nim->data + frame*sizeall;
//            double min = *ptr;
//            double max = *ptr;
//            for(d = 0; d < linesnumber; d++)
//            {
//                ptr = ((uint16*)nim->data) + frame*sizeall + (size[0] * d);
//                for(unsigned int x = 0; x < size[0]; x++)
//                {
//                    if(min > *ptr) min = *ptr;
//                    if(max < *ptr) max = *ptr;
//                    ptr++;
//                }
//            }
//            if(max > min)
//            {
//                max -= min;
//                max /= 255.9;
//                for(d = 0; d < linesnumber; d++)
//                {
//                    dptr = ((uint8*)image->data) + (image->size[0] * d);
//                    ptr = ((uint16*)nim->data) + frame*sizeall + (size[0] * d);
//                    for(unsigned int x = 0; x < size[0]; x++)
//                    {
//                        *dptr = (*ptr-min)/max;
//                        ptr++; dptr++;
//                    }
//                }
//            }
//        }
//        break;
//        case DT_FLOAT32:
//        {
//            uint8 *dptr;
//            float *ptr = (float*)nim->data + frame*sizeall;
//            float min = *ptr;
//            float max = *ptr;
//            for(d = 0; d < linesnumber; d++)
//            {
//                ptr = ((float*)nim->data) + frame*sizeall + (size[0] * d);
//                for(unsigned int x = 0; x < size[0]; x++)
//                {
//                    if(min > *ptr) min = *ptr;
//                    if(max < *ptr) max = *ptr;
//                    ptr++;
//                }
//            }
//            if(max > min)
//            {
//                max -= min;
//                max /= 255.9;
//                for(d = 0; d < linesnumber; d++)
//                {
//                    dptr = ((uint8*)image->data) + (image->size[0] * d);
//                    ptr = ((float*)nim->data) + frame*sizeall + (size[0] * d);
//                    for(unsigned int x = 0; x < size[0]; x++)
//                    {
//                        *dptr = (*ptr-min)/max;
//                        ptr++; dptr++;
//                    }
//                }
//            }
//        }
//        break;
//        case DT_FLOAT64:
//            {
//                uint8 *dptr;
//                double *ptr = (double*)nim->data + frame*sizeall;
//                double min = *ptr;
//                double max = *ptr;
//                for(d = 0; d < linesnumber; d++)
//                {
//                    ptr = ((double*)nim->data) + frame*sizeall + (size[0] * d);
//                    for(unsigned int x = 0; x < size[0]; x++)
//                    {
//                        if(min > *ptr) min = *ptr;
//                        if(max < *ptr) max = *ptr;
//                        ptr++;
//                    }
//                }
//                if(max > min)
//                {
//                    max -= min;
//                    max /= 255.9;
//                    for(d = 0; d < linesnumber; d++)
//                    {
//                        dptr = ((uint8*)image->data) + (image->size[0] * d);
//                        ptr = ((double*)nim->data) + frame*sizeall + (size[0] * d);
//                        for(unsigned int x = 0; x < size[0]; x++)
//                        {
//                            *dptr = (*ptr-min)/max;
//                            ptr++; dptr++;
//                        }
//                    }
//                }
//            }
//            break;

//        case DT_COMPLEX64:
//        {
//            uint8 *dptr;
//            float *ptr = (float*)nim->data + frame*sizeall;
//            float min = *ptr;
//            float max = *ptr;
//            for(d = 0; d < linesnumber; d++)
//            {
//                ptr = ((float*)nim->data) + frame*sizeall + (size[0] * d);
//                for(unsigned int x = 0; x < size[0]; x++)
//                {
//                    float re = *ptr; ptr++;
//                    float im = *ptr; ptr++;
//                    float mag = sqrt(re*re + im*im);
//                    if(min > mag) min = mag;
//                    if(max < mag) max = mag;
//                }
//            }
//            if(max > min)
//            {
//                max -= min;
//                max /= 255.9;
//                for(d = 0; d < linesnumber; d++)
//                {
//                    dptr = ((uint8*)image->data) + (image->size[0] * d);
//                    ptr = ((float*)nim->data) + frame*sizeall + (size[0] * d);
//                    for(unsigned int x = 0; x < size[0]; x++)
//                    {
//                        float re = *ptr; ptr++;
//                        float im = *ptr; ptr++;
//                        float mag = sqrt(re*re + im*im);
//                        *dptr = (mag-min)/max; dptr++;
//                    }
//                }
//            }
//        }
//        break;
//        case DT_RGB24:
//            {
//                uint8 *dptr;
//                uint8 *ptr = (uint8*)nim->data + frame*sizeall;
//                for(d = 0; d < linesnumber; d++)
//                {
//                    dptr = ((uint8*)image->data) + (image->size[0] * d);
//                    ptr = ((uint8*)nim->data) + frame*sizeall + (size[0] * d);
//                    for(unsigned int x = 0; x < size[0]; x++)
//                    {
//                        *dptr = *ptr; dptr++;
//                        *dptr = *ptr; dptr++;
//                        *dptr = *ptr; dptr++;
//                    }
//                }
//            }
//            break;

//        default:
//            nifti_image_free(nim);
//            return false;   //Failed
//        }

//        std::stringstream ss;
//        ss << "File: " << filename << std::endl;

//        ss << "Format: ";
//        switch(nim->nifti_type)
//        {
//        case 0: ss << "Analyze"; break;
//        case 1: ss << "Nifti-1 (single file)"; break;
//        case 2: ss << "Nifti-1 (two files)"; break;
//        case 3: ss << "Nifti-ASCII"; break;
//        default: ss << "?";
//        }
//        ss << std::endl;

//        ss << "Raster: ";
//        for(d = 1; (int)d <= nim->dim[0]; d++)
//        {
//            ss << nim->dim[d];
//            if((int)d < nim->dim[0]) ss << "x";
//        }
//        ss << std::endl;
//        if(sizemore > 1)
//        {
//            ss << "Frame: ";
//            ss << frame << " of " << sizemore;
//            ss << std::endl;
//        }

//        ss << "Original voxel type: ";
//        switch(nim->datatype)
//        {
//        case DT_UINT8: ss << "uint8"; break;
//        case DT_INT16: ss << "int16"; break;
//        case DT_UINT16: ss << "uint16"; break;
//        case DT_FLOAT32: ss << "float32"; break;
//        case DT_FLOAT64: ss << "float64"; break;
//        case DT_COMPLEX64: ss << "complex64"; break;
//        case DT_RGB24:  ss << "rgb24"; break;
//        default: ss << "?"; break;
//        }
//        ss << std::endl;
//        ss << "Stored voxel type: uint8";
//        ss << std::endl;

//        ss << "Voxel size: ";
//        for(d = 1; (int)d <= nim->dim[0]; d++)
//        {
//            ss << nim->pixdim[d];
//            if((int)d < nim->dim[0]) ss << "x";
//        }
//        ss << std::endl;

//        ss << "Space units: ";
//        switch(nim->xyz_units)
//        {
//        case NIFTI_UNITS_METER: ss << "m"; break;
//        case NIFTI_UNITS_MM: ss << "mm"; break;
//        case NIFTI_UNITS_MICRON: ss << "um"; break;
//        default: ss << "?";
//        }
//        ss << std::endl;
//        ss << "Time units: ";
//        switch(nim->time_units)
//        {
//        case NIFTI_UNITS_SEC: ss << "s"; break;
//        case NIFTI_UNITS_MSEC: ss << "ms"; break;
//        case NIFTI_UNITS_USEC: ss << "us"; break;
//        default: ss << "?";
//        }
//        ss << std::endl;

//        ss << "Slope: " << nim->scl_slope << std::endl;
//        ss << "Intercept: " << nim->scl_inter << std::endl;
//        ss << "Calibration minimum : " << nim->cal_min << std::endl;
//        ss << "Calibration maximum: " << nim->cal_max << std::endl;

//        if(image->info != NULL) delete[] image->info;
//        image->info = new char[ss.str().length()+2];
//        strcpy(image->info, ss.str().c_str());

//        if(image->filePathAndName != NULL) delete[] image->filePathAndName;
//        image->filePathAndName = new char[strlen(filename)+2];
//        strcpy(image->filePathAndName, filename);
//        image->framesInFile = sizemore;
//        image->currentFrame = frame;

///*
////Frame arround for testing
//        unsigned int x[3];
//        x[0] = 0; x[1] = 0;
//        for(x[2] = 0; x[2] < image->size[2]; x[2]++)  *image->pixel(x) = 255;
//        x[0] = 0; x[1] = image->size[1]-1;
//        for(x[2] = 0; x[2] < image->size[2]; x[2]++)  *image->pixel(x) = 255;
//        x[0] = image->size[0]-1; x[1] = 0;
//        for(x[2] = 0; x[2] < image->size[2]; x[2]++)  *image->pixel(x) = 255;
//        x[0] = image->size[0]-1; x[1] = image->size[1]-1;
//        for(x[2] = 0; x[2] < image->size[2]; x[2]++)  *image->pixel(x) = 255;

//        x[2] = 0; x[0] = 0;
//        for(x[1] = 0; x[1] < image->size[1]; x[1]++)  *image->pixel(x) = 255;
//        x[2] = 0; x[0] = image->size[0]-1;
//        for(x[1] = 0; x[1] < image->size[1]; x[1]++)  *image->pixel(x) = 255;
//        x[2] = image->size[2]-1; x[0] = 0;
//        for(x[1] = 0; x[1] < image->size[1]; x[1]++)  *image->pixel(x) = 255;
//        x[2] = image->size[2]-1; x[0] = image->size[0]-1;
//        for(x[1] = 0; x[1] < image->size[1]; x[1]++)  *image->pixel(x) = 255;

//        x[1] = 0; x[2] = 0;
//        for(x[0] = 0; x[0] < image->size[0]; x[0]++)  *image->pixel(x) = 255;
//        x[1] = 0; x[2] = image->size[2]-1;
//        for(x[0] = 0; x[0] < image->size[0]; x[0]++)  *image->pixel(x) = 255;
//        x[1] = image->size[1]-1; x[2] = 0;
//        for(x[0] = 0; x[0] < image->size[0]; x[0]++)  *image->pixel(x) = 255;
//        x[1] = image->size[1]-1; x[2] = image->size[2]-1;
//        for(x[0] = 0; x[0] < image->size[0]; x[0]++)  *image->pixel(x) = 255;
//*/

//        nifti_image_free(nim);
//        return true;
//    }

//    return false;
//}


void MainWindow::rendererSizeChanged(int zoomlevel)
{
//    ui->horizontalSliderZoom->blockSignals(true);
//    ui->horizontalSliderZoom->setValue(zoomlevel);
//    ui->horizontalSliderZoom->blockSignals(false);
    zoomSlider.blockSignals(true);
    zoomSlider.setValue(-zoomlevel);
    zoomSlider.blockSignals(false);

}

void MainWindow::zoomSliderValueChanged(int value)
{
    renderer->setZoom(-value);
}

void MainWindow::updateImageIcons(void)
{
    int ile = images.size();
    int cnt = ui->listWidgetBuffers->count();
    if(cnt < ile) ile = cnt;
    for(int i = 0; i < ile; i++)
    {
        ImageWithInfo image = images[i];
        char patt[8];
        patt[4] = 0;
        for(int c = 0; c < 3; c++)
        {
            if(image.i->GetPixelContainer()->GetImportPointer() == renderer->getImage(c)) patt[c] = '1';
            else patt[c] = '0';
        }
        patt[3] = '0';
        char text[64];
        sprintf(text, ":/icons/icons/rgba_icon_%s.png", patt);
        ui->listWidgetBuffers->item(i)->setIcon(QIcon(text));
    }
}

void MainWindow::setCurrentBufferAsNthChannel(int i)
{
    ImageWithInfo* mi = getCurrentImage();
    if(mi == NULL) return;
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

    if(renderer->isAlreadySetImage(i, mi->i->GetPixelContainer()->GetImportPointer()))
    {
        renderer->setImage(i, nullptr);
    }
    else
    {
        renderer->setImage(i, mi->i->GetPixelContainer()->GetImportPointer());
    }
    renderer->repaint();
    setCursor(oc);
    updateImageIcons();
}

void MainWindow::pointedAt(int* coords)
{
    if(ui->buttonAddSeed->isChecked())
    {
        if(renderer != NULL)
        {
            int output[3];
            if(renderer->Coordinates2Dto3D(coords, output))
            {
                QListWidgetItem *item = new QListWidgetItem(ui->listWidgetSeed);
                item->setText(QString("%1 %2 %3").arg(output[0]).arg(output[1]).arg(output[2]));
                item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
            }
        }
        ui->buttonAddSeed->setChecked(false);
        int ile = ui->listWidgetSeed->count();
        ui->listWidgetSeed->setCurrentRow(ile-1);
        return;
    }

    if(ui->buttonColorPicker->isChecked())
    {
        if(renderer != NULL)
        {
            int output[3];
            if(renderer->Coordinates2Dto3D(coords, output))
            {
                Byte3DImage::IndexType pixelIndex;
                pixelIndex[0] = output[0];
                pixelIndex[1] = output[1];
                pixelIndex[2] = output[2];
                ImageWithInfo* mmi = getCurrentImage();
                if(mmi != NULL)
                {
                    unsigned char* mi = mmi->i->GetPixelContainer()->GetImportPointer();
                    if((renderer->getImage(0) == mi || renderer->getImage(1) == mi || renderer->getImage(2) == mi) &&
                       (unsigned int)output[0] < image_size[0] && (unsigned int)output[1] < image_size[1] && (unsigned int)output[2] < image_size[2])
                    {
                        greylevel = mmi->i->GetPixel(pixelIndex);
                        ui->statusBar->showMessage(QString("(%1 %2 %3) %4").arg(output[0]).arg(output[1]).arg(output[2]).arg((int)greylevel));
                        return;
                    }
                }
                ui->statusBar->showMessage("Select image from the list");
            }
        }
        ui->buttonColorPicker->setChecked(false);
    }

    if(ui->buttonSetVoxel->isChecked())
    {
        if(renderer != NULL)
        {
            int output[3];
            if(renderer->Coordinates2Dto3D(coords, output))
            {
                Byte3DImage::IndexType pixelIndex;
                pixelIndex[0] = output[0];
                pixelIndex[1] = output[1];
                pixelIndex[2] = output[2];
                ImageWithInfo* mmi = getCurrentImage();
                if(mmi != NULL)
                {
                    unsigned char* mi = mmi->i->GetPixelContainer()->GetImportPointer();
                    if((renderer->getImage(0) == mi || renderer->getImage(1) == mi || renderer->getImage(2) == mi) &&
                       (unsigned int)output[0] < image_size[0] && (unsigned int)output[1] < image_size[1] && (unsigned int)output[2] < image_size[2])
                    {
                        greylevel = mmi->i->GetPixel(pixelIndex);
                        ui->statusBar->showMessage(QString("(%1 %2 %3)").arg(output[0]).arg(output[1]).arg(output[2]));
                        renderer->update();
                        return;
                    }
                }
                ui->statusBar->showMessage("Tree must be checked on the list");
            }
        }
        return;
    }

    if(ui->buttonIndicateNode->isChecked())
    {
        if(renderer != NULL)
        {
            unsigned int ile = trees.size() < (unsigned int) ui->listWidgetTrees->count() ? trees.size() : ui->listWidgetTrees->count();
            //Tree* besttree  = NULL;
            Tree* curtree = getCurrentTree();
            unsigned int bestnode  = -1;
            unsigned int bestrow  = -1;
            double bestdistance = 100;

            if(! renderer->Coordinates3Dto2D(NULL, NULL, true)) return;
            for(unsigned int row = 0; row < ile; row++)
            {
                if(ui->listWidgetTrees->item(row)->checkState() == Qt::Checked)
                {
                    Tree* tree = &trees[row];
                    unsigned int nodecount = tree->nodeCount();
                    for(unsigned int nodei = 0; nodei < nodecount; nodei++)
                    {
                        double input[3];
                        double output[3];
                        NodeIn3D node = tree->node(nodei);
                        input[0] = node.x;
                        input[1] = node.y;
                        input[2] = node.z;
                        if(renderer->Coordinates3Dto2D(input, output, false))
                        {
                            double xx = coords[0]-output[0];
                            double yy = coords[1]-output[1];
                            double dd = xx*xx+yy*yy;
                            if(dd < bestdistance || (dd <= bestdistance && tree == curtree))
                            {
                                //besttree = tree;
                                bestrow = row;
                                bestnode = nodei;
                                bestdistance = dd;
                            }
                        }
                    }
                }
            }
            if(bestnode != (unsigned int)-1)
            {
                Tree* tree = &trees[bestrow];
                NodeIn3D node = tree->node(bestnode);

                ui->statusBar->showMessage(QString("(%1 %2 %3) ~%4)").arg(node.x).arg(node.y).arg(node.z).arg(sqrt(bestdistance)));
//                ui->statusBar->showMessage(QString("(%1 %2) d=%3)").arg(ui->listWidgetTrees->item(bestrow)->text()).arg(bestnode).arg(sqrt(bestdistance)));
//                Tree* tree = &trees[bestrow];
//                NodeIn3D node = tree->node(bestnode);
//                node.radius = 1.5;
//                tree->setNode(node, bestnode);

                Tree extracted;
                NodeProperties dialog(&trees[bestrow], &extracted, bestnode, this);
                dialog.exec();
                if(extracted.count() > 0) appendTree(extracted, "manualyExtracted", 0xff00);
                else on_listWidgetTrees_itemChanged(NULL);
            }
        }
        ui->buttonIndicateNode->setChecked(false);
    }

}

QString MainWindow::getParameter(std::string parameterName)
{
    QList<QTreeWidgetItem*> items = ui->treeWidget->findItems(QString(parameterName.c_str()), Qt::MatchStartsWith | Qt::MatchRecursive, 0);
    int count = items.count();
    for(int n = 0; n < count; n++)
    {
        if(items[n]->text(0).toStdString() == parameterName)
        {
            QTreeWidgetItem* parent = items[n]->parent();
            if(parent == methoditem)
                    return items[n]->text(1);
        }
    }
    QMessageBox msgBox;
    msgBox.setText(QString("Parameter ") + methoditem->text(0) + "-" + QString(parameterName.c_str()) + " not found");
    msgBox.setWindowTitle("Error");
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.exec();
    return QString("");
}

std::string MainWindow::getStringParameter(std::string parameterName)
{
    return getParameter(parameterName).toStdString();
}
int MainWindow::getIntParameter(std::string parameterName)
{
    return getParameter(parameterName).toInt();
}
float MainWindow::getFloatParameter(std::string parameterName)
{
    return getParameter(parameterName).toFloat();
}

bool MainWindow::getCurrentFiducial(Byte3DImage::IndexType* fiducial)
{
    if(fiducial == NULL) return false;
    int ile = ui->listWidgetSeed->count();
    if(ile <= 0) return NULL;
    QListWidgetItem* item = ui->listWidgetSeed->currentItem();
    if(item == NULL)
    {
        ui->listWidgetSeed->setCurrentRow(ile-1);
        item = ui->listWidgetSeed->currentItem();
    }
    if(item == NULL) return false;
    QStringList list = item->text().split(" ");
    if(list.size() >= 3)
    {
        (*fiducial)[0] = list[0].toInt();
        (*fiducial)[1] = list[1].toInt();
        (*fiducial)[2] = list[2].toInt();
        return true;
    }
    else return false;
}
bool MainWindow::getFiducial(unsigned int index, Byte3DImage::IndexType* fiducial)
{
    if(fiducial == NULL) return false;
    if(ui->listWidgetSeed->count() <= (int)index) return false;
    QListWidgetItem* item = ui->listWidgetSeed->item(index);
    if(item == NULL) return false;
    QStringList list = item->text().split(" ");
    if(list.size() >= 3)
    {
        (*fiducial)[0] = list[0].toInt();
        (*fiducial)[1] = list[1].toInt();
        (*fiducial)[2] = list[2].toInt();
        return true;
    }
    else return false;
}

#if defined _MZ_UTF8_TO_SHORT_WORKAROUND
    bool MainWindow::loadImageItkFactory(const char* filename8, ImageWithInfo* imageWithInfo)
    {
        std::string fn_std_string = _mz_utf8_to_short_for_reading(filename8);
        const char* filename = fn_std_string.c_str();
#else
    bool MainWindow::loadImageItkFactory(const char* filename, ImageWithInfo* imageWithInfo)
    {
#endif

//    Byte3DImage::Pointer image = imageWithInfo.i;
    std::stringstream ss;
    itk::ImageIOBase::IOComponentType type;
    size_t numDimensions;
    itk::ImageIOBase::Pointer imageIO = itk::ImageIOFactory::CreateImageIO(filename, itk::ImageIOFactory::ReadMode);
    if( !imageIO ) return false;
    imageIO->SetFileName(filename);
    imageIO->ReadImageInformation();
    type = imageIO->GetComponentType();
    numDimensions =  imageIO->GetNumberOfDimensions();

    ss << "File name: " << filename << std::endl;
    typedef itk::Image< unsigned char, 3> ImageType;

    if(type == itk::ImageIOBase::UCHAR || type == itk::ImageIOBase::CHAR)
    {
        typedef itk::ImageFileReader<ImageType> ReaderType;
        ReaderType::Pointer reader = ReaderType::New();
        reader->SetFileName(filename);
        try
        {
            reader->Update();
        }
        catch (itk::ExceptionObject &ex)
        {
            std::cout << ex << std::endl;
            return false;
        }
        ImageType::Pointer ii = reader->GetOutput();
        Mz2Itk<unsigned char>::getMZ(&(imageWithInfo->i), ii);

        if(type == itk::ImageIOBase::CHAR)
        {
            unsigned int d;
            size_t max = 1;
            for(d = 0; d < 3; d++) max *= imageWithInfo->i->GetLargestPossibleRegion().GetSize()[d];
            unsigned char* ptro = imageWithInfo->i->GetPixelContainer()->GetImportPointer();
            for(d = 0; d < max; d++, ptro++) *ptro = *ptro + 128;
        }
        typedef itk::ImageIOBase::IOComponentType ScalarPixelType;
        const ScalarPixelType pixelType = reader->GetImageIO()->GetComponentType();
        ss << "Original voxel type: " << reader->GetImageIO()->GetComponentTypeAsString(pixelType) << std::endl;
        ss << "Dimensions: " << reader->GetImageIO()->GetNumberOfDimensions() << std::endl;
        ss << "Image size: " << ii->GetLargestPossibleRegion().GetSize() << std::endl;
        ss << "Voxel spacing: " << ii->GetSpacing() << std::endl;
    }
    else
    {
        typedef itk::Image< float, 3> ImageTypeF;
        typedef itk::ImageFileReader<ImageTypeF> ReaderType;
        ReaderType::Pointer reader = ReaderType::New();
        reader->SetFileName(filename);
        try
        {
            reader->Update();
        }
        catch (itk::ExceptionObject &ex)
        {
            std::cout << ex << std::endl;
            return false;
        }
        ImageTypeF::Pointer ii = reader->GetOutput();
        float min, max;
        Mz2Itk<float>::getMZNormalized(&(imageWithInfo->i), ii, &min, &max);
        typedef itk::ImageIOBase::IOComponentType ScalarPixelType;
        const ScalarPixelType pixelType = reader->GetImageIO()->GetComponentType();
        ss << "Original voxel type: " << reader->GetImageIO()->GetComponentTypeAsString(pixelType) << std::endl;
        ss << "Mapped from <" << min << ", " << max << "> to <0, 255>" << std::endl;
        ss << "Dimensions: " << reader->GetImageIO()->GetNumberOfDimensions() << std::endl;
        ss << "Image size: " << ii->GetLargestPossibleRegion().GetSize() << std::endl;
        ss << "Voxel spacing: " << ii->GetSpacing() << std::endl;
    }

    imageWithInfo->t = ss.str();
    return true;
}

bool MainWindow::saveImageItkFactory(const char* filename, ImageWithInfo* iwi)
{
    typedef  itk::ImageFileWriter< Byte3DImage > WriterType;
    WriterType::Pointer writer = WriterType::New();
#if defined _MZ_UTF8_TO_SHORT_WORKAROUND
        writer->SetFileName(_mz_utf8_to_short_for_writing(filename));
#else
        writer->SetFileName(filename);
#endif
    writer->SetInput(iwi->i);
    try
    {
        writer->Update();
    }
    catch (itk::ExceptionObject &ex)
    {
        std::cout << ex << std::endl;
        return false;
    }
    return true;
}

void MainWindow::createMethod(std::string name, std::string tooltip)
{
    methoditem = new QTreeWidgetItem();
    methoditem->setText(0, name.c_str());
    methoditem->setToolTip(0, tooltip.c_str());
    methoditem->setText(1, "Execute");
    methoditem->setIcon(1, QIcon(":/icons/icons/media-playback-start.png"));

//    QLinearGradient gradient(20, 0, 100, 50);
////    QRadialGradient gradient(50, 50, 50, 50, 50);
//    gradient.setColorAt(0, Qt::white);
//    gradient.setColorAt(1, Qt::red);
//    QBrush brush(gradient);
//    QBrush brush(palette().color(QPalette::Background));
//    methoditem->setBackground(1, brush);
    methoditem->setToolTip(1, "Click to run the method");


//    QWidget *buttonWidget = new QWidget();
//    QHBoxLayout *layout = new QHBoxLayout(buttonWidget);
//    layout->setContentsMargins(0, 0, 0, 0); // Remove margins
//    QPushButton *button = new QPushButton("Action");
//    //layout->addWidget(button);
//    methoditem->treeWidget()->setItemWidget(methoditem, 1, button);


}

void MainWindow::createParameter(std::string name, std::string value, std::string tooltip)
{
    if(methoditem == NULL) throw;
    QTreeWidgetItem* parameteritem = new QTreeWidgetItem();
    parameteritem->setText(0, name.c_str());
    parameteritem->setToolTip(0, tooltip.c_str());
    parameteritem->setToolTip(1, tooltip.c_str());
    parameteritem->setData(1, Qt::UserRole, QString(value.c_str()));
    QString val = value.c_str();
    QStringList valist = val.split(";");
    if(valist.size() > 1)
    {
        if(valist.size() >= 2)
        {
            parameteritem->setText(1, valist[1]);
        }
    }
    else parameteritem->setText(1, value.c_str());

    parameteritem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsEditable);
    methoditem->addChild(parameteritem);
}


void MainWindow::rendererResizeEvent(QSize size)
{
    ui->statusBar->showMessage(QString("Viewport: %1×%2").arg(size.width()).arg(size.height()), 2000);
}
