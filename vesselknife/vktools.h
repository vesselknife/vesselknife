#ifndef VKTOOLS_H
#define VKTOOLS_H

#ifdef VESSELKNIFE_CONSOLE
#include "../vk4console/vktoolc.h"
#else
#include "vktool.h"
#endif

#include "../shared/imagefilters.h"

CREATE_VKTOOL(Cropping)
void Cropping::initialize(void)
{
    createMethod("Crops all images. Fiducial points define borders.");
}
bool Cropping::runMethod()
{
    unsigned int i = 0;
    Byte3DImage::IndexType f;
    Byte3DImage::IndexType min, max;

    if(getFiducial(i, &f))
    {
        min = f; max = f;
        i++;
        while(getFiducial(i, &f))
        {
            for(unsigned int d = 0; d < 3; d++)
            {
                if(min[d] > f[d]) min[d] = f[d];
                if(max[d] < f[d]) max[d] = f[d];
            }
            i++;
        }
    //    for(unsigned int d = 0; d < 3; d++)
    //    {
    //        if(min[d] > max[d]) return false;
    //    }
        return crop(min, max);
    }
    return false;
}


#include <itkInvertIntensityImageFilter.h>
CREATE_VKTOOL(Inversion)
void Inversion::initialize(void)
{
    createMethod("Computes negative - inverts image gray scale");
}
bool Inversion::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL) return false;
    Byte3DImage::Pointer mi = mmi->i;
    typedef itk::InvertIntensityImageFilter <Byte3DImage> InvertIntensityImageFilterType;
    InvertIntensityImageFilterType::Pointer invertIntensityFilter = InvertIntensityImageFilterType::New();
    invertIntensityFilter->SetInput(mi);
    invertIntensityFilter->SetMaximum(255);
    invertIntensityFilter->Update();
    Byte3DImage::Pointer output = invertIntensityFilter->GetOutput();
    ImageWithInfo oui;
    oui.i = output;
    oui.t = name;
    appendImage(oui, name);
    return true;
}

CREATE_VKTOOL(ColorMultiplication)
void ColorMultiplication::initialize(void)
{
    createMethod("Multiplies RGB channel images");
    createParameter("GammaRed", "f;1.0");
    createParameter("GammaGreen", "f;1.0");
    createParameter("GammaBlue", "f;1.0");
    createParameter("Multiplier", "f;1.0");
}
bool ColorMultiplication::runMethod(void)
{
    unsigned int d;
    float weightRed = getFloatParameter("GammaRed");
    float weightGreen = getFloatParameter("GammaGreen");
    float weightBlue = getFloatParameter("GammaBlue");
    float bias = getFloatParameter("Multiplier");

    std::string redName, blueName, greenName;
    ImageWithInfo* inputRedI = getRedImage(&redName);
    ImageWithInfo* inputGreenI = getGreenImage(&greenName);
    ImageWithInfo* inputBlueI = getBlueImage(&blueName);

    if(inputRedI == NULL || inputBlueI == NULL || inputGreenI == NULL) return false;
    Byte3DImage::Pointer inputRed = inputRedI->i;
    Byte3DImage::Pointer inputGreen = inputGreenI->i;
    Byte3DImage::Pointer inputBlue = inputBlueI->i;

    typedef itk::ImageDuplicator< Byte3DImage > DuplicatorType;
    DuplicatorType::Pointer duplicator = DuplicatorType::New();
    duplicator->SetInputImage(inputRed);
    duplicator->Update();
    Byte3DImage::Pointer output = duplicator->GetOutput();

    size_t max = 1;
    for(d = 0; d < 3; d++) max *= output->GetLargestPossibleRegion().GetSize()[d];

    unsigned char* ptrr = inputRed->GetPixelContainer()->GetImportPointer();
    unsigned char* ptrg = inputGreen->GetPixelContainer()->GetImportPointer();
    unsigned char* ptrb = inputBlue->GetPixelContainer()->GetImportPointer();
    unsigned char* ptro = output->GetPixelContainer()->GetImportPointer();
    for(d = 0; d < max; d++)
    {
        double v =  pow((double)*ptrr/255.0, weightRed) * pow((double)*ptrg/255.0, weightGreen) * pow((double)*ptrb/255.0, weightBlue) * bias * 255.0;
        if(v > 255) v = 255;
        if(v < 0) v = 0;
        *ptro = v;
        ptro++; ptrr++; ptrg++; ptrb++;
    }
    ImageWithInfo oui;
    std::stringstream ss;
    ss << name << " "<< bias << "*" << redName << "^" << weightRed << "*" << greenName << "^" << weightGreen << "+" << blueName << "^" <<  weightBlue;
    oui.t = ss.str();
    oui.i = output;
    appendImage(oui, name);
    return true;
}


CREATE_VKTOOL(ColorAddition)
void ColorAddition::initialize(void)
{
    createMethod("Combines RGB channel images");
    createParameter("WeightRed", "f;1.0");
    createParameter("WeightGreen", "f;1.0");
    createParameter("WeightBlue", "f;1.0");
    createParameter("Bias", "f;0.0");
}
bool ColorAddition::runMethod(void)
{
    unsigned int d;
    float weightRed = getFloatParameter("WeightRed");
    float weightGreen = getFloatParameter("WeightGreen");
    float weightBlue = getFloatParameter("WeightBlue");
    float bias = getFloatParameter("Bias");

    std::string redName, blueName, greenName;
    ImageWithInfo* inputRedI = getRedImage(&redName);
    ImageWithInfo* inputGreenI = getGreenImage(&greenName);
    ImageWithInfo* inputBlueI = getBlueImage(&blueName);

    if(inputRedI == NULL || inputBlueI == NULL || inputGreenI == NULL) return false;
    Byte3DImage::Pointer inputRed = inputRedI->i;
    Byte3DImage::Pointer inputGreen = inputGreenI->i;
    Byte3DImage::Pointer inputBlue = inputBlueI->i;

    typedef itk::ImageDuplicator< Byte3DImage > DuplicatorType;
    DuplicatorType::Pointer duplicator = DuplicatorType::New();
    duplicator->SetInputImage(inputRed);
    duplicator->Update();
    Byte3DImage::Pointer output = duplicator->GetOutput();

    size_t max = 1;
    for(d = 0; d < 3; d++) max *= output->GetLargestPossibleRegion().GetSize()[d];

    unsigned char* ptrr = inputRed->GetPixelContainer()->GetImportPointer();
    unsigned char* ptrg = inputGreen->GetPixelContainer()->GetImportPointer();
    unsigned char* ptrb = inputBlue->GetPixelContainer()->GetImportPointer();
    unsigned char* ptro = output->GetPixelContainer()->GetImportPointer();
    for(d = 0; d < max; d++)
    {
        float v = weightRed**ptrr + weightGreen**ptrg + weightBlue**ptrb + bias;
        if(v > 255) v = 255;
        if(v < 0) v = 0;
        *ptro = v;
        ptro++; ptrr++; ptrg++; ptrb++;
    }
    ImageWithInfo oui;
    std::stringstream ss;
    ss << name << " " << weightRed << "*" << redName << "+" << weightGreen << "*" << greenName << "+" << weightBlue << "*" << blueName << "+" << bias;
    oui.t = ss.str();
    oui.i = output;
    appendImage(oui, name);
    return true;
}

#include "../shared/mz2itk.h"
CREATE_VKTOOL(GaussianLowPass)
void GaussianLowPass::initialize(void)
{
    createMethod("Image convolution with Gaussian kernel");
    createParameter("Sigma", "f;1.0;0.0");
}
bool GaussianLowPass::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL) return false;
    Byte3DImage::Pointer mi = mmi->i;
    float sigma = getFloatParameter("Sigma");
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    Mz2Itk<float>::Image3DType::Pointer oi = ImageFilters<float>::gaussianFilter(ii, sigma);
    Byte3DImage::Pointer ni = Mz2Itk<float>::getMZNormalized(oi);
    ImageWithInfo oui;
    oui.i = ni;
    std::stringstream ss;
    ss << name << ", Sigma = " << sigma;
    oui.t = ss.str();
    appendImage(oui, name);
    return true;
}

CREATE_VKTOOL(Gradient)
void Gradient::initialize(void)
{
    createMethod("Computes magnitude of image gradient");
}
bool Gradient::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL) return false;
    Byte3DImage::Pointer mi = mmi->i;
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    Mz2Itk<float>::Image3DType::Pointer oi = ImageFilters<float>::gradientFilter(ii);
    Byte3DImage::Pointer ni = Mz2Itk<float>::getMZNormalized(oi);
    ImageWithInfo oui;
    oui.i = ni;
    oui.t = name;
    appendImage(oui, name);
    return true;
}

CREATE_VKTOOL(Laplacian)
void Laplacian::initialize(void)
{
    createMethod("Computes image Laplacian");
}
bool Laplacian::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL) return false;
    Byte3DImage::Pointer mi = mmi->i;
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    Mz2Itk<float>::Image3DType::Pointer oi = ImageFilters<float>::laplacianFilter(ii);
    Byte3DImage::Pointer ni = Mz2Itk<float>::getMZNormalized(oi);
    ImageWithInfo oui;
    oui.i = ni;
    oui.t = name;
    appendImage(oui, name);
    return true;
}

CREATE_VKTOOL(CannyEdges)
void CannyEdges::initialize(void)
{
    createMethod("Canny edge detection");
    createParameter("Sigma", "f;1.0;0.0");
    createParameter("LowerThreshold", "i;1;0;255");
    createParameter("UpperThreshold", "i;254;0;255");
}
bool CannyEdges::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL) return false;

    float sigma = getFloatParameter("Sigma");
    int min = getIntParameter("LowerThreshold");
    int max = getIntParameter("UpperThreshold");
    Byte3DImage::Pointer mi = mmi->i;
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    Mz2Itk<float>::Image3DType::Pointer oi = ImageFilters<float>::cannyEdgeFilter(ii, sigma, min, max);
    Byte3DImage::Pointer ni = Mz2Itk<float>::getMZNormalized(oi);
    ImageWithInfo oui;
    oui.i = ni;
    std::stringstream ss;
    ss << name << ", Sigma = " << sigma << ", Lower = " << min << ", Upper = " << max;
    oui.t = ss.str();
    appendImage(oui, name);
    return true;
}

/*
    static typename ImageType::Pointer cannyEdgeFilter(typename ImageType::Pointer input_image, float sigma, float upperThreshold, float lowerThreshold)
*/

CREATE_VKTOOL(Thresholding)
void Thresholding::initialize(void)
{
    createMethod("Image binarization by gray-scale thresholding");
    createParameter("Min", "i;128;0;255");
    createParameter("Max", "i;255;0;255");
}
bool Thresholding::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage(); if(mmi == NULL) return false; Byte3DImage::Pointer mi = mmi->i;
    int min = getIntParameter("Min");
    int max = getIntParameter("Max");

    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    Mz2Itk<unsigned char>::Image3DType::Pointer oi = ImageFilters<unsigned char>::thresholdFilter(ii, min, max);
    Byte3DImage::Pointer ni = Mz2Itk<unsigned char>::getMZNormalized(oi);

    ImageWithInfo oui;
    oui.i = ni;
    std::stringstream ss;
    ss << name << ", Min = " << min << ", Max = " << max;
    oui.t = ss.str();
    appendImage(oui, name);
    return true;
}

CREATE_VKTOOL(FloodFilling)
void FloodFilling::initialize(void)
{
    createMethod("Region growing - bucket fill tool");
    createParameter("Fiducials", "t;current;current;all");
    createParameter("Min", "i;128;0;255");
    createParameter("Max", "i;255;0;255");
}
bool FloodFilling::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL) return false;
    Byte3DImage::Pointer mi = mmi->i;
    int min = getIntParameter("Min");
    int max = getIntParameter("Max");
    std::string mode = getStringParameter("Fiducials");


    Byte3DImage::IndexType fiducial;


   // if(mode == "all")


    if(!getCurrentFiducial(&fiducial)) return false;
    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    Mz2Itk<unsigned char>::Image3DType::Pointer oi = ImageFilters<unsigned char>::regionGrowing(ii, min, max, fiducial);
    Byte3DImage::Pointer ni = Mz2Itk<unsigned char>::getMZNormalized(oi);

    ImageWithInfo oui;
    oui.i = ni;
    std::stringstream ss;
    ss << name << ", Min = " << min << ", Max = " << max;
    oui.t = ss.str();
    appendImage(oui, name);
    return true;
}


CREATE_VKTOOL(Median)
void Median::initialize(void)
{
    createMethod("Median filtration");
    createParameter("Radius", "f;1.0;0.0");
}
bool Median::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage(); if(mmi == NULL) return false; Byte3DImage::Pointer mi = mmi->i;
    float radius = getFloatParameter("Radius");
    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    ImageFilters<unsigned char>::ImageType::Pointer oi = ImageFilters<unsigned char>::median(ii, radius);
    Byte3DImage::Pointer ni = Mz2Itk<unsigned char>::getMZNormalized(oi);
    ImageWithInfo oui;
    oui.i = ni;
    std::stringstream ss;
    ss << name << ", Radius = " << radius;
    oui.t = ss.str();
    appendImage(oui, name);
    return true;
}


CREATE_VKTOOL(Dilation)
void Dilation::initialize(void)
{
    createMethod("Morphological dilation");
    createParameter("Radius", "f;1.0;0.0");
}
bool Dilation::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage(); if(mmi == NULL) return false; Byte3DImage::Pointer mi = mmi->i;
    float radius = getFloatParameter("Radius");
    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    ImageFilters<unsigned char>::ImageType::Pointer oi = ImageFilters<unsigned char>::dilate(ii, radius);
    Byte3DImage::Pointer ni = Mz2Itk<unsigned char>::getMZNormalized(oi);
    ImageWithInfo oui;
    oui.i = ni;
    std::stringstream ss;
    ss << name << ", Radius = " << radius;
    oui.t = ss.str();
    appendImage(oui, name);
    return true;
}


CREATE_VKTOOL(Erosion)
void Erosion::initialize(void)
{
    createMethod("Morphological erosion");
    createParameter("Radius", "f;1.0;0.0");
}
bool Erosion::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage(); if(mmi == NULL) return false; Byte3DImage::Pointer mi = mmi->i;
    float radius = getFloatParameter("Radius");
    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    ImageFilters<unsigned char>::ImageType::Pointer oi = ImageFilters<unsigned char>::erode(ii, radius);
    Byte3DImage::Pointer ni = Mz2Itk<unsigned char>::getMZNormalized(oi);
    ImageWithInfo oui;
    oui.i = ni;
    std::stringstream ss;
    ss << name << ", Radius = " << radius;
    oui.t = ss.str();
    appendImage(oui, name);
    return true;
}


#include <itkHessianRecursiveGaussianImageFilter.h>
//#include <itkHessian3DToVesselnessMeasureImageFilter.h>
#include "../shared/vkScaffoldHessianToVesselnessFilter.h"
CREATE_VKTOOL(Vesselness)
void Vesselness::initialize(void)
{
    createMethod("Hessian based vesselness function");
    createParameter("Method", "t;Sato;Sato;Frangi;Erdt;SatoXY;FrangiXY;ErdtXY");
    createParameter("NumberOfScales", "i;1;1");
    createParameter("StdevMin", "f;1.0;0.0", "Standard deviation for minimum scale");
    createParameter("StdevMax", "f;1.0;0.0", "Standard deviation for maximum scale");
    //createParameter("Enhancement", "f;1.0;0.0;10.0", "Enhancement of higher scales");
    createParameter("C", "f;1.0;0.0", "Frangi's algorithm C parameter");
    createParameter("Alpha", "f;0.5;0.0", "Frangi's algorithm Alpha parameter");
    createParameter("Beta", "f;0.5;0.0", "Frangi's algorithm Beta parameter");
    createParameter("Alpha1", "f;0.5;0.0", "Sato's algorithm Alpha1 parameter");
    createParameter("Alpha2", "f;2.0;0.0", "Sato's algorithm Alpha2 parameter");
}
bool Vesselness::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL) return false;
    Byte3DImage::Pointer mi = mmi->i;
    std::string method = getStringParameter("Method");
    int scales = getIntParameter("NumberOfScales");
    float stdmin = getFloatParameter("StdevMin");
    float stdmax = getFloatParameter("StdevMax");
    float enhanc = 0; //getFloatParameter("Enhancement");
    float c = getFloatParameter("C");
    float alpha = getFloatParameter("Alpha");
    float beta = getFloatParameter("Beta");
    float alpha1 = getFloatParameter("Alpha1");
    float alpha2 = getFloatParameter("Alpha2");

    typedef itk::HessianRecursiveGaussianImageFilter< Mz2Itk<float>::Image3DType > F1;
    typedef F1::Pointer P1;
    typedef ScaffoldHessianToVesselnessFilter< float > F2;
    typedef F2::Pointer P2;
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    typedef itk::ImageRegionConstIterator< Mz2Itk<float>::Image3DType > ConstIteratorType;
    typedef itk::ImageRegionIterator< Mz2Itk<float>::Image3DType > IteratorType;


    P1 filterH = F1::New();
    P1 filterHi = F1::New();
    P2 filterV = F2::New();
    P2 filterVi = F2::New();
    filterH->SetInput(ii);
    filterHi->SetInput(ii);

    if(method == "Sato")
    {
        filterV->SetMethodSato(alpha1, alpha2);
        filterVi->SetMethodSato(alpha1, alpha2);
        enhanc = 2;
    }
    else if(method == "Erdt")
    {
        filterV->SetMethodErdt();
        filterVi->SetMethodErdt();
        enhanc = 2;
    }
    else if(method == "FrangiXY")
    {
        filterV->SetMethodFrangiXY(c, alpha, beta);
        filterVi->SetMethodFrangiXY(c, alpha, beta);
        enhanc = 0;
    }
    else if(method == "SatoXY")
    {
        filterV->SetMethodSatoXY(alpha1, alpha2);
        filterVi->SetMethodSatoXY(alpha1, alpha2);
        enhanc = 2;
    }
    else if(method == "ErdtXY")
    {
        filterV->SetMethodErdtXY();
        filterVi->SetMethodErdtXY();
        enhanc = 2;
    }
    else
    {
        filterV->SetMethodFrangi(c, alpha, beta);
        filterVi->SetMethodFrangi(c, alpha, beta);
        enhanc = 0;
    }

    filterH->SetSigma(stdmin);
    filterH->Update();
    filterV->SetInput(filterH->GetOutput());
    filterV->Update();
    Mz2Itk<float>::Image3DType::Pointer result = filterV->GetOutput();

    double q = pow((double)stdmax/stdmin,(1.0/double(scales-1)));
    for(int s = 1; s < scales; s++)
    {
        float stdev = stdmin*pow(q, (double)s);
        filterHi->SetSigma(stdev);
        filterHi->Update();
        filterVi->SetInput(filterHi->GetOutput());
        filterVi->Update();
        Mz2Itk<float>::Image3DType::Pointer res = filterVi->GetOutput();
        double multip = stdev/stdmin;
        multip = pow(multip, enhanc);

        IteratorType firstIterator(result, result->GetRequestedRegion());
        ConstIteratorType nextIterator(res, res->GetRequestedRegion());
        firstIterator.GoToBegin();
        nextIterator.GoToBegin();
        while (!firstIterator.IsAtEnd())
        {
            double np = multip*nextIterator.Get();
            double sp = firstIterator.Get();
            if(np > sp)
                firstIterator.Set(np);
            ++firstIterator;
            ++nextIterator;
        }
    }
    Byte3DImage::Pointer ni = Mz2Itk<float>::getMZNormalized(result);
    ImageWithInfo oui;
    oui.i = ni;
    std::stringstream ss;
    ss << name << ", Method: "<< method <<", Scales = " << scales << ": <" << stdmin << ", " << stdmax;
    oui.t = ss.str();
    appendImage(oui, name);
    return true;
}


CREATE_VKTOOL(HoleFilling)
void HoleFilling::initialize(void)
{
    createMethod("Fills spaces in binary objects");
    createParameter("Object", "t;White;White;Black");

}
bool HoleFilling::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL) return false;
    Byte3DImage::Pointer mi = mmi->i;
    std::string object = getStringParameter("Object");

    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    ImageFilters< unsigned char >::ImageType::Pointer oi;
    unsigned char foreground;
    if(object == "Black")
    {
        foreground = ImageFilters< unsigned char >::minIntensity(ii);
    }
    else
    {
        foreground = ImageFilters< unsigned char >::maxIntensity(ii);
    }
    oi = ImageFilters<unsigned char>::holeFill(ii, foreground);

    Byte3DImage::Pointer ni = Mz2Itk<unsigned char>::getMZ(oi);
    ImageWithInfo oui;
    oui.i = ni;
    std::stringstream ss;
    ss << name;
    oui.t = ss.str();
    appendImage(oui, name);
    return true;
}


CREATE_VKTOOL(TopHat)
void TopHat::initialize(void)
{
    createMethod("Top hat transform");
    createParameter("Object", "t;White;White;Black");
    createParameter("Radius", "f;1.0;0.0");
}
bool TopHat::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL) return false;
    Byte3DImage::Pointer mi = mmi->i;
    std::string object = getStringParameter("Object");
    float radius = getFloatParameter("Radius");
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    ImageFilters<float>::ImageType::Pointer oi;
    if(object == "Black") oi = ImageFilters<float>::blackTopHat(ii, radius);
    else oi = ImageFilters<float>::whiteTopHat(ii, radius);
    Byte3DImage::Pointer ni = Mz2Itk<float>::getMZNormalized(oi);
    ImageWithInfo oui;
    oui.i = ni;
    std::stringstream ss;
    ss << name << ", Radius = " << radius;
    oui.t = ss.str();
    appendImage(oui, name);
    return true;
}

CREATE_VKTOOL(SkeletonFromBinary)
void SkeletonFromBinary::initialize(void)
{
    createMethod("Fills spaces in binary images");
}
bool SkeletonFromBinary::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage(); if(mmi == NULL) return false; Byte3DImage::Pointer mi = mmi->i;
    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    Mz2Itk<unsigned char>::Image3DType::Pointer oi = ImageFilters<unsigned char>::skeletonFromBinary(ii);
    Byte3DImage::Pointer ni = Mz2Itk<unsigned char>::getMZNormalized(oi);
    ImageWithInfo oui;
    oui.i = ni;
    std::stringstream ss;
    ss << name;
    oui.t = ss.str();
    appendImage(oui, name);
    return true;
}

CREATE_VKTOOL(TreeFromSkeleton)
void TreeFromSkeleton::initialize(void)
{
    createMethod("Center lines vectorization");
}
bool TreeFromSkeleton::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL) return false;
    Byte3DImage::Pointer mi = mmi->i;
    Mz2Itk<unsigned char>::Image3DType::Pointer ii = Mz2Itk<unsigned char>::getItk(mi);
    Tree tree;
    Byte3DImage::IndexType fiducial;
    if(getCurrentFiducial(&fiducial)) tree = BuildTree<unsigned char>::skeletonToTree(ii, &fiducial);
    else tree = BuildTree<unsigned char>::skeletonToTree(ii, NULL);
    appendTree(tree, name);
    return true;
}


CREATE_VKTOOL(TreeFromUpsizedBinary)
void TreeFromUpsizedBinary::initialize(void)
{
    createMethod("First it scales image up");
    createParameter("UpsizeMethod", "t;NeighborCheck;NeighborCheck;ItkResampleImageFilter");
}
bool TreeFromUpsizedBinary::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage(); if(mmi == NULL) return false; Byte3DImage::Pointer mi = mmi->i;
    std::string mode = getStringParameter("UpsizeMethod");
    Mz2Itk<unsigned char>::Image3DType::Pointer ii;
    if(mode == "ItkResampleImageFilter")  ii = Mz2Itk<unsigned char>::getUpsizedBinaryI(mi);
    else ii = Mz2Itk<unsigned char>::getUpsizedBinaryM(mi);
    Mz2Itk<unsigned char>::Image3DType::Pointer oi = ImageFilters<unsigned char>::skeletonFromBinary(ii);
    Tree tree;
    Byte3DImage::IndexType fiducial;
    if(getCurrentFiducial(&fiducial)) tree = BuildTree<unsigned char>::skeletonToTree(oi, &fiducial);
    else tree = BuildTree<unsigned char>::skeletonToTree(oi, NULL);
    appendTree(tree, name);
    return true;
}

/*
#include "../shared/treediameters.h"
CREATE_VKTOOL(RadiusFromBinary)
void RadiusFromBinary::initialize(void)
{
    createMethod("RadiusFromBinary", "Estimates radius at every point of existing tree centerline from binary image");
    createParameter("Density", "i;2;1", "Divider for icosahedron based mesh");
    createParameter("Gamma", "f;4.0;0.0", "Weights impact of smallest to second smallest eigenvalue");
    createParameter("Rho", "f;1.4142136;0.0", "Multiplier, should be equal to square root of two");
}
bool RadiusFromBinary::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    ImageWithInfo* mmi = getCurrentImage(); if(mmi == NULL) return false; Byte3DImage::Pointer mi = mmi->i;

    int divider = getIntParameter("Density");
    float gamma = getFloatParameter("Gamma");
    float rho = getFloatParameter("Rho");

    TreeDiameters diams(mt, mi, gamma, divider, rho);
    Tree tree = diams.getResult();

    appendTree(tree, name);
    return true;
}
*/

#include "../shared/radiiestimation.h"

CREATE_VKTOOL(RadiiFromBinaryCrossSection)
void RadiiFromBinaryCrossSection::initialize(void)
{
    createMethod("Estimates radii at every point of existing tree by matching erfc function");
    createParameter("DirectionSigma", "f;1.0;0.3;10.0", "Sigma of a filter to estimate direction of the centerline");
    createParameter("Directions", "i;16;4;360", "Number of directions around the centerline for radius sampling");
    createParameter("EigenWeight", "f;0.5;-0.5;1.0", "Zero to use first eigenvalue and 0.5 to use two");
}
bool RadiiFromBinaryCrossSection::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL)
        return false;
    Byte3DImage::Pointer mi = mmi->i;
    double direction_sigma = getFloatParameter("DirectionSigma");
    unsigned int num_of_dirs = getIntParameter("Directions");
    double eigenvalue_weight = getFloatParameter("EigenWeight");
    Tree tree = RadiiEstimation::RadiiFromBinaryCrossSection(mt, mi,
                                                             direction_sigma, num_of_dirs,
                                                             eigenvalue_weight);
    appendTree(tree, name);
    return true;
}

CREATE_VKTOOL(RadiiFromBinaryOmnidirectional)
void RadiiFromBinaryOmnidirectional::initialize(void)
{
    createMethod("Estimates radii at every point of existing tree by matching erfc function");
    createParameter("IcosahedronDivider", "i;2;0;10", "Icosahedron triangle divider to find omnidirectional vectors");
    createParameter("EigenWeight", "f;0.5;-0.5;1.0", "Zero to use first eigenvalue and 0.5 to use two");
}
bool RadiiFromBinaryOmnidirectional::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL)
        return false;
    Byte3DImage::Pointer mi = mmi->i;
    unsigned int divisions = getIntParameter("IcosahedronDivider");
    double eigenvalue_weight= getFloatParameter("EigenWeight");
    Tree tree = RadiiEstimation::RadiiFromBinaryOmnidirectional(mt, mi,
                                                              divisions,
                                                              eigenvalue_weight);
    appendTree(tree, name);
    return true;
}

CREATE_VKTOOL(RadiiFromCrossSectionErfcMatching)
void RadiiFromCrossSectionErfcMatching::initialize(void)
{
    createMethod("Estimates radii at every point of existing tree by matching erfc function");
    createParameter("DirectionSigma", "f;1.0;0.3;10.0", "Sigma of a filter to estimate direction of the centerline");
    createParameter("Directions", "i;16;4;360", "Number of directions around the centerline for radius sampling");
    createParameter("ProfileSamples", "i;20;5;1000", "Number of samples to be matched by Erfc function");
    createParameter("ProfileDistance", "f;10.0;1.0;100.0", "Profile distance should be higher then maximum expected radius");
    createParameter("EigenWeight", "f;0.5;-0.5;1.0", "Zero to use first eigenvalue and 0.5 to use two");
}
bool RadiiFromCrossSectionErfcMatching::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL)
        return false;
    Byte3DImage::Pointer mi = mmi->i;
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    double direction_sigma = getFloatParameter("DirectionSigma");
    unsigned int num_of_dirs = getIntParameter("Directions");
    unsigned int profile_samples = getIntParameter("ProfileSamples");
    double profile_range = getFloatParameter("ProfileDistance");
    double eigenvalue_weight = getFloatParameter("EigenWeight");
    Tree tree = RadiiEstimation::RadiiFromCrossSectionErfcMatching(mt, ii,
                                                                      direction_sigma, num_of_dirs,
                                                                      profile_samples, profile_range,
                                                                      eigenvalue_weight);
    appendTree(tree, name);
    return true;
}

CREATE_VKTOOL(RadiiFromOmnidirectionalErfcMatching)
void RadiiFromOmnidirectionalErfcMatching::initialize(void)
{
    createMethod("Estimates radii at every point of existing tree by matching erfc function");
    createParameter("IcosahedronDivider", "i;2;0;10", "Icosahedron triangle divider to find omnidirectional vectors");
    createParameter("ProfileSamples", "i;20;5;1000", "Number of samples to be matched by Erfc function");
    createParameter("ProfileDistance", "f;10.0;1.0;100.0", "Profile distance should be higher then maximum expected radius");
    createParameter("EigenWeight", "f;0.5;-0.5;1.0", "Zero to use first eigenvalue and 0.5 to use two");
}
bool RadiiFromOmnidirectionalErfcMatching::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL)
        return false;
    Byte3DImage::Pointer mi = mmi->i;
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    unsigned int divisions = getIntParameter("IcosahedronDivider");
    unsigned int profile_samples = getIntParameter("ProfileSamples");
    double profile_range= getFloatParameter("ProfileDistance");
    double eigenvalue_weight= getFloatParameter("EigenWeight");
    Tree tree = RadiiEstimation::RadiiFromOmnidirectionalErfcMatching(mt, ii,
                                                                      divisions,
                                                                      profile_samples, profile_range,
                                                                      eigenvalue_weight);
    appendTree(tree, name);
    return true;
}

CREATE_VKTOOL(RadiiFromMultiscaleVesselness)
void RadiiFromMultiscaleVesselness::initialize(void)
{
    createMethod("Estimates radii at every point of existing tree by matching vesselness profile");
    createParameter("StdevMin", "f;0.5;0.1;100.0", "Minimum standard deviation for Gaussian window");
    createParameter("StdevMax", "f;2.5;0.1;100.0", "Maximum standard deviation for Gaussian window");
    createParameter("Scales", "i;5;2;100", "Number of scales and points for matching");
    createParameter("StdevPower", "f;2.0;0.0;3.0", "Matches vesselness multiplied by the standard deviation to this power");
}
bool RadiiFromMultiscaleVesselness::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    ImageWithInfo* mmi = getCurrentImage();
    if(mmi == NULL)
        return false;
    Byte3DImage::Pointer mi = mmi->i;
    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    int scales = getIntParameter("Scales");
    float minstd = getFloatParameter("StdevMin");
    float maxstd = getFloatParameter("StdevMax");
    float power = getFloatParameter("StdevPower");
    Tree tree = RadiiEstimation::RadiiFromVesselnessProfile(mt, ii, minstd, maxstd, scales, power);
    appendTree(tree, name);
    return true;
}

#include "../shared/smoothing.h"
CREATE_VKTOOL(SmoothCenterlines)
void SmoothCenterlines::initialize(void)
{
    createMethod("Smoothes centerlines by active contour energy minimization");
    createParameter("Tau1", "f;2.0;0.0;10.0", "Determines minimization of the first derivative");
    createParameter("Tau2", "f;1.0;0.0;10.0", "Determines minimization of the second derivative");
    createParameter("Iterations", "i;1;1;20", "Number of iterations");
}
bool SmoothCenterlines::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    double tau1 = getFloatParameter("Tau1");;
    double tau2 = getFloatParameter("Tau2");;
    unsigned int iterations = getIntParameter("Iterations");;
    Tree tree = Smoothing::SmoothCenterline(mt, tau1, tau2, iterations);
    appendTree(tree, name);
    return true;
}

CREATE_VKTOOL(SmoothRadii)
void SmoothRadii::initialize(void)
{
    createMethod("Smoothes radii (surface) by active contour energy minimization");
    createParameter("Tau1", "f;2.0;0.0;10.0", "Determines minimization of the first derivative");
    createParameter("Tau2", "f;1.0;0.0;10.0", "Determines minimization of the second derivative");
    createParameter("Iterations", "i;1;1;20", "Number of iterations");
}
bool SmoothRadii::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    double tau1 = getFloatParameter("Tau1");;
    double tau2 = getFloatParameter("Tau2");;
    unsigned int iterations = getIntParameter("Iterations");;
    Tree tree = Smoothing::SmoothRadii(mt, tau1, tau2, iterations);
    appendTree(tree, name);
    return true;
}

/*
#include "../shared/treesmooth.h"
CREATE_VKTOOL(SmoothTreeBranches)
void SmoothTreeBranches::initialize(void)
{
    createMethod("SmoothTreeBranches", "Smoothes centerlines");
    createParameter("Alpha", "f;0.5;0.0", "Controls minimization of the first derivative");
    createParameter("Beta", "f;0.2;0.0", "Controls minimization of the second derivative");
    createParameter("Spring", "f;0.1;0.0", "Minimizes distance from original location");
    createParameter("Anchor", "f;0.1;0.0", "Minimizes third power of distance from original location");
    createParameter("Iterations", "i;5;1", "Number of iterations");
    createParameter("Chillout", "i;5;0", "Number of finishing iterations");
}
bool SmoothTreeBranches::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    float d1 = getFloatParameter("Alpha");;
    float d2 = getFloatParameter("Beta");;
    float fa = getFloatParameter("Spring");;
    float faaa = getFloatParameter("Anchor");;
    int iterations = getIntParameter("Iterations");;
    int chillout = getIntParameter("Chillout");;

    TreeSmooth smoothing(mt, d1, d2, fa, faaa);
    Tree tree = smoothing.getResult(iterations, chillout);

    appendTree(tree, name);
    return true;
}

#include "../shared/smoothdiameter.h"
CREATE_VKTOOL(SmoothDiameters)
void SmoothDiameters::initialize(void)
{
    createMethod("SmoothDiameters", "Smoothes diameters");
    createParameter("Alpha", "f;0.5;0.0", "Controls minimization of the first derivative");
    createParameter("Spring", "f;0.1;0.0", "Minimizes distance from original location");
    createParameter("Anchor", "f;0.1;0.0", "Minimizes third power of distance from original location");
    createParameter("Iterations", "i;5;1", "Number of iterations");
    createParameter("Chillout", "i;5;0", "Number of finishing iterations");
}
bool SmoothDiameters::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    float d1 = getFloatParameter("Alpha");
    float fa = getFloatParameter("Spring");
    float faaa = getFloatParameter("Anchor");
    int iterations = getIntParameter("Iterations");
    int chillout = getIntParameter("Chillout");
    SmoothDiameter smoothing(mt, d1, fa, faaa);
    Tree tree = smoothing.getResult(iterations, chillout);
    appendTree(tree, name);
    return true;
}
*/

#include "../shared/treeextend.h"
CREATE_VKTOOL(ExtendBranches)
void ExtendBranches::initialize(void)
{
    createMethod("Extends existing branches of selected tree");
    createParameter("MinCriterion", "f;0.5;0.0", "Minimum value to extend branch");
    createParameter("Sigma", "f;0.5;0.0", "Vesselness scale");
    createParameter("Normalize", "t;Yes;Yes;No");
    createParameter("Alpha1", "f;0.5;0.0", "Sato's vesselness computation parameter");
    createParameter("Alpha2", "f;2.0;0.0", "Sato's vesselness computation parameter");
    createParameter("MaxNodes", "i;100;1", "Maximum number of nodes added to a branch");
}
bool ExtendBranches::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    ImageWithInfo* mmi = getCurrentImage(); if(mmi == NULL) return false; Byte3DImage::Pointer mi = mmi->i;

    float mingoal = getFloatParameter("MinCriterion");
    float sigma = getFloatParameter("Sigma");
    std::string normalizes = getStringParameter("Normalize");
    bool normalize = (normalizes == "Yes");
    float alfa1 = getFloatParameter("Alpha1");
    float alfa2 = getFloatParameter("Alpha2");
    int maxnodes = getFloatParameter("MaxNodes");
    TreeExtend tool(mt, Mz2Itk<double>::getItk(mi), mingoal, maxnodes, sigma, normalize, alfa1, alfa2);
    Tree tree = tool.getResult();
    appendTree(tree, "extendedTree");
    return true;
}


CREATE_VKTOOL(DisjoinBranches)
void DisjoinBranches::initialize(void)
{
    createMethod("Disjoin branches of selected tree");
}
bool DisjoinBranches::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    std::vector<Tree> ntrees;
    std::vector< std::string > nnames;
    std::vector< unsigned int > ncolors;

    unsigned int nb = mt->count();
    for(unsigned int n = 0; n < nb; n++)
    {
        Tree tree;
        std::vector<NodeIn3D> branch = mt->branch(n);
        if(branch.size() > 0)
        {
            tree.addBranch(branch);
            ntrees.push_back(tree);
            std::stringstream ss;
            ss << "branch" << n;
            nnames.push_back(ss.str());
            ncolors.push_back(0x00ff88);
        }
    }
    appendTrees(ntrees, nnames, ncolors);
    return true;
}


#include "../shared/tree.h"
CREATE_VKTOOL(JoinChecked)
void JoinChecked::initialize(void)
{
    createMethod("Connects checked trees and branches");
}
bool JoinChecked::runMethod()
{
    Tree* tree;
    Tree newtree;
    unsigned int i = 0;
    bool checked;
    bool add = false;
    while((tree = getTree(i, NULL, &checked)) != NULL)
    {
        if(checked)
        {
            unsigned int nb = tree->count();
            for(unsigned int b = 0; b < nb; b++)
            {
                std::vector<NodeIn3D> branch = tree->branch(b);
                newtree.addBranch(branch, false);
                add = true;
            }
        }
        i++;
    }
    if(add)
    {
        newtree.correctConnectivity();
        appendTree(newtree, name);
        return true;
    }
    return false;
}

#include "../shared/tree.h"
CREATE_VKTOOL(ConvertBranches)
void ConvertBranches::initialize(void)
{
    createMethod("Changes storage format of branches between short and long");
    createParameter("Format", "t;Short;Short;Long");
}
bool ConvertBranches::runMethod()
{
    Tree* mt = getCurrentTree();
    if(mt == 0) return false;
    Tree tree = *(mt);
    std::string format = getStringParameter("Format");

    if(format == "Long")
    {
        if(tree.rebuildTreeLongBranches())
        {
            appendTree(tree, ("longBranchesFormat"), 0x77ff00);
        }
    }
    else
    {
        if(tree.rebuildTreeShortBranches())
        {
            appendTree(tree, ("shortBranchesFormat"), 0x77ff00);
        }
    }
    return true;
}

/*
 *
 *
 *
#include "../shared/imagefilters2.h"

CREATE_VKTOOL(LevelSetChanVeseDense)
void LevelSetChanVeseDense::initialize(void)
{
    createMethod("Level-set Chan and Vese dense model");
    createParameter("Iterations", "i;50;1");
    createParameter("InitialDistance", "f;1.0;0.0");
    createParameter("Rms", "f;0.0;0.0");
    createParameter("Epsilon", "f;1.0;0.0");
    createParameter("CurvatureWeight", "f;0.0;0.0");
    createParameter("AreaWeight", "f;0.0;0.0");
    createParameter("ReinitializationWeight", "f;0.0;0.0");
    createParameter("VolumeWeight", "f;0.0;0.0");
    createParameter("Volume", "f;0.0;0.0");
    createParameter("Lambda1", "f;1.0;0.0");
    createParameter("Lambda2", "f;1.0;0.0");

}
bool LevelSetChanVeseDense::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage(); if(mmi == NULL) return false; Byte3DImage::Pointer mi = mmi->i;

    unsigned int nb_iteration = getIntParameter("Iterations");
    double initialDistance = getFloatParameter("InitialDistance");
    double rms = getFloatParameter("Rms");
    double epsilon = getFloatParameter("Epsilon");
    double curvature_weight = getFloatParameter("CurvatureWeight");
    double area_weight = getFloatParameter("AreaWeight");
    double reinitialization_weight = getFloatParameter("ReinitializationWeight");
    double volume_weight = getFloatParameter("VolumeWeight");
    double volume = getFloatParameter("Volume");
    double l1 = getFloatParameter("Lambda1");
    double l2 = getFloatParameter("Lambda2");

    Byte3DImage::IndexType fiducial;
    if(!getCurrentFiducial(&fiducial)) return false;

    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    Mz2Itk<float>::Image3DType::Pointer oi = ImageFilters2<float>::levelSetChanVeseDense(ii, fiducial,
                                                                                    nb_iteration,
                                                                                    initialDistance,
                                                                                    rms,
                                                                                    epsilon,
                                                                                    curvature_weight,
                                                                                    area_weight,
                                                                                    reinitialization_weight,
                                                                                    volume_weight,
                                                                                    volume,
                                                                                    l1,
                                                                                    l2);
    Byte3DImage::Pointer ni = Mz2Itk<float>::getMZNormalized(oi);

    ImageWithInfo oui;
    oui.i = ni;
    oui.t = name;
    appendImage(oui, name);
    return true;
}



CREATE_VKTOOL(LevelSetChanVeseSparse)
void LevelSetChanVeseSparse::initialize(void)
{
    createMethod("Level-set Chan and Vese sparse model");
    createParameter("Iterations", "i;50;1");
    createParameter("InitialDistance", "f;1.0;0.0");
    createParameter("Rms", "f;0.0;0.0");
    createParameter("Epsilon", "f;1.0;0.0");
    createParameter("CurvatureWeight", "f;0.0;0.0");
    createParameter("AreaWeight", "f;0.0;0.0");
    createParameter("ReinitializationWeight", "f;0.0;0.0");
    createParameter("VolumeWeight", "f;0.0;0.0");
    createParameter("Volume", "f;0.0;0.0");
    createParameter("Lambda1", "f;1.0;0.0");
    createParameter("Lambda2", "f;1.0;0.0");

}
bool LevelSetChanVeseSparse::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage(); if(mmi == NULL) return false; Byte3DImage::Pointer mi = mmi->i;

    unsigned int nb_iteration = getIntParameter("Iterations");
    double initialDistance = getFloatParameter("InitialDistance");
    double rms = getFloatParameter("Rms");
    double epsilon = getFloatParameter("Epsilon");
    double curvature_weight = getFloatParameter("CurvatureWeight");
    double area_weight = getFloatParameter("AreaWeight");
    double reinitialization_weight = getFloatParameter("ReinitializationWeight");
    double volume_weight = getFloatParameter("VolumeWeight");
    double volume = getFloatParameter("Volume");
    double l1 = getFloatParameter("Lambda1");
    double l2 = getFloatParameter("Lambda2");

    Byte3DImage::IndexType fiducial;
    if(!getCurrentFiducial(&fiducial)) return false;

    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    Mz2Itk<float>::Image3DType::Pointer oi = ImageFilters2<float>::levelSetChanVeseSparse(ii, fiducial,
                                                                                    nb_iteration,
                                                                                    initialDistance,
                                                                                    rms,
                                                                                    epsilon,
                                                                                    curvature_weight,
                                                                                    area_weight,
                                                                                    reinitialization_weight,
                                                                                    volume_weight,
                                                                                    volume,
                                                                                    l1,
                                                                                    l2);
    Byte3DImage::Pointer ni = Mz2Itk<float>::getMZNormalized(oi);

    ImageWithInfo oui;
    oui.i = ni;
    oui.t = name;
    appendImage(oui, name);
    return true;
}


CREATE_VKTOOL(LevelSetCurves)
void LevelSetCurves::initialize(void)
{
    createMethod("Level-set curves");
    createParameter("Iterations", "i;800;1");
    createParameter("SmoothingIterations", "i;5;0");
    createParameter("InitialDistance", "f;1.0;0.0");
    createParameter("PropagationScaling", "f;1.0;0.0");
    createParameter("Sigma", "f;2.0;0.0");
    createParameter("Alpha", "f;1.0;0.0");
    createParameter("Beta", "f;1.0;0.0");
    createParameter("ThresholdLower", "f;-1000.0");
    createParameter("ThresholdUpper", "f;0.0;0.0");
    createParameter("ThresholdOutside", "f;0.0;0.0");
    createParameter("ThresholdInside", "f;255.0;0.0");
    createParameter("SigmoidMin", "f;0.0;0.0");
    createParameter("SigmoidMax", "f;1.0;0.0");
    createParameter("CurvatureScaling", "f;1.0;0.0");
    createParameter("AdvectionScaling", "f;1.0;0.0");
    createParameter("Rms", "f;0.02;0.0");
    createParameter("SmoothingTimeStep", "f;0.125;0.0");
    createParameter("SmoothingConductance", "f;9.0;0.0");
}
bool LevelSetCurves::runMethod()
{
    ImageWithInfo* mmi = getCurrentImage(); if(mmi == NULL) return false; Byte3DImage::Pointer mi = mmi->i;

    double initialDistance = getFloatParameter("InitialDistance");
    double propagationScaling = getFloatParameter("PropagationScaling");
    double sigma = getFloatParameter("Sigma");
    double alpha = getFloatParameter("Alpha");
    double beta = getFloatParameter("Beta");
    double thrLower = getFloatParameter("ThresholdLower");
    double thrUpper = getFloatParameter("ThresholdUpper");
    double thrOutside = getFloatParameter("ThresholdOutside");
    double thrInside = getFloatParameter("ThresholdInside");
    double sigMin = getFloatParameter("SigmoidMin");
    double sigMax = getFloatParameter("SigmoidMax");
    double curvSc = getFloatParameter("CurvatureScaling");
    double advSc = getFloatParameter("AdvectionScaling");
    double rms = getFloatParameter("Rms");
    unsigned int iterations = getIntParameter("Iterations");
    double smoTimeStep = getFloatParameter("SmoothingTimeStep");
    unsigned int smoIterations = getIntParameter("SmoothingIterations");
    double smoConductance = getFloatParameter("SmoothingConductance");

    Byte3DImage::IndexType fiducial;
    if(!getCurrentFiducial(&fiducial)) return false;

    Mz2Itk<float>::Image3DType::Pointer ii = Mz2Itk<float>::getItk(mi);
    Mz2Itk<float>::Image3DType::Pointer oi = ImageFilters2<float>::levelSetCurves(ii, fiducial,
                                                                                  initialDistance,
                                                                                  propagationScaling,
                                                                                  sigma, alpha, beta,
                                                                                  thrLower, thrUpper, thrOutside, thrInside,
                                                                                  sigMin, sigMax, curvSc, advSc, rms,
                                                                                  iterations, smoTimeStep, smoIterations, smoConductance
                                                                                  );
    Byte3DImage::Pointer ni = Mz2Itk<float>::getMZNormalized(oi);

    ImageWithInfo oui;
    oui.i = ni;
    oui.t = name;
    appendImage(oui, name);
    return true;
}
*/
#endif // VKTOOLS_H
