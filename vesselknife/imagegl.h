#ifndef IMAGEGL_H
#define IMAGEGL_H

#include <QWidget>
//#include <QtOpenGL/QGLWidget>
//#include <QtOpenGL/QGLFunctions>
#include <QVector3D>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QApplication>

//#include <QPainter>
//#include <QWindow>
//#include <QOpenGLFunctions>
//#include <QOpenGLPaintDevice>
//#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>

//#ifdef _WIN32
//#include <QtOpenGL/QGLFunctions>
//#else
#include <GL/gl.h>		   // Open Graphics Library (OpenGL) header
//#endif

#include "../shared/sphear.h"
//#include "isetview.h"
#include "../shared/tree.h"
//#include "../shared/byteimage.h"

const static double sina = 3.48994967025009716459951816253e-2;
const static double cosa = 0.99939082701909573000624344004393;

class ImageGl : public QOpenGLWidget//QGLWidget
//#ifdef _WIN32
    , protected QOpenGLFunctions
//#endif

{
    Q_OBJECT
public:
    explicit ImageGl(QWidget *parent = 0);
    ~ImageGl();

    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void wheelEvent(QWheelEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void mouseDoubleClickEvent(QMouseEvent *ev);

    void resizeEvent(QResizeEvent *ev);


    //void setView(unsigned int view);
    bool isAlreadySetImage(unsigned int channel, unsigned char* rimage);

    void setImage(unsigned int channel, unsigned char* rimage);
    unsigned char* getImage(unsigned int channel);
    void setImageSizes(unsigned int size[3], double spacing[3]);

    void rotateLeft(double degrees);

    void setVolumeView(int view);
    void setCrossView(int direction, bool view);
    void setTreesView(std::vector<Tree *> &trees, std::vector<unsigned int> &colors, int viewballs);

    void setBackgroundColor(unsigned int background);
    unsigned int backgroundColor(void) {return backgroundcolor;}
    void setZoom(int z);
    void setStereo(int s, int d);
    void setCrossSection(int direction, unsigned int index);
    void setCrossSection(int direction, bool show);
    //void setViewBalls(bool view);
    //void setImageChannel(char ichannel);
//    void setImageBrightnessLow(double iminbrightness);
//    void setImageBrightnessHigh(double imaxbrightness);
    void setImageGrayWindowLow(double iminwindow);
    void setImageGrayWindowHigh(double imaxwindow);
    //void setImageVisibility(ImageVisibilityMode ivisibility);
    //void setImageVisibilityAll(char ichannel, double iminwindow, double imaxwindow, ImageVisibilityMode ivisibility);
    //void getCrossSection(int direction, unsigned int* index, bool* show);
    //void getBackgroundColor(unsigned int* background);
    //void getZoom(int* z);
    //void getImageVisibilityAll(char* ochannel, double* ominwindow, double* omaxwindow, ImageVisibilityMode* ovisibility);
    void getMatrix(double m[]);
    void setMatrix(double m[]);

    bool Coordinates3Dto2D(double input[3], double output[3], bool update_matrices);
    bool Coordinates2Dto3D(int input[2], int output[3]);

    void setFreeze(bool f) {freeze = f;}

signals:
    void zoomChanged(int);
    void pointedAt(int*);
    void rendererResizeEvent(QSize);

private:
    unsigned int image_size[3];
    double voxel_spacing[3];
   // bool viewballs;
    //char channel;
//    double minbrightness;
//    double maxbrightness;
    double minwindow;
    double maxwindow;
    unsigned int backgroundcolor;
    //ImageVisibilityMode visibility;
    int zoomlevel;
    unsigned int sliceindex[3];
    bool renderslice[3];
    int rendervolume;
    bool rendertrees;
    //bool renderslices;
#ifdef VESSELKNIFE_FAST_MIP
    bool repaintvolume;
    int volumelist[6];
#else
    int volumelist;
    int lastdirectionofvolume;
#endif
    int treeslist;
    int stereo;
    int stereodepth;
    bool freeze;

    //bool slicevisible[3];

    int mousex, mousey;
    void initializeGL();
    void paintGL();
    void paintGLSetMatrices(GLdouble modelview[16] = NULL, GLdouble projection[16] = NULL, GLint viewport[4] = NULL);

    //unsigned int currentview;


    unsigned char* image[4];
    GLdouble modelViewMat[16];
    //GLdouble mato[16];
    //GLdouble mat_1[16];
    //bool uninverted;

    void setIdentityMatrix(GLdouble W[16]);
    void copy4to4(GLdouble W[14], GLdouble W1[14]);
    void shift(int qx, int qy);
    void rotate(bool d, int l1, int l2);
    void rotatem(double rot, int l1, int l2, GLdouble W[14]);
//    void rotatemo(int rot, int l1, int l2);
//    void sizemo(GLdouble prop);
    GLdouble determinant(GLdouble W[14]);
    GLdouble subdet3of4(GLdouble W[16], int w, int k);
    GLdouble invert3(GLdouble W[16], GLdouble W1[16]);
    int invert4(GLdouble W[16], GLdouble W1[16]);
    void multiply3x3(GLdouble W[16], GLdouble W1[16], GLdouble W2[16]);
    void multiply4x4(GLdouble W[16], GLdouble W1[16], GLdouble W2[16]);

    //void initView(void);
    int getFacingDirection(void);
    bool createVolumetricView(int direction);
    void renderCrossSection(unsigned int direction, unsigned int sliceindex, GLubyte* bits, unsigned int sizeimg[2], unsigned int sizetex[2]);
    //bool fillCrossSectionSingle(GLubyte *bits, unsigned int stride, unsigned int direction, unsigned int sliceindex);
    bool fillCrossSection(GLubyte *bits, unsigned int stride, unsigned int direction, unsigned int sliceindex);
    bool createCrossSection(unsigned int direction, unsigned int sliceindex);
    void createCube(float bottomRadius, float topRadius, float halfHeight);
    void createTube(float bottomRadius, float topRadius, float halfHeight);
    void createSphear(float radius, int divisions = 4);
    void createSphearMesh(float radius, int divisions = 4);
    void createDirections(float radius, int divisions = 4);

    void createTree(Tree* tree, unsigned int color, int viewballs);

    void paintSingleGL(void);
    void setFalseColor(float level);
};

#endif // IMAGEGL_H
