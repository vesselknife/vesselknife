#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "nodeproperties.h"
#include "dialogvoxelsize.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <QFileDialog>
#include <QMessageBox>
#include <QColorDialog>
#include <QDropEvent>
#include <QMimeData>
#include <QDesktopServices>
#include <QUrl>
#include <QPushButton>
#include <QLinearGradient>
#include <QStringList>
#include <QWindow>
#include <QScreen>
#include <QGuiApplication>

#include <string>
#include <sstream>

//#include "../ITKIOFactoryRegistration/itkImageIOFactoryRegisterManager.h"
//#include "../ITKIOFactoryRegistration/itkTransformIOFactoryRegisterManager.h"
#include <itkImageIOFactory.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkCropImageFilter.h>

#include "../shared/mz2itk.h"
#include "../shared/filenameremap.h"

#include <itkVersion.h>


void MainWindow::on_actionLoad_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Load image"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Images (*.nii *.nifti *.hdr *.nii.gz *.dicom *.dcm);;All files (*)"));
    if (!fileName.isEmpty())
    {
        QCursor oc = cursor();
        setCursor(Qt::WaitCursor);
        loadImage(fileName);
        setCursor(oc);
    }
}


void MainWindow::on_actionLoad_stack_triggered()
{
    QStringList fileNames = QFileDialog::getOpenFileNames(this,
                                                    tr("Load image stack"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Tiff (*.tif *.tiff);;All files (*)"));
    if (!(fileNames.count() > 0))
    {
//        loadTiffImages(fileNames);
    }
}

void MainWindow::on_actionSave_triggered()
{
    if(images.size() <= 0) return;
    QString itemName;
    ImageWithInfo* image = getCurrentImage(&itemName);
    if(image == NULL) return;
//    unsigned int row = ui->listWidgetBuffers->currentRow();
//    if(row >= images.size()) return;
//    QListWidgetItem* item  = ui->listWidgetBuffers->currentItem();
//    if(item == NULL) return;

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save image"),
                                                    itemName,
                                                    tr("Images (*.nii *.nifti *.hdr *.nii.gz *.dicom *.dcm);;All files (*)"));
    if (!fileName.isEmpty())
    {
        saveImageItkFactory(fileName.toStdString().c_str(), image);
    }
}


void MainWindow::on_actionTo_STL_triggered()
{
    if(trees.size() <= 0) return;

    QString itemName;
    bool checked;
    Tree* tree = getCurrentTree(&itemName, &checked);
    if(tree == NULL) return;

//    unsigned int row = ui->listWidgetTrees->currentRow();
//    if(row >= trees.size()) return;
//    QListWidgetItem* item  = ui->listWidgetTrees->currentItem();
//    if(item == NULL) return;

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Export tree"),
                                                    itemName,
                                                    tr("Stereolithography (*.stl);;All files (*)"));
    if (!fileName.isEmpty())
    {
        //QString fnw = QString::fromStdString(fileName);
        tree->save(tempTreeFile.toStdString().c_str(), 0);
        exportImportConversion("tree2stl", &tempTreeFile, &fileName);
    }
}

void MainWindow::on_actionTo_VTK_triggered()
{
    if(trees.size() <= 0) return;


    QString itemName;
    bool checked;
    Tree* tree = getCurrentTree(&itemName, &checked);
    if(tree == NULL) return;


//    unsigned int row = ui->listWidgetTrees->currentRow();
//    if(row >= trees.size()) return;
//    QListWidgetItem* item  = ui->listWidgetTrees->currentItem();
//    if(item == NULL) return;

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Export tree"),
                                                    itemName,
                                                    tr("Visualization toolkit file format (*.vtk);;All files (*)"));
    if (!fileName.isEmpty())
    {
        //QString fnw = QString::fromStdString(fileNameRemapWrite(&fileName));
        tree->save(tempTreeFile.toStdString().c_str(), 0);
        exportImportConversion("tree2vtk", &tempTreeFile, &fileName);
    }
}



//void MainWindow::on_actionColor_triggered()
//{
//    on_imageLevelsButton_clicked();
//}

void MainWindow::on_spinBoxMin_valueChanged(int val)
{
//    QCursor oc = cursor();
//    setCursor(Qt::WaitCursor);
    renderer->setImageGrayWindowLow((double)val);
//    setCursor(oc);
}

void MainWindow::on_spinBoxMax_valueChanged(int val)
{
//    QCursor oc = cursor();
//    setCursor(Qt::WaitCursor);
    renderer->setImageGrayWindowHigh((double)val);
//    setCursor(oc);
}

void MainWindow::on_spinBox_X_valueChanged(int arg1)
{
    renderer->setCrossSection(1, (unsigned int)arg1);
}

void MainWindow::on_spinBox_Y_valueChanged(int arg1)
{
    renderer->setCrossSection(2, (unsigned int)arg1);
}

void MainWindow::on_spinBox_Z_valueChanged(int arg1)
{
    renderer->setCrossSection(3, (unsigned int)arg1);
}

void MainWindow::on_checkBox_X_clicked(bool checked)
{
    renderer->setCrossSection(1, checked);
}

void MainWindow::on_checkBox_Y_clicked(bool checked)
{
    renderer->setCrossSection(2, checked);
}

void MainWindow::on_checkBox_Z_clicked(bool checked)
{
    renderer->setCrossSection(3, checked);
}


void MainWindow::on_setAsRChannel_clicked()
{
    setCurrentBufferAsNthChannel(0);
}

void MainWindow::on_setAsGChannel_clicked()
{
    setCurrentBufferAsNthChannel(1);
}

void MainWindow::on_setAsBChannel_clicked()
{
    setCurrentBufferAsNthChannel(2);
}

//void MainWindow::on_setAsAChannel_clicked()
//{
//    setCurrentBufferAsNthChannel(3);
//}

void MainWindow::on_buttonColorPicker_clicked()
{
    ui->buttonAddSeed->setChecked(false);
    ui->buttonIndicateNode->setChecked(false);
    ui->buttonSetVoxel->setChecked(false);
    renderer->setFreeze(false);
}

void MainWindow::on_buttonSetVoxel_clicked(bool checked)
{
    ui->buttonColorPicker->setChecked(false);
    ui->buttonAddSeed->setChecked(false);
    ui->buttonIndicateNode->setChecked(false);
    renderer->setFreeze(checked);
}


void MainWindow::on_buttonIndicateNode_clicked()
{
    ui->buttonColorPicker->setChecked(false);
    ui->buttonAddSeed->setChecked(false);
    ui->buttonSetVoxel->setChecked(false);
    renderer->setFreeze(false);
}

void MainWindow::on_buttonAddSeed_clicked()
{
    ui->buttonColorPicker->setChecked(false);
    ui->buttonIndicateNode->setChecked(false);
    ui->buttonSetVoxel->setChecked(false);
    renderer->setFreeze(false);
}


void MainWindow::on_buttonRemoveSeed_clicked()
{
    QListWidgetItem* item  = ui->listWidgetSeed->currentItem();
    if(item != NULL) delete item;
}

void MainWindow::on_removeBuffer_clicked()
{
    QListWidgetItem* item  = ui->listWidgetBuffers->currentItem();
    int row  = ui->listWidgetBuffers->currentRow();
    if(item == NULL || row < 0) return;
    images[row].i = NULL;
    images.erase(images.begin()+row);
    delete item;

    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);
    row  = ui->listWidgetBuffers->currentRow();
    if(row < 0)
    {
        renderer->setImage(0, NULL);
        renderer->setImage(1, NULL);
        renderer->setImage(2, NULL);
        //renderer->setImage(3, NULL);
    }
    else
    {
        unsigned char* imgdata = images[row].i->GetPixelContainer()->GetImportPointer();
        renderer->setImage(0, imgdata);
        renderer->setImage(1, imgdata);
        renderer->setImage(2, imgdata);
        //renderer->setImage(3, imgdata);
    }
    renderer->repaint();
    setCursor(oc);
    updateImageIcons();
}

void MainWindow::on_imageOffButton_clicked(bool checked)
{
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

    if(checked) renderer->setVolumeView(1);
    else renderer->setVolumeView(0);
    renderer->repaint();

    setCursor(oc);
    ui->imageFooButton->setChecked(false);
}

void MainWindow::on_imageFooButton_clicked(bool checked)
{
    QCursor oc = cursor();
    setCursor(Qt::WaitCursor);

    if(checked) renderer->setVolumeView(-1);
    else renderer->setVolumeView(0);

    renderer->repaint();

    setCursor(oc);
    ui->imageOffButton->setChecked(false);
}

void MainWindow::on_actionLoadTree_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Load tree"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Tree (*.txt);;All files (*)"));
    if (!fileName.isEmpty())
    {
        Tree tree;
        if(tree.load(fileName.toStdString().c_str()))
        {
            appendTree(tree, QFileInfo(fileName).fileName().toStdString());
        }
    }
}



//void MainWindow::on_actionImport_tree_triggered()
//{
//    QString fileName = QFileDialog::getOpenFileName(this,
//                                                    tr("Import tree"),
//                                                    NULL,//domyslna nazwa pliku
//                                                    tr("Vtk polydata (*.vtk);;All files (*)"));
//    if (!fileName.isEmpty())
//    {
//        Tree tree = VtkImport::getTree(fileName.toStdString().c_str());
//        if(tree.count() > 0)
//        {
//            appendToTrees(tree, QFileInfo(fileName).fileName());
//        }
//    }
//}

void MainWindow::on_actionSaveTree_triggered()
{
    if(trees.size() <= 0) return;

    QString itemName;
    bool checked;
    Tree* tree = getCurrentTree(&itemName, &checked);
    if(tree == NULL) return;

//    unsigned int row = ui->listWidgetTrees->currentRow();
//    if(row >= trees.size()) return;
//    QListWidgetItem* item  = ui->listWidgetTrees->currentItem();
//    if(item == NULL) return;

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save tree"),
                                                    itemName,
                                                    tr("Tree (*.txt);;All files (*)"));
    if (!fileName.isEmpty())
    {
        if(QApplication::keyboardModifiers() & Qt::ShiftModifier)
            tree->save(fileName.toStdString().c_str(), 1);
        else
            tree->save(fileName.toStdString().c_str(), 0);
    }
}


void MainWindow::on_getMatrixButton_clicked()
{
    QString m;
    double mat[16];
    renderer->getMatrix(mat);
    m = QString("%1 %2 %3 %4\n%5 %6 %7 %8\n%9 %10 %11 %12\n%13 %14 %15 %16").arg(mat[0]).arg(mat[1]).arg(mat[2]).arg(mat[3]).arg(mat[4]).arg(mat[5]).arg(mat[6]).arg(mat[7]).arg(mat[8]).arg(mat[9]).arg(mat[10]).arg(mat[11]).arg(mat[12]).arg(mat[13]).arg(mat[14]).arg(mat[15]);
    ui->matrixTextEdit->setPlainText(m);
}

void MainWindow::on_setMatrixButton_clicked()
{
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    //QRegularExpression rx = QRegularExpression::fromWildcard(txt);
    QStringList list = ui->matrixTextEdit->toPlainText().split(QRegularExpression("\\s+"), Qt::SkipEmptyParts);
#else
    QStringList list = ui->matrixTextEdit->toPlainText().split(QRegExp("\\s+"), Qt::SkipEmptyParts);
#endif

    if(list.count() != 16) return;
    double mat[16];
    for(int i = 0; i < 16; i++)
    {
        mat[i] = list[i].toDouble();
    }
    renderer->setMatrix(mat);
}

void MainWindow::on_defaultMatrixButton_clicked()
{
    QString m = QString("1 0 0 0\n0 1 0 0\n0 0 1 0\n0 0 -1000 1");
    ui->matrixTextEdit->setPlainText(m);
}

void MainWindow::on_removeTree_clicked()
{
    QListWidgetItem* item  = ui->listWidgetTrees->currentItem();
    unsigned int row  = ui->listWidgetTrees->currentRow();
    if(item == NULL || row < 0) return;
    trees.erase(trees.begin()+row);
    if(row < treecolors.size()) treecolors.erase(treecolors.begin()+row);
    delete item;
    on_listWidgetTrees_itemChanged(NULL);
}

void MainWindow::on_toolButton_clicked()
{
    Tree* tree = getCurrentTree();
    if(tree == NULL) return;
    unsigned int nnodes = tree->nodeCount();
    unsigned int njoints = 0;
    unsigned int ntips = 0;
    unsigned int nnjoints = 0;

    double minr = 0;
    double maxr = 0;

    for(unsigned int n = 0; n < nnodes; n++)
    {
        if(n == 0)
        {
            minr = maxr = tree->node(n).radius;
        }
        else
        {
            if(minr > tree->node(n).radius) minr = tree->node(n).radius;
            if(maxr < tree->node(n).radius) maxr = tree->node(n).radius;
        }
        unsigned int cc = tree->node(n).connections;
        if(cc > 2)
        {
            njoints++;
            nnjoints += cc;
        }
        if(cc < 2) ntips++;
    }

    unsigned int nbranches = tree->count();

    double length = 0.0;
    for(unsigned int b = 0; b < nbranches; b++)
    {
        std::vector<NodeIn3D> branch = tree->branch(b);
        unsigned int nmax = branch.size();
        for(unsigned int n = 1; n < nmax; n++)
        {
            double xx = branch[n-1].x - branch[n].x;
            double yy = branch[n-1].y - branch[n].y;
            double zz = branch[n-1].z - branch[n].z;
            length += sqrt(xx*xx+yy*yy+zz*zz);
        }
    }

    Tree mt = *tree;
    mt.rebuildTreeShortBranches();
    unsigned int nnets = mt.disconnectedNumber();

    QString info = QString("Trees: %1\nNodes: %2\nBranches: %3\nLength: %4\nJoints: %5\nTips: %6\nRadii: %7-%8").arg(nnets).arg(nnodes).arg(nbranches).arg(length).arg(njoints).arg(ntips).arg(minr).arg(maxr);

    QMessageBox msgBox;
    msgBox.setText(info);
    msgBox.setWindowTitle("Tree info");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.exec();
}



void MainWindow::on_toolButtonBalls_clicked()
{
    on_listWidgetTrees_itemChanged(NULL);
    //renderer->setViewBalls(ui->toolButtonBalls->isChecked());
}
void MainWindow::on_toolButtonWires_clicked()
{
    on_listWidgetTrees_itemChanged(NULL);
}

void MainWindow::on_toolButtonStream_clicked()
{
    on_listWidgetTrees_itemChanged(NULL);
}


void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_stereoCombo_currentIndexChanged(int index)
{
    renderer->setStereo(index, ui->stereoSlider->value());
}

void MainWindow::on_stereoSlider_valueChanged(int value)
{
    on_stereoCombo_currentIndexChanged(ui->stereoCombo->currentIndex());
}


void MainWindow::on_buttonImageInfo_clicked()
{
    ImageWithInfo* mi = getCurrentImage();
    if(mi == NULL) return;

    QMessageBox msgBox;
    msgBox.setText(mi->t.c_str());
    msgBox.setWindowTitle("Image info");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.exec();
}

void MainWindow::on_actionAbout_triggered()
{
    std::stringstream ss;
    ss << "<h2>VesselKnife</h2> " << std::endl;
    ss << "Program for the analysis of 3D angiography images and for the vascular system vectorization and modeling.<br><br>" << std::endl;
    ss << "Copyright 2015-2022 by Piotr M. Szczypinski" << "<br>" << std::endl;
    ss << RELEASE_VERSION << "<br>" << std::endl;
    ss << "Built on " << __DATE__ << " at " << __TIME__ << "<br> <br>" << std::endl;
    ss << "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE." << "<br> <br>" << std::endl;
    ss << "Visit: <a href=\"http://www.eletel.p.lodz.pl/pms/SoftwareVesselKnife.html\"> http://www.eletel.p.lodz.pl/pms/SoftwareVesselKnife.html </a> <br>" << std::endl;
    ss << "<br>Built with:<br>" << std::endl;
    ss << "- Qt " << QT_VERSION_STR << " <a href=\"https://www.qt.io/developers/\">https://www.qt.io/developers</a> <br>" << std::endl;
    ss << "- Insight Toolkit " <<  ITK_VERSION << " <a href=\"https://itk.org/\">https://itk.org</a> <br>" << std::endl;
    // ss << "- Visualization Toolkit " <<  VTK_VERSION << " <a href=\"https://vtk.org/\">https://vtk.org</a> <br>" << std::endl;
    char* glversion = (char*) glGetString(GL_VERSION);
    if(glversion != NULL)
        ss << "- OpenGL " << glversion <<std::endl;
    else
        ss << "- OpenGL" << std::endl;
    QMessageBox::about(this, "About VesselKnife", ss.str().c_str());
}

void MainWindow::on_actionContents_triggered()
{
    QDir execDir(qApp->applicationDirPath());
    QString pathq = execDir.absolutePath();
    QString path = pathq + QString("/vesselknife.pdf");
    QDesktopServices::openUrl(QUrl(path, QUrl::TolerantMode));
}

//void MainWindow::frameChanged(int frame)
//{
//    renderer->setCrossSection(3, (unsigned int)frame);

//}


//void MainWindow::on_actionReload_raster_triggered()
//{
//    Byte3DImage * im = getCurrentImage();
//    if(im)
//    {
//        if(im->size[2] > 1)
//        {
//            unsigned int selected;
//            unsigned int frames = im->size[2];
//            ImageSelect dialog(frames, &selected, this);
//            connect(&dialog, SIGNAL(frameChanged(int)), this, SLOT(frameChanged(int)));
//            dialog.exec();
//            disconnect(&dialog, SIGNAL(frameChanged(int)), 0, 0);
//        }
//    }
//}

void MainWindow::on_imageLevelsButton_clicked()
{
    on_actionBackground_color_triggered();
}

void MainWindow::on_actionBackground_color_triggered()
{
    QColor color(renderer->backgroundColor());
    color = QColorDialog::getColor(color);
    if(color.isValid())
        renderer->setBackgroundColor(color.rgb());
}

void MainWindow::on_listWidgetBuffers_currentRowChanged(int currentRow)
{
    if(timer.isActive())
    {
        timer.stop();
        ui->playButton->setIcon(QIcon(":/icons/icons/media-playback-start.png"));
    }

    if(images.size() > currentRow)
    {
//        ImageWithInfo * im = images[currentRow];
//        if(im)
//        {
//            unsigned int frames = im->i->framesInFile;
//            unsigned int frame = im->i->currentFrame;
//            if(frames > 1)
//            {
//                ui->frameSlider->setMaximum(frames-1);
//                ui->frameSpinBox->setMaximum(frames-1);
//                ui->frameSlider->setValue(frame);
//                ui->frameSpinBox->setValue(frame);
//                ui->frameSlider->setEnabled(true);
//                ui->frameSpinBox->setEnabled(true);
//                ui->playButton->setEnabled(true);
//                return;
//            }
//        }
    }

    ui->frameSlider->setEnabled(false);
    ui->frameSpinBox->setEnabled(false);
    ui->playButton->setEnabled(false);
}

void MainWindow::on_frameSpinBox_valueChanged(int arg1)
{

//    int ile = images.size();
//    int row = ui->listWidgetBuffers->currentRow();
//    if(row >= ile || ile <= 0 || row < 0) return;
//    Byte3DImage* imgc = images[row];
//    if(imgc->framesInFile <= (unsigned int)arg1) return;
//    Byte3DImage* imgn = new Byte3DImage();

//    if(loadNifti(imgc->filePathAndName, imgn, arg1))
//    {
//        images[row] = imgn;
//        bool upd = false;
//        for(int i = 0; i < 4; i++)
//        {
//            if(renderer->getImage(i) == imgc)
//            {
//                renderer->setImage(i, imgn, false);
//                upd = true;
//            }
//        }
//        if(upd) renderer->update();
//        delete imgc;
//    }
//    else
//    {
//        delete imgn;
//    }
}

void MainWindow::on_playButton_clicked()
{
    if(timer.isActive())
    {
        timer.stop();
        ui->playButton->setIcon(QIcon(":/icons/icons/media-playback-start.png"));
    }
    else
    {
        timer.start(100);
        ui->playButton->setIcon(QIcon(":/icons/icons/media-playback-stop.png"));
    }
}

void MainWindow::processTimer(void)
{
    if(ui->frameSpinBox->value() < ui->frameSpinBox->maximum())
    {
        ui->frameSpinBox->setValue(ui->frameSpinBox->value()+1);
    }
    else
    {
        ui->frameSpinBox->setValue(0);
        timer.stop();
        ui->playButton->setIcon(QIcon(":/icons/icons/media-playback-start.png"));
    }
}

void MainWindow::on_actionVoxel_size_triggered()
{
    //double voxelsize[3];
    int ile = images.size();
    if(ile < 0) return;
    //Byte3DImage::Pointer img = images[0];
    //if(img == NULL) return;
//    voxelsize[0] = images[0]->GetSpacing()[0];
//    voxelsize[1] = images[0]->GetSpacing()[1];
//    voxelsize[2] = images[0]->GetSpacing()[2];
    DialogVoxelSize dialog(voxel_spacing, this);
    int r = dialog.exec();

    Byte3DImage::SpacingType spacing;
    spacing[0] = voxel_spacing[0];
    spacing[1] = voxel_spacing[1];
    spacing[2] = voxel_spacing[2];
    if(QDialog::Accepted == r)
    {
        for(int i = 0; i < ile; i++)
        {
            images[i].i->SetSpacing(spacing);
        }
    }
    renderer->setImageSizes(image_size, voxel_spacing);
    renderer->update();
}



void MainWindow::on_treeWidget_itemChanged(QTreeWidgetItem *item, int column)
{
    if(column == 1)
    {
//        printf("Item changed: %s %s\n", item->text(1).toStdString().c_str(), item->data(1, Qt::UserRole).toString().toStdString().c_str());
//        fflush(stdout);

        QStringList valist = item->data(1, Qt::UserRole).toString().split(";");
        if(valist.size() >= 2)
        {
            if(valist[0] == "f")
            {
                float v = item->text(1).toFloat();
                if(valist.size() >= 3)
                {
                    float m = valist[2].toFloat();
                    if(v < m) v = m;
                }
                if(valist.size() >= 4)
                {
                    float m = valist[3].toFloat();
                    if(v > m) v = m;
                }
                item->setText(1, QString::number(v));
            }
            else if(valist[0] == "i")
            {
                int v = item->text(1).toFloat();
                if(valist.size() >= 3)
                {
                    int m = valist[2].toFloat();
                    if(v < m) v = m;
                }
                if(valist.size() >= 4)
                {
                    int m = valist[3].toFloat();
                    if(v > m) v = m;
                }
                item->setText(1, QString::number(v));
            }
        }
    }
}

void MainWindow::on_actionTree_to_Comsol_triggered()
{
    if(trees.size() <= 0) return;

    QString itemName;
    bool checked;
    Tree* tree = getCurrentTree(&itemName, &checked);
    if(tree == NULL) return;

//    unsigned int row = ui->listWidgetTrees->currentRow();
//    if(row >= trees.size()) return;
//    QListWidgetItem* item  = ui->listWidgetTrees->currentItem();
//    if(item == NULL) return;

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Export tree to Comsol"),
                                                    itemName,
                                                    tr("Text files (*.txt);;All files (*)"),
                                                    0,
                                                    QFileDialog::DontConfirmOverwrite);
    if (!fileName.isEmpty())
    {
        unsigned int brn = tree->count();

        char* fileNameC;
        fileNameC = new char[fileName.length()+32];
        char* fileNameR;
        fileNameR = new char[fileName.length()+32];
        char* fileNameV;
        fileNameV = new char[fileName.length()+32];

        for(unsigned int b = 0; b < brn; b++)
        {
            unsigned int nn = tree->count(b);
            if(nn < 2) continue;
            sprintf(fileNameC, "%s%04i%s", fileName.toStdString().c_str(), b+1, "C.txt");
            sprintf(fileNameR, "%s%04i%s", fileName.toStdString().c_str(), b+1, "R.txt");
            sprintf(fileNameV, "%s%04i%s", fileName.toStdString().c_str(), b+1, "V.txt");
            std::ofstream file;

            file.open(fileNameV);
            if (!file.is_open()) continue;
            if (!file.good()) continue;
            double di, dii;
            NodeIn3D noprev = tree->node(b, 0);
            NodeIn3D no = tree->node(b, 1);
            double xx, yy, zz;
            xx = no.x-noprev.x;
            yy = no.y-noprev.y;
            zz = no.z-noprev.z;
            di = sqrt(xx*xx+yy*yy+zz*zz);
            file << xx/di << " " << yy/di << " " << zz/di << std::endl;
            file.close();

            file.open(fileNameC);
            if (!file.is_open()) continue;
            if (!file.good()) continue;
            di = 0.0;
            noprev = tree->node(b, 0);
            for(unsigned int n = 0; n < nn; n++)
            {
                no = tree->node(b, n);
                xx = no.x-noprev.x;
                yy = no.y-noprev.y;
                zz = no.z-noprev.z;
                di += sqrt(xx*xx+yy*yy+zz*zz);
                file << no.x << " " << no.y << " " << no.z << std::endl;
                noprev = no;
            }
            file.close();

            file.open(fileNameR);
            if (!file.is_open()) continue;
            if (!file.good()) continue;
            noprev = tree->node(b, 0);
            dii = 0.0;
            for(unsigned int n = 0; n < nn; n++)
            {
                no = tree->node(b, n);
                xx = no.x-noprev.x;
                yy = no.y-noprev.y;
                zz = no.z-noprev.z;
                dii += sqrt(xx*xx+yy*yy+zz*zz);
                //file << (float)n/(float)(nn-1) << " " << no.radius << std::endl;
                file << (float)(dii/di) << " " << no.radius << std::endl;
                noprev = no;
            }
            file.close();
        }

        delete[] fileNameV;
        delete[] fileNameR;
        delete[] fileNameC;
    }
}

void MainWindow::on_actionAnimation_triggered()
{
    char name[128];
    unsigned char* ii[4];
    ii[0] = renderer->getImage(0);
    ii[1] = renderer->getImage(1);
    ii[2] = renderer->getImage(2);
    for(int d = 0; d < images.size(); d++)
    {
        ImageWithInfo* mi = getCurrentImage();
        if(mi == NULL) return;
        renderer->setImage(0, images[d].i->GetPixelContainer()->GetImportPointer());
        renderer->setImage(1, images[d].i->GetPixelContainer()->GetImportPointer());
        renderer->setImage(2, images[d].i->GetPixelContainer()->GetImportPointer());
        renderer->repaint();
        sprintf(name, "animation%04d.png", d);
//        QPixmap pixmap = renderer->renderPixmap();
//        pixmap.save(name);
        QImage image = renderer->grabFramebuffer();
        image.save(name);
    }
    renderer->setImage(0, ii[0]);
    renderer->setImage(1, ii[1]);
    renderer->setImage(2, ii[2]);
    renderer->repaint();
}

void MainWindow::on_actionAnimate_rotation_triggered()
{
    char name[128];
    double W[16];
    unsigned int l1 = 0;
    unsigned int l2 = 2;
    double a, b;
    double sina = sin(4*M_PI/180);
    double cosa = cos(4*M_PI/180);

    for(int d = 0; d < 90; d++)
    {
        renderer->getMatrix(W);
        for(int k =0; k<12; k+=4)
        {
            a = cosa * W[k + l1] + sina * W[k + l2];
            b = -sina * W[k + l1] + cosa * W[k + l2];
            W[k + l1] = a;
            W[k + l2] = b;
        }
        renderer->setMatrix(W);
        renderer->repaint();
        //renderer->update();
        sprintf(name, "rotation%04d.png", d);
        renderer->setMatrix(W);
//        QPixmap pixmap = renderer->renderPixmap();
//        pixmap.save(name);
        QImage image = renderer->grabFramebuffer();
        image.save(name);
    }
}
