#include "vktool.h"

extern MainWindow* parentWindow;

//VkTool* VkTool::getObject(unsigned int i)
//{
//    if(objectPointers.size() <= i) return NULL;
//    return objectPointers[i];
//}
std::string VkTool::getObjectsName(void)
{
    return name;
}
//std::string VkTool::getObjectsName(unsigned int i)
//{
//    std::string e;
//    VkTool* p = getObject(i);
//    if(p != NULL) return p->name;
//    else return e;
//}

void VkTool::createMethod(std::string tooltip)
{
    parentWindow->createMethod(name, tooltip);
}
void VkTool::createParameter(std::string name, std::string value, std::string tooltip)
{
    parentWindow->createParameter(name, value, tooltip);
}

std::string VkTool::getStringParameter(std::string parameterName)
{
    return parentWindow->getStringParameter(parameterName);
}
int VkTool::getIntParameter(std::string parameterName)
{
    return parentWindow->getIntParameter(parameterName);
//    std::string str = getParameter(parameterName);
//    return atoi(str.c_str());
}
float VkTool::getFloatParameter(std::string parameterName)
{
    return parentWindow->getFloatParameter(parameterName);
//    std::string str = getParameter(parameterName);
//    return atof(str.c_str());
}

bool VkTool::crop(Byte3DImage::IndexType min, Byte3DImage::IndexType max)
{
    return parentWindow->crop(min, max);
}
bool VkTool::getCurrentFiducial(Byte3DImage::IndexType* fiducial)
{
    return parentWindow->getCurrentFiducial(fiducial);
}
bool VkTool::getFiducial(unsigned int index, Byte3DImage::IndexType* fiducial)
{
    return parentWindow->getFiducial(index, fiducial);
}

ImageWithInfo* VkTool::getImage(unsigned int index, std::string* name)
{
    return parentWindow->getImage(index, name);
}
ImageWithInfo* VkTool::getCurrentImage(std::string* name)
{
    return parentWindow->getCurrentImage(-1, name);
}
ImageWithInfo* VkTool::getRedImage(std::string* name)
{
    return parentWindow->getCurrentImage(0, name);
}
ImageWithInfo* VkTool::getGreenImage(std::string* name)
{
    return parentWindow->getCurrentImage(1, name);
}
ImageWithInfo* VkTool::getBlueImage(std::string* name)
{
    return parentWindow->getCurrentImage(2, name);
}
ImageWithInfo* VkTool::getAlphaImage(std::string* name)
{
    return parentWindow->getCurrentImage(3, name);
}
void VkTool::appendImage(ImageWithInfo img, std::string name)
{
    parentWindow->appendImage(img, name);
}

Tree* VkTool::getTree(unsigned int index, std::string* name, bool* checked)
{
    return parentWindow->getTree(index, name, checked);
}
Tree* VkTool::getCurrentTree(std::string *name, bool *checked)
{
    return parentWindow->getCurrentTree(name, checked);
}


void VkTool::appendTree(Tree tree, std::string name, unsigned int color)
{
    parentWindow->appendTree(tree, name, color);
}
void VkTool::appendTrees(std::vector<Tree> trees, std::vector<std::string> names, std::vector<unsigned int> colors)
{
    parentWindow->appendTrees(trees, names, colors);
}
