#ifndef DIALOGVOXELSIZE_H
#define DIALOGVOXELSIZE_H

#include <QDialog>

namespace Ui {
class DialogVoxelSize;
}

class DialogVoxelSize : public QDialog
{
    Q_OBJECT

public:
    explicit DialogVoxelSize(double* voxels, QWidget *parent = 0);
    ~DialogVoxelSize();

private slots:
    void on_voxelSizeX_editingFinished();

    void on_voxelSizeY_editingFinished();

    void on_voxelSizeZ_editingFinished();

private:
    Ui::DialogVoxelSize *ui;
    double* voxelsize;
};

#endif // DIALOGVOXELSIZE_H
