/*
 * nifti2tiff - conversion from nifti to 3D tiff format
 *
 * Copyright 2015 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <nifti/nifti1.h>
#include <nifti/nifti1_io.h>
#include <tiffio.h>

/**
 * @brief printniftiinfo prints info on nifti image
 * @param nim is a pointer to nifti data
 */
void printniftiinfo(nifti_image* nim)
{
    printf("Format: ");
    switch(nim->nifti_type)
    {
    case 0: printf("Analyze"); break;
    case 1: printf("Nifti-1 (single file)"); break;
    case 2: printf("Nifti-1 (two files)"); break;
    case 3: printf("Nifti-ASCII"); break;
    default: printf("?");
    }
    printf("\n");

    printf("Raster: ");
    for(int d = 1; d <= nim->dim[0]; d++)
    {
        printf("%i", (int)nim->dim[d]);
        if((int)d < nim->dim[0]) printf("x");
    }
    printf("\n");
    printf("Voxel type: ");

    switch(nim->datatype)
    {
    case DT_UINT8: printf("uint8"); break;
    case DT_INT16: printf("int16"); break;
    case DT_UINT16: printf("uint16"); break;
    case DT_FLOAT32: printf("float32"); break;
    case DT_FLOAT64: printf("float64"); break;
    case DT_COMPLEX64: printf("complex64"); break;
    case DT_RGB24:  printf("rgb24"); break;
    default: printf("?"); break;
    }
    printf("\n");

    printf("Voxel size: ");
    for(int d = 1; d <= nim->dim[0]; d++)
    {
        printf("%f", (float)(nim->pixdim[d]));
        if((int)d < nim->dim[0]) printf("x");
    }
    printf("\n");

    printf("Space units: ");
    switch(nim->xyz_units)
    {
    case NIFTI_UNITS_METER: printf("m"); break;
    case NIFTI_UNITS_MM: printf("mm"); break;
    case NIFTI_UNITS_MICRON: printf("um"); break;
    default: printf("defaults to cm");
    }
    printf("\n");
    printf("Time units: ");
    switch(nim->time_units)
    {
    case NIFTI_UNITS_SEC: printf("s"); break;
    case NIFTI_UNITS_MSEC: printf("ms"); break;
    case NIFTI_UNITS_USEC: printf("us"); break;
    default: printf("?");
    }
    printf("\n");
}

/**
 * @brief nifti2tiff
 * @param input name of nifti file
 * @param output name of tiff file
 * @return zero on success
 */
int nifti2tiff(const char* input, const char* output)
{
    nifti_image* nim = NULL;
    nim = nifti_image_read(input, 1);
    if(nim == NULL)
    {
        fprintf(stderr, "Failed to open %s\n", input);
        return 1;
    }

    printniftiinfo(nim);

    if(nim->dim[0] < 2)
    {
        fprintf(stderr, "Dimensionality error\n");
        nifti_image_free(nim);
        return 2;
    }

    if( nim->datatype != DT_UINT8 &&
        nim->datatype != DT_INT16 &&
        nim->datatype != DT_UINT16 &&
        nim->datatype != DT_FLOAT32 &&
        nim->datatype != DT_COMPLEX64 &&
        nim->datatype != DT_FLOAT64 &&
        nim->datatype != DT_RGB24)
    {
        fprintf(stderr, "Unhandled voxel type\n");
        nifti_image_free(nim);
        return 3;
    }

    TIFF* tiff = NULL;
    tiff = TIFFOpen(output, "w");
    if(!tiff)
    {
        fprintf(stderr, "Failed to create %s\n", output);
        nifti_image_free(nim);
        return 4;
    }


    unsigned int lines;
    TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, (int)nim->dim[1]);
    TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, (int)nim->dim[2]);

    lines = nim->dim[2];
    if(nim->dim[0] > 2 && nim->dim[3] > 1)
    {
        TIFFSetField(tiff, TIFFTAG_IMAGEDEPTH, nim->dim[3]);
        lines *= nim->dim[3];
    }

    unsigned int bytes = 1;
    switch(nim->datatype)
    {
    case DT_UINT8: TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 8);  break;
    case DT_INT16: TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 16);  break;
    case DT_UINT16: TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 16);  break;
    case DT_FLOAT32: TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 32);  break;
    case DT_COMPLEX64: TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 64);  break;
    case DT_FLOAT64: TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 64);  break;
    case DT_RGB24: TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 24); break;
    }

    TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);

    switch(nim->datatype)
    {
    case DT_UINT8: TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT); bytes = 1; break;
    case DT_INT16: TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_INT); bytes = 2; break;
    case DT_UINT16: TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT); bytes = 2; break;
    case DT_FLOAT32: TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP); bytes = 4; break;
    case DT_COMPLEX64: TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_COMPLEXIEEEFP); bytes = 8; break;
    case DT_FLOAT64: TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP); bytes = 8; break;
    case DT_RGB24: TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT); bytes = 3; break;
    }

    TIFFSetField(tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(tiff, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
    TIFFSetField(tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);

    double units = 1;
    switch(nim->xyz_units)
    {
    case NIFTI_UNITS_METER: units = 10; break;
    case NIFTI_UNITS_MM: units = 0.1; break;
    case NIFTI_UNITS_MICRON: units = 0.0001; break;
    }

    char tmp[128];
    TIFFSetField(tiff, TIFFTAG_DOCUMENTNAME, input);
    if(nim->dim[0] > 2 && nim->dim[3] > 1)
    {
        sprintf(tmp, "ZRESOLUTION %f", (float)(1.0/nim->pixdim[2]*units));
        TIFFSetField(tiff, TIFFTAG_IMAGEDESCRIPTION, tmp);
    }

//    TIFFSetField(tiff, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
//    TIFFSetField(tiff, TIFFTAG_PAGENUMBER, 0, 1);

    TIFFSetField(tiff, TIFFTAG_XRESOLUTION, (float)(1.0/nim->pixdim[1]*units));
    TIFFSetField(tiff, TIFFTAG_YRESOLUTION, (float)(1.0/nim->pixdim[2]*units));
    TIFFSetField(tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_CENTIMETER);

    unsigned char* ptr = ((unsigned char*)nim->data);
    for(unsigned int l = 0; l < lines; l++)
    {
        TIFFWriteScanline(tiff, (void*)ptr, l, 0);
        ptr += (nim->dim[1]*bytes);
    }
//    TIFFWriteDirectory(tiff);
    TIFFFlushData(tiff);
    TIFFClose(tiff);
    nifti_image_free(nim);
    return true;
}

/**
 * @brief main
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char* argv[])
{
    if(argc < 3)
    {
        printf("Usage: %s input output\n", argv[0]);
        printf("Converts 2D and 3D raster image formats from NIfTI to TIFF\n");
        printf("2015.08.23 by Piotr M. Szczypinski\n");
        return 1;
    }
    return nifti2tiff(argv[1], argv[2]);
}
