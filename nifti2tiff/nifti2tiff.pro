#-------------------------------------------------
#
# Project created by QtCreator 2014-12-28T20:40:55
#
#-------------------------------------------------

TEMPLATE = app
TARGET = nifti2tiff

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += /usr/include/nifti

DESTDIR = ../Executables

SOURCES += \
    nifti2tiff.cpp

    
unix|win32: LIBS += -lniftiio

macx{
    LIBS += /usr/local/lib/libtiff.dylib
}
else:unix{
    LIBS += -ltiff
}
else:win32{
    INCLUDEPATH += D:/OpenCV/opencv/sources/3rdparty/libtiff
    LIBS += D:/OpenCV/opencv/build/x86/vc10/staticlib/libtiff.lib
    LIBS += D:/OpenCV/opencv/build/x86/vc10/staticlib/zlib.lib
}    
