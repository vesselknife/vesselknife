DEFINES += ITK_IO_FACTORY_REGISTER_MANAGER

INCLUDEPATH += /home/piotr/Program/testItk/BlockMatchingImageFilter/Build/ITKFactoryRegistration
INCLUDEPATH += /home/piotr/Program/ThirdParty/usr/include/ITK-4.13
INCLUDEPATH += /home/piotr/Program/ThirdParty/usr/include
INCLUDEPATH += /home/piotr/Program/ThirdParty/InsightToolkit-4.13.0/Modules/Nonunit/Review/include

LIBS += \
    /home/piotr/Program/ThirdParty/usr/lib/libITKLabelMap-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKQuadEdgeMesh-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKPolynomials-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKBiasCorrection-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKBioCell-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKDICOMParser-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOSpatialObjects-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKFEM-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOBMP-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOBioRad-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOBruker-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOCSV-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOGDCM-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOGE-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOGIPL-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOHDF5-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOJPEG-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOLSM-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOMINC-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOMRC-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOMesh-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOMeta-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIONIFTI-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIONRRD-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOPNG-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOSiemens-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOStimulate-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOTransformHDF5-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOTransformInsightLegacy-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOTransformMatlab-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOVTK-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKKLMRegionGrowing-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKOptimizersv4-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKVTK-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKVideoIO-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKWatersheds-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOXML-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkgdcmMSFF-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkgdcmDICT-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkgdcmIOD-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkgdcmDSED-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkgdcmCommon-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkgdcmjpeg8-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkgdcmjpeg12-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkgdcmjpeg16-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkgdcmopenjp2-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkgdcmcharls-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkgdcmuuid-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOTIFF-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitktiff-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkjpeg-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkminc2-4.13.a \
    -lrt \
    /home/piotr/Program/ThirdParty/usr/lib/libITKgiftiio-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKEXPAT-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKMetaIO-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKniftiio-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKznz-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKNrrdIO-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkpng-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOIPL-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkhdf5_cpp.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkhdf5.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkzlib-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOTransformBase-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKTransformFactory-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKOptimizers-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitklbfgs-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKIOImageBase-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKVideoCore-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKStatistics-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkNetlibSlatec-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKSpatialObjects-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKMesh-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKTransform-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKPath-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKCommon-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkdouble-conversion-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitksys-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libITKVNLInstantiation-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkvnl_algo-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkvnl-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkv3p_netlib-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitknetlib-4.13.a \
    /home/piotr/Program/ThirdParty/usr/lib/libitkvcl-4.13.a \
    -lpthread \
    -ldl \
    -lm
