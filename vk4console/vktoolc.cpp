#include "vktoolc.h"
#include <string>
#include <vector>
#include <iostream>
//#include "../ITKIOFactoryRegistration/itkImageIOFactoryRegisterManager.h"
//#include "../ITKIOFactoryRegistration/itkTransformIOFactoryRegisterManager.h"
#include <itkImageIOFactory.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkCropImageFilter.h>

extern ImageWithInfo image;
extern ImageWithInfo imager;
extern ImageWithInfo imageg;
extern ImageWithInfo imageb;
extern ImageWithInfo imagea;
extern Tree tree;
extern char* output;
extern char* algorithm;
extern bool verbose;

extern int fiducial_x;
extern int fiducial_y;
extern int fiducial_z;
extern std::vector <NameValue> parameterList;

bool saveImageItkFactory(const char* filename, ImageWithInfo* iwi)
{
    typedef  itk::ImageFileWriter< Byte3DImage > WriterType;
    WriterType::Pointer writer = WriterType::New();
    writer->SetFileName(filename);
    writer->SetInput(iwi->i);
    try
    {
        writer->Update();
    }
    catch (itk::ExceptionObject &ex)
    {
        std::cout << ex << std::endl;
        return false;
    }
    return true;
}

std::string VkTool::getObjectsName(void)
{
    return name;
}
std::string VkTool::getParameter(std::string parameterName)
{
    std::string r = "0";
    int count = parameterList.size();
    for(int n = 0; n < count; n++)
    {
        if(parameterName.compare(parameterList[n].name) == 0)
        {
            if(verbose)
            {
                std::cout << "  " << parameterName << "=" << parameterList[n].value << std::endl;
            }
            return parameterList[n].value;
        }
    }
    count = params.size();
    for(int n = 0; n < count; n++)
    {
        if(parameterName.compare(params[n].name) == 0)
        {
            if(verbose)
            {
                std::cout << "  " << parameterName << "=" << params[n].value << std::endl;
            }
            return params[n].value;
        }
    }
    if(verbose)
    {
        std::cout << "Parameter " << parameterName << " not set." << output <<std::endl;
    }
    return r;
}
void VkTool::createMethod(std::string tooltip)
{
}
void VkTool::createParameter(std::string name, std::string value, std::string tooltip)
{
    //std::cout << "    **" << name << "= " << value << std::endl;
    NameValue param;
    param.name = name;
    size_t f1 = value.find(';');
    if(f1 == std::string::npos)
        param.value = "0";
    else
    {
        size_t f2 = value.find(';', f1+1);
        param.value = value.substr(f1+1, f2-f1-1);
    }
    params.push_back(param);
    if(verbose)
    {
        std::cout << "    " << param.name << "=" << param.value << std::endl;
    }
}
std::string VkTool::getStringParameter(std::string parameterName)
{
    return getParameter(parameterName);
}
int VkTool::getIntParameter(std::string parameterName)
{
    return std::stoi (getParameter(parameterName));
}
float VkTool::getFloatParameter(std::string parameterName)
{
    return std::stof (getParameter(parameterName));
}


bool VkTool::crop(Byte3DImage::IndexType min, Byte3DImage::IndexType max)
{
    if(verbose)
    {
        std::cout << "Crop is not implemented." << std::endl;
    }
    return false;
}

bool VkTool::getCurrentFiducial(Byte3DImage::IndexType* fiducial)
{
    if(fiducial_x == std::numeric_limits<int>::min() &&
       fiducial_y == std::numeric_limits<int>::min() &&
       fiducial_z == std::numeric_limits<int>::min())
        return false;
    (*fiducial)[0] = fiducial_x;
    (*fiducial)[1] = fiducial_y;
    (*fiducial)[2] = fiducial_z;
    return true;
}
bool VkTool::getFiducial(unsigned int index, Byte3DImage::IndexType* fiducial)
{
    return getCurrentFiducial(fiducial);
}

ImageWithInfo* VkTool::getImage(unsigned int index, std::string* name)
{
    return &image;
}
ImageWithInfo* VkTool::getCurrentImage(std::string* name)
{
    return &image;
}
ImageWithInfo* VkTool::getRedImage(std::string* name)
{
    return &imager;
}
ImageWithInfo* VkTool::getGreenImage(std::string* name)
{
    return &imageg;
}
ImageWithInfo* VkTool::getBlueImage(std::string* name)
{
    return &imageb;
}
ImageWithInfo* VkTool::getAlphaImage(std::string* name)
{
    return &imagea;
}
void VkTool::appendImage(ImageWithInfo img, std::string name)
{
    saveImageItkFactory(output, &img);
    if(verbose)
    {
        std::cout << "Completed " << name << ": image saved to " << output << std::endl;
    }
}

Tree* VkTool::getTree(unsigned int index, std::string* name, bool* checked)
{
    return &tree;
}
Tree* VkTool::getCurrentTree(std::string *name, bool *checked)
{
    return &tree;
}
void VkTool::appendTree(Tree tree, std::string name, unsigned int color)
{
    tree.save(output, 0);
    if(verbose)
    {
        std::cout << "Completed " << name << ": tree saved to " << output << std::endl;
    }
}
void VkTool::appendTrees(std::vector<Tree> trees, std::vector<std::string> names, std::vector<unsigned int> colors)
{
    if(trees.size() > 0 && names.size() > 0)
    {
        trees[0].save(output, 0);
        if(verbose)
        {
            std::cout << "First tree " << names[0] << " saved to " << output <<std::endl;
        }
    }
}
