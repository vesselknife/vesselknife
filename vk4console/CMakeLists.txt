cmake_minimum_required(VERSION 3.1)

project(vk4console LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

add_executable(vk4console 
    main.cpp
    ../shared/sphear.cpp
    ../shared/tree.cpp 
    ../shared/radiiestimation.cpp
    ../shared/smoothing.cpp
    ../shared/treeextend.cpp 
    vktoolc.cpp
    ../shared/sphear.h 
    ../shared/imagefilters.h 
    ../shared/tree.h 
    ../shared/buildtree.h 
    ../shared/mz2itk.h 
    ../shared/radiiestimation.h
    ../shared/smoothing.h
    ../shared/byteimage.h 
    ../shared/treeextend.h 
    ../shared/imagefilters2.h 
    vktoolc.h
    ../vesselknife/vktools.h
    ../shared/filenameremap.h
    ../shared/filenameremap.cpp
)

find_package(ALGLIB REQUIRED)
target_link_libraries(vk4console PRIVATE ${ALGLIB_LIB})
target_include_directories(vk4console PRIVATE ${ALGLIB_INCLUDE_DIRS})

###############   ITK 5.10 crashes on vesselness computation, works fine with ITK 4.13.x
find_package(ITK 4 REQUIRED)
include(${ITK_USE_FILE})
target_link_libraries(vk4console PRIVATE ${ITK_LIBRARIES})
 
###############   This part needs to be set manually to give an access to ITK_Review modules
# target_include_directories(vk4console PRIVATE ~/Program/_3rdParty/InsightToolkit-4.13.3/Modules/Nonunit/Review/include)
# target_include_directories(vk4console PRIVATE ~/Program/ThirdParty/InsightToolkit-4.13.0/Modules/Nonunit/Review/include)
# target_include_directories(vk4console PRIVATE ~/Program/_3rdParty/InsightToolkit-5.1.0/Modules/Nonunit/Review/include)
 
# find_package(VTK REQUIRED)
# include(${VTK_USE_FILE}) 
# target_link_libraries(vk4console PRIVATE ${VTK_LIBRARIES})

# find_package(OpenGL REQUIRED)
# target_link_libraries(vk4console PRIVATE ${OPENGL_LIBRARY} )

find_package(dlib  REQUIRED)
target_link_libraries(vk4console PRIVATE dlib::dlib)

install(TARGETS vk4console DESTINATION bin)
