#include <string.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include "../shared/getoption.h"
#include "vktoolc.h"
std::vector< VkTool* > objectPointers;
#include "../vesselknife/vktools.h"
#include <chrono>

char* input = NULL;
char* inr = NULL;
char* ing = NULL;
char* inb = NULL;
char* ina = NULL;
char* itree = NULL;
char* output = NULL;
char* parameter = NULL;
char* algorithm = NULL;
bool isprinthelp = false;
bool verbose = false;
int threads = 0;

unsigned int image_size[3] = {(unsigned int)-1, (unsigned int)-1, (unsigned int)-1};
double voxel_spacing[3] = {-1, -1, -1};

ImageWithInfo image;
ImageWithInfo imager;
ImageWithInfo imageg;
ImageWithInfo imageb;
ImageWithInfo imagea;
Tree tree;

int fiducial_x = std::numeric_limits<int>::min();
int fiducial_y = std::numeric_limits<int>::min();
int fiducial_z = std::numeric_limits<int>::min();
std::vector <NameValue> parameterList;


//-----------------------------------------------------------------------------
int scan_parameters_local(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-a", "--algorithm", algorithm)
        else GET_STRING_OPTION("-i", "--input", input)
        else GET_STRING_OPTION("-inr", "--input-red", inr)
        else GET_STRING_OPTION("-ing", "--input-green", ing)
        else GET_STRING_OPTION("-inb", "--input-blue", inb)
        else GET_STRING_OPTION("-ina", "--input-alpha", ina)
        else GET_STRING_OPTION("-t", "--input-tree", itree)
        else GET_STRING_OPTION("-o", "--output", output)
        else GET_STRING_OPTION("-p", "--set-parameter", parameter)
        else GET_INT_OPTION("-x", "--fiducial-x", fiducial_x)
        else GET_INT_OPTION("-y", "--fiducial-y", fiducial_y)
        else GET_INT_OPTION("-z", "--fiducial-z", fiducial_z)
        else GET_NOARG_OPTION("-v", "--verbose", verbose, true)
        else GET_INT_OPTION("-n", "--num-threads", threads)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;

        if(parameter != nullptr)
        {
            NameValue par;
            std::string s = parameter;
            unsigned int d = s.find("=");
            par.name = s.substr(0, d);
            par.value = s.substr(d+1);
            parameterList.push_back(par);
            parameter = nullptr;
        }
        argi++;
    }
    return 0;
}

//-----------------------------------------------------------------------------
void printhelp_local(void)
{
    //------------------------------------------------------------------------------------------v
    printf("Usage: vk4console [OPTION]...\n");
    printf("VesselKnife for console and batch processing\n");
    printf("Release 22.04\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -a, --algorithm      Name of the algorithm\n");
    printf("  -i, --input <file>   Input image\n");
    printf("  -inr, --input-red    Red channel image\n");
    printf("  -ing, --input-green  Green channel image\n");
    printf("  -inb, --input-blue   Blue channel image\n");
    printf("  -ina, --input-alpha  Alpha channel image\n");
    printf("  -o, --output <file>  Output image or tree\n");
    printf("  -t, --input-tree     Input tree\n");
    printf("  -p, --set-parameter  Set parameter, e.g. -p Sigma=1\n");
    printf("  -x, --fiducial-x     x coordinate in integer, voxel space\n");
    printf("  -y, --fiducial-y     y coordinate in integer, voxel space\n");
    printf("  -z, --fiducial-z     z coordinate in integer, voxel space\n");
    printf("  -v, --verbose        Verbose mode\n");
    printf("  -n, --num-threads    Number of ITK threads - defaults to maximum available\n");
    printf("  /?, --help           Display help and exit\n\n");
    printf("Examples:\n");
    printf("  vk4console -v -o dummy -a dummy\n");
    printf("  vk4console -i image.nii -o thresholded.nii -a Thresholding -p Min=127\n");
    printf("  vk4console -i thresholded.nii -o skelet.nii -a SkeletonFromBinary\n");
    printf("  vk4console -i skelet.nii -o tree.txt -a TreeFromSkeleton\n");
    printf("  vk4console -i image.nii -t tree.txt -o out.txt -a RadiiFromMultiscaleVesselness -p StdevMin=1 -p StdevMax=4\n\n");
}

bool loadImageItkFactory(const char* filename, ImageWithInfo* imageWithInfo)
{
    std::stringstream ss;
    itk::ImageIOBase::IOComponentType type;
    size_t numDimensions;
    itk::ImageIOBase::Pointer imageIO = itk::ImageIOFactory::CreateImageIO(filename, itk::ImageIOFactory::ReadMode);
    if( !imageIO ) return false;
    imageIO->SetFileName(filename);
    imageIO->ReadImageInformation();
    type = imageIO->GetComponentType();
    numDimensions =  imageIO->GetNumberOfDimensions();

    ss << "File name: " << filename << std::endl;
    typedef itk::Image< unsigned char, 3> ImageType;

    if(type == itk::ImageIOBase::UCHAR || type == itk::ImageIOBase::CHAR)
    {
        typedef itk::ImageFileReader<ImageType> ReaderType;
        ReaderType::Pointer reader = ReaderType::New();
        reader->SetFileName(filename);
        try
        {
            reader->Update();
        }
        catch (itk::ExceptionObject &ex)
        {
            std::cout << ex << std::endl;
            return false;
        }
        ImageType::Pointer ii = reader->GetOutput();
        Mz2Itk<unsigned char>::getMZ(&(imageWithInfo->i), ii);

        if(type == itk::ImageIOBase::CHAR)
        {
            unsigned int d;
            size_t max = 1;
            for(d = 0; d < 3; d++) max *= imageWithInfo->i->GetLargestPossibleRegion().GetSize()[d];
            unsigned char* ptro = imageWithInfo->i->GetPixelContainer()->GetImportPointer();
            for(d = 0; d < max; d++, ptro++) *ptro = *ptro + 128;
        }
        typedef itk::ImageIOBase::IOComponentType ScalarPixelType;
        const ScalarPixelType pixelType = reader->GetImageIO()->GetComponentType();
        ss << "Original voxel type: " << reader->GetImageIO()->GetComponentTypeAsString(pixelType) << std::endl;
        ss << "Dimensions: " << reader->GetImageIO()->GetNumberOfDimensions() << std::endl;
        ss << "Image size: " << ii->GetLargestPossibleRegion().GetSize() << std::endl;
        ss << "Voxel spacing: " << ii->GetSpacing() << std::endl;
    }
    else
    {
        typedef itk::Image< float, 3> ImageTypeF;
        typedef itk::ImageFileReader<ImageTypeF> ReaderType;
        ReaderType::Pointer reader = ReaderType::New();
        reader->SetFileName(filename);
        try
        {
            reader->Update();
        }
        catch (itk::ExceptionObject &ex)
        {
            std::cout << ex << std::endl;
            return false;
        }
        ImageTypeF::Pointer ii = reader->GetOutput();
        float min, max;
        Mz2Itk<float>::getMZNormalized(&(imageWithInfo->i), ii, &min, &max);
        typedef itk::ImageIOBase::IOComponentType ScalarPixelType;
        const ScalarPixelType pixelType = reader->GetImageIO()->GetComponentType();
        ss << "Original voxel type: " << reader->GetImageIO()->GetComponentTypeAsString(pixelType) << std::endl;
        ss << "Mapped from <" << min << ", " << max << "> to <0, 255>" << std::endl;
        ss << "Dimensions: " << reader->GetImageIO()->GetNumberOfDimensions() << std::endl;
        ss << "Image size: " << ii->GetLargestPossibleRegion().GetSize() << std::endl;
        ss << "Voxel spacing: " << ii->GetSpacing() << std::endl;
    }

    imageWithInfo->t = ss.str();
    return true;
}


bool loadImage(const char* filename, ImageWithInfo* img)
{

    if(loadImageItkFactory(filename, img))
    {
        if(image_size[0] == (unsigned int)-1)
        {
            image_size[0] = img->i->GetLargestPossibleRegion().GetSize()[0];
            image_size[1] = img->i->GetLargestPossibleRegion().GetSize()[1];
            image_size[2] = img->i->GetLargestPossibleRegion().GetSize()[2];
            voxel_spacing[0] = img->i->GetSpacing()[0];
            voxel_spacing[1] = img->i->GetSpacing()[1];
            voxel_spacing[2] = img->i->GetSpacing()[2];
        }
        else
        {
            if(img->i->GetLargestPossibleRegion().GetSize()[0] != image_size[0] ||
               img->i->GetLargestPossibleRegion().GetSize()[1] != image_size[1] ||
               img->i->GetLargestPossibleRegion().GetSize()[2] != image_size[2])
            {
                fprintf(stderr, "Aborting: Size of images do not match.\n");
                return false;
            }
            if( img->i->GetSpacing()[0] != voxel_spacing[0] ||
                img->i->GetSpacing()[1] != voxel_spacing[1] ||
                img->i->GetSpacing()[2] != voxel_spacing[2])
            {
                fprintf(stderr, "Warning: Voxel spacing does not match in %s.\n", filename);
            }
        }

        if(verbose)
        {
            printf("\n%s\n", img->t.c_str());
            fflush(stdout);
        }
        return true;
    }
    return false;
}

//-----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    int ret = scan_parameters_local(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp_local();
        return 1;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try --help for more information\n");
        return 2;
    }

    if(threads > 0)
    {

#if (ITK_VERSION_MAJOR < 5)
        itk::MultiThreader::SetGlobalMaximumNumberOfThreads( threads );
        itk::MultiThreader::SetGlobalDefaultNumberOfThreads( threads );
#else
        itk::MultiThreaderBase::SetGlobalMaximumNumberOfThreads( threads );
        itk::MultiThreaderBase::SetGlobalDefaultNumberOfThreads( threads );
#endif
    }

    if(input != nullptr)
        if(!loadImage(input, &image))
        {
            fprintf(stderr, "Aborting: Cannot load image from %s\n", input);
            return 3;
        }
    if(inr != nullptr)
        if(!loadImage(inr, &imager))
        {
            fprintf(stderr, "Aborting: Cannot load image from %s\n", inr);
            return 4;
        }
    if(ing != nullptr)
        if(!loadImage(ing, &imageg))
        {
            fprintf(stderr, "Aborting: Cannot load image from %s\n", inb);
            return 4;
        }
    if(inb != nullptr)
        if(!loadImage(inb, &imageb))
        {
            fprintf(stderr, "Aborting: Cannot load image from %s\n", inb);
            return 4;
        }
    if(ina != nullptr)
        if(!loadImage(ina, &imagea))
        {
            fprintf(stderr, "Aborting: Cannot load image from %s\n", ina);
            return 4;
        }
    if(itree != nullptr)
        if(!tree.load(itree))
        {
            fprintf(stderr, "Aborting: Cannot load tree from %s\n", itree);
            return 5;
        }
    if(output == nullptr)
    {
        fprintf(stderr, "Aborting: Missing output file name\n");
        return 6;
    }

    if(algorithm == nullptr)
    {
        fprintf(stderr, "Aborting: Missing algorithm name\n");
        return 7;
    }


    if(verbose)
        printf("Initializing algorithms:\n");
    unsigned int count = objectPointers.size();
    unsigned int c;

    for(c = 0; c < count; c++)
    {
        if(verbose)
        {
            printf("  %s\n", objectPointers[c]->getObjectsName().c_str());
            fflush(stdout);
        }
        objectPointers[c]->initialize();
    }
    for(c = 0; c < count; c++)
    {
        if(objectPointers[c]->getObjectsName().compare(algorithm) == 0)
        {
            break;
        }
    }

    if(c < count)
    {
        if(verbose)
        {
            printf("\nExecuting: %s\n", objectPointers[c]->getObjectsName().c_str());
            fflush(stdout);
        }

        auto t1 = std::chrono::high_resolution_clock::now();

        if(!objectPointers[c]->runMethod())
        {
            fprintf(stderr, "Executing: Error - the method returned false\n");
            return 9;
        }

        auto t2 = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> ms_double = t2 - t1;
        if(verbose)
        {
            float ms_float = ms_double.count();
            printf("Execution time: %f ms\n", ms_float);
            fflush(stdout);
        }
    }
    else
    {
        fprintf(stderr, "Aborting: Algorithm name %s does not match\n", algorithm);
        return 8;
    }
    return 0;
}

