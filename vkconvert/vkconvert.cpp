/*
 * VesselKnife tree format to VTK data format converter
 *
 * Copyright 2015 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "../shared/tree.h"
#include <string>

#include <vtkGenericDataObjectWriter.h>
#include <vtkStructuredGrid.h>
#include <vtkPointData.h>
#include <vtkSmartPointer.h>
#include <vtkDoubleArray.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkFieldData.h>
#include <vtkDataSetAttributes.h>



#include <vtkSmartPointer.h>
#include <vtkVersion.h>

#include <vtkParametricFunctionSource.h>
#include <vtkTupleInterpolator.h>
#include <vtkTubeFilter.h>
#include <vtkParametricSpline.h>

#include <vtkDoubleArray.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>

#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
//#include <vtkRenderWindow.h>
//#include <vtkRenderer.h>
//#include <vtkRenderWindowInteractor.h>
#include <vtkBooleanOperationPolyDataFilter.h>
#include <vtkSTLWriter.h>
#include <vtkTriangleFilter.h>
#include <vtkAppendPolyData.h>
#include "../shared/vtkimport.h"

#include "../shared/getoption.h"

char* modestring = NULL;
char* input = NULL;
char* output = NULL;
bool isprinthelp = false;

//-----------------------------------------------------------------------------
int scan_parameters_local(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-m", "--mode", modestring)
        else GET_STRING_OPTION("-i", "--input", input)
        else GET_STRING_OPTION("-o", "--output", output)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}

//-----------------------------------------------------------------------------
void printhelp_local(char* name)
{
    //------------------------------------------------------------------------------------------v
    printf("Usage: %s [OPTION]...\n", name);
    printf("Converts 3D model formats\n");
    printf("2019-2022 by Piotr M. Szczypinski\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -m, --mode <file>    Mode 'tree2stl', 'tree2vtk' or 'vtk2tree'\n");
    printf("  -i, --input <file>   Input file\n");
    printf("  -o, --output <file>  Output file\n");
    printf("  /?, --help           Display this help and exit\n\n");
}


vtkSmartPointer<vtkPolyData> makeUnion(Tree* tree)
{
    const int determine_derivative = 3;
    const bool interpoate_radius_spline = false;
    const double nodes_density_decimator = 1.0;
    const char* radius_name = "Radius";
    bool use_boolean = false;
    double downsize = 0.95;

    vtkSmartPointer<vtkPolyData> currentUnion = vtkSmartPointer<vtkPolyData>::New();

    unsigned int branch_index_count = tree->count();
    //branch_index_count = 2;

    for (unsigned int branch_index = 0; branch_index<branch_index_count; branch_index++)
    {
        vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
        vtkSmartPointer<vtkTupleInterpolator> interpolatedRadius = vtkSmartPointer<vtkTupleInterpolator> ::New();

        if(interpoate_radius_spline) interpolatedRadius->SetInterpolationTypeToSpline();
        else interpolatedRadius->SetInterpolationTypeToLinear();

        interpolatedRadius->SetNumberOfComponents(1);
        unsigned int node_index_count = tree->count(branch_index);
        for(unsigned int node_index = 0; node_index < node_index_count; node_index++)
        {
            NodeIn3D node = tree->node(branch_index, node_index);
            points->InsertPoint(node_index, node.x, node.y, node.z);
            double radius = fabs(node.radius);
            interpolatedRadius->AddTuple(node_index, &radius);

            if(node.radius > 0)
            {
                //node.radius = -node.radius * downsize;
                tree->setNode(node, tree->nodeIndex(branch_index, node_index));
                tree->setRadius(tree->nodeIndex(branch_index, node_index), -node.radius * downsize);
            }

        }
        vtkSmartPointer<vtkParametricSpline> spline = vtkSmartPointer<vtkParametricSpline>::New();
        spline->SetPoints(points);
        spline->ParameterizeByLengthOff();
        spline->SetLeftConstraint(determine_derivative);
        spline->SetRightConstraint(determine_derivative);
        vtkSmartPointer<vtkParametricFunctionSource> functionSource = vtkSmartPointer<vtkParametricFunctionSource>::New();
        functionSource->SetParametricFunction(spline);
        functionSource->SetUResolution(nodes_density_decimator * node_index_count);
        functionSource->Update();

        vtkSmartPointer<vtkDoubleArray> tubeRadius = vtkSmartPointer<vtkDoubleArray>::New();
        tubeRadius->SetName(radius_name);
        unsigned int interpolated_node_index_count = functionSource->GetOutput()->GetNumberOfPoints();
        tubeRadius->SetNumberOfTuples(interpolated_node_index_count);
        double tMin = interpolatedRadius->GetMinimumT();
        double tMax = interpolatedRadius->GetMaximumT();

        for (unsigned int interpolated_node_index = 0; interpolated_node_index < interpolated_node_index_count; ++interpolated_node_index)
        {
            double r[1];
            double t = (tMax - tMin) / (interpolated_node_index_count - 1) * interpolated_node_index + tMin;
            interpolatedRadius->InterpolateTuple(t, r);
            tubeRadius->SetTuple1(interpolated_node_index, r[0]);
        }

        vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();
        polyData = functionSource->GetOutput();
        polyData->GetPointData()->AddArray(tubeRadius);
        polyData->GetPointData()->SetActiveScalars(radius_name);

        vtkSmartPointer<vtkTubeFilter> tuber = vtkTubeFilter::New();

#if VTK_MAJOR_VERSION <= 5
        tuber->SetInput(polyData);
#else
        tuber->SetInputData(polyData);
#endif
        tuber->SetNumberOfSides(20);
        tuber->SetVaryRadiusToVaryRadiusByAbsoluteScalar();
        tuber->CappingOn();

        vtkSmartPointer< vtkTriangleFilter > triangleFilter = vtkSmartPointer<vtkTriangleFilter>::New();
        triangleFilter->SetInputConnection( tuber->GetOutputPort() );
        triangleFilter->Update();

        if(branch_index == 0)
        {
            currentUnion = triangleFilter->GetOutput();
        }
        else if(use_boolean)
        {
            vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
            booleanOperation->SetOperationToUnion();
#if VTK_MAJOR_VERSION <= 5
            booleanOperation->SetInputConnection( 0, currentUnion);
            booleanOperation->SetInputConnection( 1, triangleFilter->GetProducerPort() );
#else
            booleanOperation->SetInputData( 0, currentUnion );
            booleanOperation->SetInputData( 1, triangleFilter->GetOutput());
#endif
            booleanOperation->Update();
            currentUnion = booleanOperation->GetOutput();
        }
        else
        {
            vtkSmartPointer<vtkAppendPolyData> appendFilter =  vtkSmartPointer<vtkAppendPolyData>::New();
#if VTK_MAJOR_VERSION <= 5
            appendFilter->AddInputConnection(currentUnion);
            appendFilter->AddInputConnection(triangleFilter->GetProducerPort());
#else
            appendFilter->AddInputData(currentUnion);
            appendFilter->AddInputData(triangleFilter->GetOutput());
#endif
            appendFilter->Update();
            currentUnion = appendFilter->GetOutput();
        }
    }

    return currentUnion;
}




bool export2stl(Tree* tree, const char* filename)
{
    vtkSmartPointer<vtkPolyData> currentUnion = makeUnion(tree);

    vtkSmartPointer<vtkSTLWriter> stlWriter = vtkSmartPointer<vtkSTLWriter>::New();
    stlWriter->SetFileName(filename);
    stlWriter->SetFileTypeToASCII();
#if VTK_MAJOR_VERSION <= 5
    stlWriter->SetInputConnection(currentUnion);
#else
    stlWriter->SetInputData(currentUnion);
#endif
    stlWriter->Write();

//    vtkSmartPointer<vtkPolyDataMapper> tubeMapper =
//      vtkSmartPointer<vtkPolyDataMapper>::New();
//    tubeMapper->SetInputData(currentUnion);
//    //tubeMapper->SetInputConnection(triangleFilter->GetOutputPort());
//    //tubeMapper->SetScalarRange(tubePolyData->GetScalarRange());
//    vtkSmartPointer<vtkActor> tubeActor = vtkSmartPointer<vtkActor>::New();
//    tubeActor->SetMapper(tubeMapper);
//    vtkSmartPointer<vtkRenderer> renderer =
//      vtkSmartPointer<vtkRenderer>::New();
//    vtkSmartPointer<vtkRenderWindow> renderWindow =
//      vtkSmartPointer<vtkRenderWindow>::New();
//    renderWindow->AddRenderer(renderer);
//    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
//      vtkSmartPointer<vtkRenderWindowInteractor>::New();
//    renderWindowInteractor->SetRenderWindow(renderWindow);
//    renderer->AddActor(tubeActor);
//    renderer->SetBackground(.4, .5, .6);
//    renderWindow->Render();
//    renderWindowInteractor->Start();
    return true;
}





//=============================================================================
// converter
bool export2vtk(Tree* tree, const char* filename)
{
    vtkSmartPointer<vtkPolyData> currentUnion = makeUnion(tree);

    vtkSmartPointer<vtkGenericDataObjectWriter> writer = vtkSmartPointer<vtkGenericDataObjectWriter>::New();

#if VTK_MAJOR_VERSION <= 5
    writer->SetInput(poly);
#else
    writer->SetInputData(currentUnion);
#endif

    writer->SetFileName(filename);
    writer->Update();
    //always true as VTK has no notification on IO completion
    return true;
}

//=============================================================================
// Main function
int main(int argc, char* argv[])
{
    int ret = scan_parameters_local(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp_local(argv[0]);
        return 1;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try --help for more information.\n");
        return 2;
    }
    if(modestring == NULL)
    {
        fprintf(stderr, "Unspecified mode (-m)\n");
        return 3;
    }
    if(input == NULL)
    {
        fprintf(stderr, "Unspecified input (-i)\n");
        return 4;
    }
    if(output == NULL)
    {
        fprintf(stderr, "Unspecified output (-o)\n");
        return 5;
    }

    std::string ms = modestring;
    if(ms == "tree2stl")
    {
        Tree tree;
        if(!tree.load(input))
        {
            fprintf(stderr, "Cannot load tree from %s\n", input);
            return 11;
        }
        if(!export2stl(&tree, output))
        {
            fprintf(stderr, "Cannot save stl to %s\n", output);
            return 12;
        }
    }
    else if(ms == "tree2vtk")
    {
        Tree tree;
        if(!tree.load(input))
        {
            fprintf(stderr, "Cannot load tree from %s\n", input);
            return 21;
        }
        if(!export2vtk(&tree, output))
        {
            fprintf(stderr, "Cannot save vtk to %s\n", output);
            return 22;
        }
    }
    else if(ms == "vtk2tree")
    {
        Tree tree = VtkImport::getTree(input);
        if(tree.count() <= 0)
        {
            fprintf(stderr, "Cannot load vtk from %s\n", input);
            return 31;
        }
        if(!tree.save(output, 0))
        {
            fprintf(stderr, "Cannot save tree to %s\n", output);
            return 32;
        }
    }
    else
    {
        fprintf(stderr, "Invalid mode (-m %s)\n", modestring);
        return 6;
    }
    return 0;
}
