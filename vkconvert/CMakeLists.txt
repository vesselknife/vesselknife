cmake_minimum_required(VERSION 3.1)

project(vkconvert LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

add_executable(vkconvert
    vkconvert.cpp
    ../shared/tree.cpp
    ../shared/vtkimport.cpp
    ../shared/tree.h 
    ../shared/vtkimport.h
    ../shared/filenameremap.h
    ../shared/filenameremap.cpp
)

# find_package(ALGLIB REQUIRED)
# target_link_libraries(vkconvert PRIVATE ${ALGLIB_LIB})
# target_include_directories(vkconvert PRIVATE ${ALGLIB_INCLUDE_DIRS})
# 
# find_package(ITK REQUIRED)
# include(${ITK_USE_FILE})
# if( "${ITK_VERSION_MAJOR}" LESS 4 )
#   target_link_libraries(vkconvert ITKReview ${ITK_LIBRARIES})
# else( "${ITK_VERSION_MAJOR}" LESS 4 )
#   target_link_libraries(vkconvert PRIVATE ${ITK_LIBRARIES})
# endif( "${ITK_VERSION_MAJOR}" LESS 4 )
 
find_package(VTK REQUIRED)
include(${VTK_USE_FILE}) 
target_link_libraries(vkconvert PRIVATE ${VTK_LIBRARIES})

# find_package(OpenGL REQUIRED)
# target_link_libraries(vkconvert PRIVATE ${OPENGL_LIBRARY} )

# find_package(dlib  REQUIRED)
# target_link_libraries(vkconvert PRIVATE dlib::dlib)

install(TARGETS vkconvert DESTINATION bin)
