#-------------------------------------------------
#
# Project created by QtCreator 2014-12-28T20:40:55
#
#-------------------------------------------------

TEMPLATE = app
TARGET = vkconvert

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
DESTDIR = ../../Executables

SOURCES += \
    ../shared/tree.cpp \
    ../shared/vtkimport.cpp \
    vkconvert.cpp

HEADERS  += \
    ../shared/getoption.h \
    ../shared/tree.h \
    ../shared/vtkimport.h

include(../Pri/config.pri)
include(../Pri/vtk.pri)
